<?php
//die('king');
require '.././libs/Slim/Slim.php';
require_once 'dbHelper.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app = \Slim\Slim::getInstance();
$db = new dbHelper();

date_default_timezone_set("Asia/Kolkata");
$base_url = "http://base3.engineerbabu.com/waggingpal/api/v1/";
//define('base_url', $base_url);

$dateTime = date("Y-m-d H:i:s", time()); 
$militime=round(microtime(true) * 1000);
define('dateTime', $dateTime);
define('base_url', $base_url);
define('militime', $militime);



$app->post('/register', function() use ($app) { 
    $email = $app->request()->params('email');
    $password = md5($app->request()->params('password'));
    $name = $app->request()->params('name');
    $gender = $app->request()->params('gender');
    $dob = $app->request()->params('dob');

    $city = $app->request()->params('city');
    $state = $app->request()->params('state');
    $country = $app->request()->params('country');

    $city_id = $app->request()->params('city_id');
    $state_id = $app->request()->params('state_id');
    $country_id = $app->request()->params('country_id');

    $device_type = $app->request()->params('device_type');
    $device_token = $app->request()->params('device_token');
    $device_id = $app->request()->params('device_id');
    $lat = $app->request()->params('lat');
    $lng = $app->request()->params('lng');
   
    $condition = array('email'=>$email);
    
    $data = array(
                    'email'=>$email,
                    'password'=>$password,
                    'name'=>$name,
                    'gender'=>$gender,
                    'dob'=>$dob,
                    'city'=>$city,
                    'state'=>$state,
                    'country'=>$country,
                    'city_id'=>$city_id,
                    'state_id'=>$state_id,
                    'country_id'=>$country_id,
                    'create_at'=>militime,
                    'update_at'=>militime,
                    'device_type'=>$device_type,
                    'device_token'=>$device_token,
                    'device_id'=>$device_id,
                    'lat'=>$lat,
                    'lng'=>$lng

        );

    global $db;
    

    $rows = $db->select("user","*",$condition);
//print_r($rows);exit;
    if($rows["status"]=="success")
    {  
        foreach ($rows['data'] as $key2) {
            $status = $key2['email_verify'];
            $userid = $key2['user_id'];
           // $user_info = $key2;

             $user_info = array('user_id'=>$key2['user_id'],
                    'email'=>$key2['email'],
                    'name'=>$key2['name'],
                    'gender'=>$key2['gender'],
                    'dob'=>$key2['dob'],
                    'city'=>$key2['city'],
                    'state'=>$key2['state'],
                    'country'=>$key2['country'],
                    'city_id'=>$key2['city_id'],
                    'state_id'=>$key2['state_id'],
                    'country_id'=>$key2['country_id'],
                    'device_type'=>$key2['device_type'],
                    'device_token'=>$key2['device_token'],
                    'device_id'=>$key2['device_id'],
                    'lat'=>$key2['lat'],
                    'lng'=>$key2['lng']
                    );
        }

        if($status == '1')
        {
            $rows["message"] = "Email already exists";
            $rows["status"] = "fail";
            $rows["data"] = '';
            
        }else
        {
              $code = substr(randomPassword(),0,6);
              $sel_temp = $db->select("email_template_descriptions","*",array('id'=>'1'),array());
              if($sel_temp['status'] =="success")
              {
                foreach($sel_temp['data'] as $key3)
                {
                    $content = $key3['content'];
                }
              }else
              {
               
                     $content = ''; 
              }
              
              
              $subject = "Waggingpal App: Verification";
             
              $message=stripcslashes($content);
              $message=str_replace("{date}",date("d"),$message);
              $message=str_replace("images/line-break-3.jpg",base_url.'images/email_img/line-break-3.jpg',$message);
              $message=str_replace("images/line-break-2.jpg",base_url.'images/email_img/line-break-2.jpg',$message);

              $message=str_replace("{email}",$email,$message);
              //$message=str_replace("{password}",$u_password,$message);
              $message=str_replace("{link}","<a href='http://base3.engineerbabu.com/waggingpal/api/v1/index.php/verifyEmail?user_id=".$userid."&code=".$code."'>CLICK HERE</a>",$message);
             // echo $message; exit;
              $email_from='no-repply@app.waggingpal.com';
              $headers  = 'MIME-Version: 1.0' . "\r\n";
              $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
              $headers .= 'From: '.$email_from. '\r\n';
            // Mail it
              $rows3 = $db->update("user", array("code"=>$code), $condition,array());

            @mail($email, $subject, $message, $headers);
            
            $rows["message"] = "Verification Link has been sent to your registered email address";
            $rows['data'] = $user_info;
            
        }
        echoResponse(200, $rows);
        
    }else
    {
        $rows1 = $db->insert("user", $data, array());
        //print_r($rows1); exit;
        if($rows1["status"]=="success")
        {
            //$user_id1 = $rows1["data"];
             $rowss = $db->select("user","*",$condition);
             
                if($rowss['status'] == "success")
                {  
                    foreach ($rowss['data'] as $key) {
                     
                       $arr = array('user_id'=>$key['user_id'],
                                'email'=>$key['email'],
                                'name'=>$key['name'],
                                'gender'=>$key['gender'],
                                'dob'=>$key['dob'],
                                'city'=>$key['city'],
                                'state'=>$key['state'],
                                'country'=>$key['country'],
                                'city_id'=>$key['city_id'],
                                'state_id'=>$key['state_id'],
                                'country_id'=>$key['country_id'],
                                'device_type'=>$key['device_type'],
                                'device_token'=>$key['device_token'],
                                'device_id'=>$key['device_id'],
                                'lat'=>$key['lat'],
                                'lng'=>$key['lng']
                                );
                    }

                    $id = $rows1["data"];
                    $condition1 = array('user_id'=>$id);

                    $code = substr(randomPassword(),0,6);
                    
                    $sel_temp = $db->select("email_template_descriptions","*",array('id'=>'1'),array());
                    
                    if($sel_temp['status'] =="success")
                    {
                        foreach($sel_temp['data'] as $key3)
                        {
                          $content = $key3['content'];
                        }
                    }else
                    {
                     $content = ''; 
                    }
                      
                  
                    $subject = "Waggingpal App: Verification";
                   
                    $message=stripcslashes($content);
                    $message=str_replace("{date}",date("d"),$message);
                    $message=str_replace("images/line-break-3.jpg",base_url.'images/email_img/line-break-3.jpg',$message);
                    $message=str_replace("images/line-break-2.jpg",base_url.'images/email_img/line-break-2.jpg',$message);

                    $message=str_replace("{email}",$email,$message);
                    //$message=str_replace("{password}",$u_password,$message);
                    $message=str_replace("{link}","<a href='http://base3.engineerbabu.com/waggingpal/api/v1/index.php/verifyEmail?user_id=".$id."&code=".$code."'>CLICK HERE</a>",$message);
                   // echo $message; exit;
                    $email_from='no-repply@app.waggingpal.com';
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: '.$email_from. '\r\n';            // Mail it
                   
                   $rows3 = $db->update("user", array("code"=>$code), $condition1,array());

                  @mail($email, $subject, $message, $headers);

                  $rowss["message"] = "Verification Link has been sent to your registered email address";
                  $rowss['data'] = $arr;
                }else
                {
                    $rowss["message"] = "some error";
                    $rowss["data"] = "";
                }
         
            
            echoResponse(200, $rowss);
        }else
            {
                $rows1["message"] = "some error";
                echoResponse(200, $rows1);
            }
    }
        
    
});


function randomPassword() {
    $alphabet = "0123456789";
   // $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

    $pass = array(); /*remember to declare $pass as an array*/
    $alphaLength = strlen($alphabet) - 1; /*put the length -1 in cache*/
    for ($i = 0; $i < 6; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); /*turn the array into a string*/
}


$app->post('/login', function() use ($app) { 
    $email = $app->request()->params('email');
    $password = md5($app->request()->params('password'));

    $device_id = $app->request()->params('device_id');
    $device_type = $app->request()->params('device_type');
    $device_token = $app->request()->params('device_token');
    //$lat = $app->request()->params('lat');
    //$lng = $app->request()->params('lng');

    $condition = array('email'=>$email,'password'=>$password);

    $condition1 = array('email'=>$email);

    global $db;
    
    $rows = $db->select("user","*",$condition);

    if($rows["status"]=="success")
    {     
          $user_id = $rows['data'][0]['user_id'];
          $status = $rows['data'][0]['email_verify'];

      if($status == '1')
      {

        $rows3 = $db->update("user", array("device_id"=>$device_id,"device_type"=>$device_type,"device_token"=>$device_token), array('user_id'=>$user_id,'email'=>$email),array());
        
        $rows1 = $db->select("device_token","*",array("device_id"=>$device_id,"device_type"=>$device_type),array());
        
        if($rows1['status']=="success")
        {   
            foreach ($rows1['data'] as $key3) {
                $user_id1= $key3['user_id'];
                $email1 = $key3['email'];
            }

            if($user_id == $user_id1 && $email1 == $email)
            {
                $rows5 = $db->update("device_token", array("device_token"=>$device_token,"updated_at"=>militime),array('user_id'=>$user_id1,'email'=>$email1),array());
            
            }else
            {
                $rows6 = $db ->insert("device_token",array('user_id'=>$user_id,'email'=>$email,'device_id'=>$device_id,'device_type'=>$device_type,"device_token"=>$device_token,"created_at"=>militime),array());    

                $update =  "UPDATE device_token SET device_token ='' WHERE device_id ='$device_id' AND device_type = '$device_type' AND user_id !='$user_id'";
                $rows5 = $db->customQuery($update);
            }   
        }else
        {
            $rows4 = $db ->insert("device_token",array('user_id'=>$user_id,'email'=>$email,'device_id'=>$device_id,'device_type'=>$device_type,"device_token"=>$device_token,"created_at"=>militime),array());
        }

        $rows2 = $db->select("user","*",$condition); 
        
        if($rows2['status'] == "success")
        {

            foreach ($rows2['data'] as $key) 
            { 
               $image = $key['profile_pic'];

                $arr = array('user_id'=>$key['user_id'],
                                'email'=>$key['email'],
                                'name'=>$key['name'],
                                'gender'=>$key['gender'],
                                'image'=>$image,
                                'dob'=>$key['dob'],
                                'city'=>$key['city'],
                                'state'=>$key['state'],
                                'country'=>$key['country'],
                                'city_id'=>$key['city_id'],
                                'state_id'=>$key['state_id'],
                                'country_id'=>$key['country_id'],
                                'device_type'=>$key['device_type'],
                                'device_token'=>$key['device_token'],
                                'device_id'=>$key['device_id'],
                                'lat'=>$key['lat'],
                                'lng'=>$key['lng'],
                                'pet_type'=>$key['pet_type'],
                                'breed'=>$key['breed'],
                                'pet_gender'=>$key['pet_gender'],
                                'size'=>$key['size'],
                                'age'=>$key['age'],
                                'first_login'=>$key['first_login']

                              );
              $rows7 = $db->update("user", array('first_login'=>'1'),array('email'=>$email),array());

              }
              $rows2["message"] = "Login successfully.";
              $rows2['data']=$arr;

            }else
            {
                $rows2["message"] = "error";
                $rows2['data']= '';
            }
            echoResponse(200, $rows2);
      }else
      {
        $rows["message"] = "Please verify your email address";
        $rows['data']= '';
        echoResponse(200, $rows);
      }

    }else
    {
        $rows["message"] = "Invalid Credential";
        $rows['data']= '';
        echoResponse(200, $rows);
    }
    
});

$app->get('/verifyEmail', function() use ($app){
    $user_id = $app->request()->params('user_id');
    $code = $app->request()->params('code');

    $condition = array('user_id'=>$user_id);

    global $db;
    $rows = $db->select("user","*",$condition);
    if($rows["status"]=="success")
    {
        foreach ($rows['data'] as $key) {
                $very_code = $key['code'];
        }
        if($code == $very_code)
        {
          $sel_temp = $db->select("email_template_descriptions","*",array('id'=>'3'),array());
          
          if($sel_temp['status'] =="success")
              {
                foreach($sel_temp['data'] as $key3)
                {
                    $content = $key3['content'];
                }
              }else
              {
                     $content = ''; 
              }
         
          $message=stripcslashes($content);
          $message=str_replace("{date}",date("d"),$message);
          $message=str_replace("images/line-break-3.jpg",base_url.'images/email_img/line-break-3.jpg',$message);
          $message=str_replace("images/line-break-2.jpg",base_url.'images/email_img/line-break-2.jpg',$message);

          $rows3 = $db->update("user", array("email_verify"=>1,"code"=>''), $condition,array());

          echo $message;exit;
        }else
        {
          $sel_temp = $db->select("email_template_descriptions","*",array('id'=>'4'),array());
          if($sel_temp['status'] =="success")
              {
                foreach($sel_temp['data'] as $key3)
                {
                    $content = $key3['content'];
                }
              }else
              {
                     $content = ''; 
              }
         
          $message=stripcslashes($content);
          $message=str_replace("{date}",date("d"),$message);
          $message=str_replace("images/line-break-3.jpg",base_url.'images/email_img/line-break-3.jpg',$message);
          $message=str_replace("images/line-break-2.jpg",base_url.'images/email_img/line-break-2.jpg',$message);

          echo $message;exit;
        }


    }else
    {
      // die('Invalid user');
       $rows["message"]= "Invalid verification code";
       $rows["data"]="";
       echoResponse(200, $rows);
    }
    
});

$app->post('/ForgetPassword', function() use ($app){

   // $user_id = $app->request()->params('user_id');

    $email = $app->request()->params('email');

    $condition = array('email'=>$email);

    $time = round(microtime(true) * 1000);

    global $db;
   
    $rows = $db->select("user","*",$condition);
    //print_r($rows);exit;
        if($rows['status'] == "success")
        {
            $code = substr(randomPassword(),0,6);
            
            $sel_temp = $db->select("email_template_descriptions","*",array('id'=>'2'),array());
                    
                    if($sel_temp['status'] =="success")
                    {
                        foreach($sel_temp['data'] as $key3)
                        {
                          $content = $key3['content'];
                        }
                    }else
                    {
                     $content = ''; 
                    }
                  
                    $subject = "Waggingpal App: Forget Password";
                   
                  $message=stripcslashes($content);
                  $message=str_replace("{date}",date("d"),$message);
                  $message=str_replace("images/line-break-3.jpg",base_url.'images/email_img/line-break-3.jpg',$message);
                  $message=str_replace("images/line-break-2.jpg",base_url.'images/email_img/line-break-2.jpg',$message);
                  $message=str_replace("{email}",$email,$message);
                  $message=str_replace("{password}",$code,$message);
                   // echo $message; exit;
                  $email_from='no-repply@app.waggingpal.com';
                  $headers  = 'MIME-Version: 1.0' . "\r\n";
                  $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                  $headers .= 'From: '.$email_from. '\r\n';            // Mail it
                   
                  @mail($email, $subject, $message, $headers);

                  $password= ($code);
                  $newpass = md5($code);

                  $rows3 = $db->update("user", array("password"=>$newpass), $condition,array());

                  $rows["message"] = "Password has been sent to your registered email address";
                  $rows['data'] = '';

        }else
            {
                 $rows["message"] = "Invalid Email Id";
                  $rows["data"]= '';
            }
            echoResponse(200, $rows);
    
});

$app->post('/ChangePassword', function() use ($app) { 
    
   $user_id = $app->request()->params('user_id');
   $current_password = md5($app->request()->params('current_password'));
   $new_password = $app->request()->params('new_password');
    
    $time = round(microtime(true) * 1000);
   
    global $db;
    
    $condition = array('user_id'=>$user_id);
    
    
    $rows1 = $db->select("user","*",$condition);
   
    if($rows1['status'] == "success")
    {
        $password = $rows1['data'][0]['password']; 

        if($current_password == $password)
        {
              $newpassword = md5($new_password);
              
              $data = array(
                      'password'=>$newpassword,
                      'update_at'=>$time
              );

              $rows = $db->update("user",$data,$condition,array());
             
              if($rows['status']='success')
              {die("df");
                  $rows["message"] = "successfull";
                  $rows['data']= $new_password;
              }else
              {
                  $rows["message"] = "failed";
                  $rows['data']= '';
              }
              echoResponse(200, $rows);
    
        }else
        {
            $rows1["message"] = "Old Password not matched";
            $rows1['data']= '';
            echoResponse(200, $rows1);
        }    
    }else
        {
            $rows1["message"] = "user not found";
            $rows1['data']= '';
            echoResponse(200, $rows1);
        }     
});
$app->post('/GetState', function() use ($app){
    
    $country_id = $app->request()->params('country_id');
    
    $search = $app->request()->params('search');

    global $db;
    
    $select =  "SELECT * from region WHERE (Region like '%$search%' OR Code like '%$search%') AND CountryId = '$country_id'";

    $rows = $db->customQuery($select);

    if($rows["status"]=="success")
    {
        foreach ($rows['data'] as $key) {
            
            $arr = $key;
        }

        $rows["message"] = "successfully";
        $rows["data"] = $arr;
    }else
    {
        $rows["message"] = "failed";
        $rows["data"] = '';
    }
    echoResponse(200, $rows);
});

$app->post('/GetCityById', function() use ($app){
    
    $region_id = $app->request()->params('region_id');
    
   // $search = $app->request()->params('search');

    $condition = array('RegionID'=>$region_id);

    global $db;
    
    $rows = $db->select("city","*",$condition);

    if($rows["status"]=="success")
    {
        foreach ($rows['data'] as $key) {
            
            $arr[] = array(
                        'CityId'=>$key['CityId'],
                        'CountryID'=>$key['CountryID'],
                        'RegionID'=>$key['RegionID'],
                        'City'=>$key['City'],
                        );
        
        }

        $rows["message"] = "successfully";
        $rows["data"] = $arr;
    }else
    {
        $rows["message"] = "failed";
        $rows["data"] = '';
    }
    echoResponse(200, $rows);
});

$app->post('/AddPreference', function() use ($app){
    
    $user_id = $app->request()->params('user_id');
    
    $pet_type = $app->request()->params('pet_type');

    $breed = $app->request()->params('breed');

    $gender = $app->request()->params('gender');
    
    $size = $app->request()->params('size');
    
    $age = $app->request()->params('age');

    $condition = array('user_id'=>$user_id);

    global $db;
    
    $rows = $db->select("user","*",$condition);
    
    if($rows["status"]=="success")
    {
        $rows1 = $db->update("user", array("pet_type"=>$pet_type,"breed"=>$breed,"pet_gender"=>$gender,"size"=>$size,"age"=>$age), $condition,array());
       
        if($rows1["status"]=="success")
        {
           $rows2 = $db->select("user","*",$condition);

           if($rows2['status']=="success")
           {
                foreach ($rows2['data'] as $key) {
                    
                    $data = array(
                                'pet_type'=>$key['pet_type'],
                                'breed'=>$key['breed'],
                                'pet_gender'=>$key['pet_gender'],
                                'size'=>$key['size'],
                                'age'=>$key['age'],
                                );
                    }

                $rows2["message"] = "Successfull";
                $rows2["data"]=$data;
           }else
           {
                $rows2["message"] = "Please try again later";
           }
            echoResponse(200, $rows2);
        }
        else
        {
           $rows1["message"] = "Please try again later";
           echoResponse(200, $rows1);
        }
   
    }else
    {
       $rows["message"] = "Please try again later";
       echoResponse(200, $rows);
    }
    
});
























$app->post('/publist', function()use ($app){

    $lat = $app->request()->params('lat');
    $lng = $app->request()->params('lng');


    global $db;
    $rows = $db->select("pubs","*",array());
    if($rows["status"]=="success")
    {
        $rows["message"] = "successfully.";

        
        foreach ($rows['data'] as $key ) 
        {
          
            $pub_id = $key['pub_id'];
            $rows1 = $db->select("pub_events","*",array("pub_id"=>$pub_id));
            foreach ($rows1['data'] as $key1 ) 
            {
                if($key1['pub_event_image'])
                {
                    $pub_event_image=base_url."uploads/pub_events/img/".$key1['pub_event_image'];
                }else
                {
                    $pub_event_image="";
                }

                ${'arr1'.$pub_id}[] = array(
                    "pub_event_id"=>$key1['pub_event_id'],
                    "pub_id"=>$key1['pub_id'],
                    "pub_event_name"=>$key1['pub_event_name'],
                    "pub_event_start_date"=>$key1['pub_event_start_date'],
                    "pub_event_end_date"=>$key1['pub_event_end_date'],
                    "pub_event_image"=>$pub_event_image,
                    "pub_event_venue"=>$key1['pub_event_venue'],
                    "city_id"=>$key1['city_id'],
                    "pub_event_desc"=>$key1['pub_event_desc'],
                    "pub_event_contact_number"=>$key1['pub_event_contact_number'],
                    "pub_event_charge_type"=>$key1['pub_event_charge_type'],
                    "pub_events_featured"=>$key1['pub_events_featured']

                    );

            }
            if(${'arr1'.$pub_id}==null)
            {
                ${'arr1'.$pub_id}=array();
            }

            $rows2 = $db->select("pub_gallery","*",array("pub_id"=>$pub_id,"pub_gallery_status"=>1));
            foreach ($rows2['data'] as $key2 ) 
            {
                if($key2['pub_gallery_image'])
                {
                    $pub_gallery_image=base_url."/uploads/pub_gallery/img/".$key2['pub_gallery_image'];
                }else
                {
                    $pub_gallery_image="";
                }


                ${'arr2'.$pub_id}[] = array(
                    "pub_gallery_id"=>$key2['pub_gallery_id'],
                    "pub_id"=>$key2['pub_id'],
                    "pub_gallery_image"=>$pub_gallery_image
                    );
            }
            if(${'arr2'.$pub_id}==null)
            {
                ${'arr2'.$pub_id}=array();
            }


            $rows3 = $db->select("pub_review","*",array("pub_id"=>$pub_id,"pub_review_status"=>1));
            foreach ($rows3['data'] as $key3 ) 
            {

                $pub_review_id=$key3['pub_review_id'];
                $rows4 = $db->select("pub_review_rating","*",array("pub_review_id"=>$pub_review_id));
                foreach ($rows4['data'] as $key4 ) 
                {
                    $rating_category_id=$key4['rating_category_id'];
                    $rows5 = $db->select("rating_category","*",array("rating_category_id"=>$rating_category_id));
                    foreach ($rows5['data'] as $key5 ) 
                    {
                        $rating_category_name=$key5['rating_category_name'];
                    }

                    ${'arr4'.$pub_review_id}[] = array(
                    "pub_review_rating_id"=>$key4['pub_review_rating_id'],
                    "rating_category_id"=>$key4['rating_category_id'],
                    "rating_category_name"=>$rating_category_name,
                    "pub_review_rate"=>$key4['pub_review_rate']
                    );
                }
                if(${'arr4'.$pub_review_id}==null)
                {
                    ${'arr4'.$pub_review_id}=array();
                }

                if($key3['pub_review_pic1'])
                {
                    $pub_review_pic1=base_url."uploads/pub_review/img/".$key3['pub_review_pic1'];
                }else
                {
                    $pub_review_pic1="";
                }

                if($key3['pub_review_pic2'])
                {
                    $pub_review_pic2=base_url."uploads/pub_review/img/".$key3['pub_review_pic2'];
                }else
                {
                    $pub_review_pic2="";
                }
                if($key3['pub_review_pic3'])
                {
                    $pub_review_pic3=base_url."uploads/pub_review/img/".$key3['pub_review_pic3'];
                }else
                {
                    $pub_review_pic3="";
                }

                ${'arr3'.$pub_id}[] = array(
                    "pub_review_id"=>$key3['pub_review_id'],
                    "pub_review_content"=>$key3['pub_review_content'],
                    "pub_review_pic1"=>$pub_review_pic1,
                    "pub_review_pic2"=>$pub_review_pic2,
                    "pub_review_pic3"=>$pub_review_pic3,
                    "pub_review_date_time"=>$key3['pub_review_date_time'],
                    "name"=>$key3['name'],
                    "email"=>$key3['email'],
                    "mobile"=>$key3['mobile'],
                    "city"=>$key3['city'],
                    "pub_review_rating"=>${'arr4'.$pub_review_id}
                    );
            }
            if(${'arr3'.$pub_id}==null)
            {
                ${'arr3'.$pub_id}=array();
            }


            if($key['pub_cover_pic'])
            {
                $pub_cover_pic=base_url."uploads/pubs/img/".$key['pub_cover_pic'];
            }else
            {
                $pub_cover_pic="";
            }

            $dis_cal = distance($lat,$lng, $key['pub_latitude'], $key['pub_longitude'],'K');

            $arr[]= array(
                    "pub_id"=>$key['pub_id'],
                    "dis_cal"=>round($dis_cal,2),
                    "pub_name"=>$key['pub_name'],
                    "pub_desc"=>$key['pub_desc'],
                    "pub_address"=>$key['pub_address'],
                    "pub_rating"=>$key['pub_rating'],
                    "pub_number_of_rating"=>$key['pub_number_of_rating'],
                    "pub_rating_users"=>$key['pub_rating_users'],
                    "pub_longitude"=>$key['pub_longitude'],
                    "pub_latitude"=>$key['pub_latitude'],
                    "city_id"=>$key['city_id'],
                    "pub_cover_pic"=>$pub_cover_pic,
                    "contact1"=>$key['contact1'],
                    "contact2"=>$key['contact2'],
                    "contact3"=>$key['contact3'],
                    "pub_cuisine"=>$key['pub_cuisine'],
                    "pub_reg_date"=>$key['pub_reg_date'],
                    "events"=>${'arr1'.$pub_id},
                    "pub_gallery"=>${'arr2'.$pub_id},
                    "pub_review"=>${'arr3'.$pub_id}
                        );
          
            //print_r($arr); exit;
        }
        $rows['data']=$arr;

    }else
    {
        $rows["message"] = "no pub";
    }
    echoResponse(200, $rows);

});


function distance($lat1, $lon1, $lat2, $lon2, $unit)
{
      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);
      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
            return $miles;
        }
}


$app->post('/user', function(){

    global $db;
    $rows = $db->select("users","*",array());
    echoResponse(200, $rows);
});

$app->post('/userProfile', function() use ($app){
    $user_id = $app->request()->params('user_id');
    $condition = array('user_id'=>$user_id);

    global $db;
    $rows = $db->select("users","*",$condition);
    if($rows["status"]=="success")
    {
        $rows["message"] = "successfully.";
    }else
    {
        $rows["message"] = "Invalid user_id";
    }
    echoResponse(200, $rows);
});

$app->post('/city', function(){

    global $db;
    $rows = $db->select("cities","*",array());
    if($rows["status"]=="success")
    {
        $rows["message"] = "successfully";
    }else
    {
        $rows["message"] = "no city";
    }
    echoResponse(200, $rows);
});



$app->post('/PubLogin', function() use ($app) { 
    $email = $app->request()->params('email');
    $password = $app->request()->params('password');

    //$device_type = $app->request()->params('device_type');
    //$device_token = $app->request()->params('device_token');
    //$lat = $app->request()->params('lat');
    //$lng = $app->request()->params('lng');

    $condition = array('pub_admin_email'=>$email,'pub_admin_password'=>$password);
    global $db;
    $rows = $db->select("pub_admins","*",$condition);

    if($rows["status"]=="success")
    {
        //$rows3 = $db->update("pub_admins", array("device_type"=>$device_type,"device_token"=>$device_token), $condition,array());
        $rows["message"] = "Login successfully.";
        foreach ($rows['data'] as $key) 
        {  
            if($key['pub_admin_profile_image'])
            {
                $pub_admin_profile_image=base_url."uploads/pub_admin/img/".$key['pub_admin_profile_image'];
            }else
            {
                $pub_admin_profile_image="";
            }
            $arr = array(
                "pub_admin_id"=>$key['pub_admin_id'],
                "pub_admin_name"=>$key['pub_admin_name'],
                "pub_admin_email"=>$key['pub_admin_email'],
                "pub_id"=>$key['pub_id'],
                "pub_admin_mobile"=>$key['pub_admin_mobile'],
                "pub_admin_gender"=>$key['pub_admin_gender'],
                "pub_admin_profile_image"=>$pub_admin_profile_image
                );
        }
        $rows['data']=$arr;

    }else
    {
        $rows["message"] = "Invalid Credential";
    }
        
    echoResponse(200, $rows);
});




$app->post('/SocialSignIn', function() use ($app) { 
    $email = $app->request()->params('email');
    //$user_password = $app->request()->params('password');
    $user_name = $app->request()->params('name');
    $user_mobile = $app->request()->params('mobile');
    $user_gender = $app->request()->params('gender');
    $user_dob = $app->request()->params('dob');
    $user_profile_image = $app->request()->params('profile_image');
    $user_authentication_type = $app->request()->params('user_authentication_type');
    $device_type = $app->request()->params('device_type');
    $device_token = $app->request()->params('device_token');
    $lat = $app->request()->params('lat');
    $lng = $app->request()->params('lng');
   
    $condition = array('user_email'=>$email);
    $data = array('user_email'=>$email,
                    //'user_password'=>$user_password,
                    'user_name'=>$user_name,
                    'user_mobile'=>$user_mobile,
                    'user_gender'=>$user_gender,
                    'user_dob'=>$user_dob,
                    'user_authentication_type'=>$user_authentication_type,
                    'user_reg_date'=>dateTime,
                    'user_profile_image'=>$user_profile_image,
                    'device_type'=>$device_type,
                    'device_token'=>$device_token,
                    'lat'=>$lat,
                    'lng'=>$lng

        );

    global $db;
    $rows = $db->select("users","*",$condition);

    if($rows["status"]=="success")
    {
        $rows3 = $db->update("users", $data, $condition,array());
        if($rows3["status"]=="success")
        {
            $rows4 = $db->select("users","*",$condition);

            if($rows4["status"]=="success")
            {
                $rows4["message"] = "successfully Sign In";
                echoResponse(200, $rows4);
            }
        }
    }else
    {
        $rows1 = $db->insert("users", $data, array());
        if($rows1["status"]=="success")
        {
            $rows2 = $db->select("users","*",$condition);

            if($rows2["status"]=="success")
            {
                $rows2["message"] = "successfully Sign In";
                echoResponse(200, $rows2);
            }
           /* $rows1["message"] = "successfully Sign In";
            $rows1["user_id"] = $rows1["data"];

            echoResponse(200, $rows1);*/
        }else
            {
                $rows1["message"] = "some error";
                echoResponse(200, $rows1);
            }
    }
        
    
});

$app->post('/PubData', function() use ($app) { 
    $pub_id = $app->request()->params('pub_id');
   
    $condition = array('pub_id'=>$pub_id);
    global $db;
    $rows = $db->select("pubs","*",$condition);

    if($rows["status"]=="success")
    {
        $rows["message"] = "successfully.";
        foreach ($rows['data'] as $key ) 
        {
          
            $pub_id = $key['pub_id'];
            $rows1 = $db->select("pub_events","*",array("pub_id"=>$pub_id));
            foreach ($rows1['data'] as $key1 ) 
            {
                if($key1['pub_event_image'])
                {
                    $pub_event_image=base_url."uploads/pub_events/img/".$key1['pub_event_image'];
                }else
                {
                    $pub_event_image="";
                }

                $start = $key1['pub_event_start_date'];
                $end = $key1['pub_event_end_date'];

                $start_datetime = explode(" ",$start);
                $start_date = $start_datetime[0];
                $start_time = $start_datetime[1];

                $end_datetime = explode(" ",$end);
                $end_date = $end_datetime[0];
                $end_time = $end_datetime[1];

                $arr1[] = array(
                    "pub_event_id"=>$key1['pub_event_id'],
                    "pub_id"=>$key1['pub_id'],
                    "pub_event_name"=>$key1['pub_event_name'],
                    "start_date"=>$start_date,
                    "start_time"=>$start_time,
                    "end_date"=>$end_date,
                    "end_time"=>$end_time,
                    "pub_event_image"=>$pub_event_image,
                    "pub_event_venue"=>$key1['pub_event_venue'],
                    "city_id"=>$key1['city_id'],
                    "pub_event_desc"=>$key1['pub_event_desc'],
                    "pub_event_contact_number"=>$key1['pub_event_contact_number'],
                    "pub_event_charge_type"=>$key1['pub_event_charge_type'],
                    "pub_events_featured"=>$key1['pub_events_featured']

                    );

            }
            if($arr1==null)
            {
                $arr1=array();
            }

            $rows2 = $db->select("pub_gallery","*",array("pub_id"=>$pub_id,"pub_gallery_status"=>1));
            foreach ($rows2['data'] as $key2 ) 
            {
                if($key2['pub_gallery_image'])
                {
                    $pub_gallery_image=base_url."uploads/pubs/img/".$key2['pub_gallery_image'];
                }else
                {
                    $pub_gallery_image="";
                }
                $arr2[] = array(
                    "pub_gallery_id"=>$key2['pub_gallery_id'],
                    "pub_id"=>$key2['pub_id'],
                    "pub_gallery_image"=>$pub_gallery_image
                    );
            }

            if($arr2==null)
            {
                $arr2=array();
            }



            $rows3 = $db->select("offers","*",array("pub_id"=>$pub_id));
            foreach ($rows3["data"] as $key3) 
            {
                $offer_template_id= $key3['offer_template_id'];
                $rows4 = $db->select("offer_templates","*",array('offer_template_id'=>$offer_template_id,'offer_template_status'=>1));
                foreach ($rows4['data'] as $key4) 
                {   
                    if($key4['offer_template_image'])
                    {
                        $offer_template_image= base_url."uploads/offer_templates/img/".$key4['offer_template_image'];
                    }else
                    {
                        $offer_template_image="";
                    }
                    $arr4=array(
                        "offer_template_id"=>$key4['offer_template_id'],
                        "offer_template_name"=>$key4['offer_template_name'],
                        "offer_template_desc"=>$key4['offer_template_desc'],
                        "offer_template_image"=>$offer_template_image
                        );
                }
                $arr3[]=array(
                    'offer_id'=>$key3['offer_id'],
                    'offer_template_id'=>$key3['offer_template_id'],
                    'pub_id'=>$key3['pub_id'],
                    'offer_start_date'=>$key3['offer_start_date'],
                    'offer_end_date'=>$key3['offer_end_date'],
                    'offer'=>$arr4,
                    );
            }

            if($arr3==null)
            {
                $arr3=array();
            }
         

            $rows5 = $db->select("menu_category","*",array("pub_id"=>$pub_id,"menu_category_status"=>1));
            foreach ($rows5["data"] as $key5) 
            {
                $menu_category_id= $key5['menu_category_id'];
                $rows6 = $db->select("pub_menu","*",array('menu_category_id'=>$menu_category_id));
                foreach ($rows6['data'] as $key6) 
                {   
                    if($key6['pub_menu_image'])
                    {
                        $pub_menu_image= base_url."uploads/menu_item/img/".$key6['pub_menu_image'];
                    }else
                    {
                        $pub_menu_image="";
                    }
                    ${'arr6'.$menu_category_id}[]=array(
                        "pub_menu_id"=>$key6['pub_menu_id'],
                        "pub_menu_title"=>$key6['pub_menu_title'],
                        "pub_menu_price"=>$key6['pub_menu_price'],
                        "pub_menu_order"=>$key6['pub_menu_order'],
                        "pub_menu_item_type"=>$key6['pub_menu_item_type'],
                        "item_liquor_ingredient"=>$key6['item_liquor_ingredient'],
                        "pub_best_drink"=>$key6['pub_best_drink'],
                        "pub_menu_image"=>$pub_menu_image
                        );
                }
                $arr5[]=array(
                    'menu_category_id'=>$key5['menu_category_id'],
                    'menu_category_name'=>$key5['menu_category_name'],
                    'menu'=>${'arr6'.$menu_category_id},
                    );
            }

            if($arr5==null)
            {
                $arr5=array();
            }




            $arr[]= array(
                    "pub_id"=>$key['pub_id'],
                    "pub_name"=>$key['pub_name'],
                    "pub_desc"=>$key['pub_desc'],
                    "pub_address"=>$key['pub_address'],
                    "pub_rating"=>$key['pub_rating'],
                    "pub_number_of_rating"=>$key['pub_number_of_rating'],
                    "pub_rating_users"=>$key['pub_rating_users'],
                    "pub_longitude"=>$key['pub_longitude'],
                    "pub_latitude"=>$key['pub_latitude'],
                    "city_id"=>$key['city_id'],
                    "pub_cover_pic"=>$pub_cover_pic,
                    "contact1"=>$key['contact1'],
                    "contact2"=>$key['contact2'],
                    "contact3"=>$key['contact3'],
                    "pub_cuisine"=>$key['pub_cuisine'],
                    "pub_reg_date"=>$key['pub_reg_date'],
                    "events"=>$arr1,
                    "pub_gallery"=>$arr2,
                    "offers"=>$arr3,
                    "menu_category"=>$arr5,


                        );
          
            //print_r($arr); exit;
        }
        $rows['data']=$arr;

    }else
    {
        $rows["message"] = "Invalid Pub Id";
    }
    echoResponse(200, $rows);
        
    
});




$app->post('/eventList', function(){
    global $db;
    $rows = $db->select("pub_events","*",array());
    if($rows["status"]=="success")
    {
        $rows["message"] = "successfully.";
        foreach ($rows["data"] as $key) 
        {
            $pub_event_id=$key['pub_event_id'];
            $rows1 = $db->select("pub_event_gallery","*",array("pub_event_id"=>$pub_event_id,"pub_event_gallery_status"=>1));
            foreach ($rows1["data"] as $key1) 
            {
                if($key1['pub_event_gallery_image'])
                {
                    $pub_event_gallery_image= base_url."uploads/pub_event_gallery/img/".$key1['pub_event_gallery_image'];
                }else
                {
                    $pub_event_gallery_image="";
                }

                ${"arr1".$pub_event_id}[]= array(
                    'pub_event_gallery_id'=>$key1['pub_event_gallery_id'],
                    'pub_event_gallery_image'=>$pub_event_gallery_image
                    );
            }
            if(${"arr1".$pub_event_id}=="")
            {
                ${"arr1".$pub_event_id}=array();
            }

            if($key['pub_event_image'])
                {
                    $pub_event_image= base_url."uploads/pub_events/img/".$key['pub_event_image'];
                }else
                {
                    $pub_event_image="";
                }

            $arr[]=array(
                'pub_event_id'=>$key['pub_event_id'],
                'pub_id'=>$key['pub_id'],
                'pub_event_name'=>$key['pub_event_name'],
                'pub_event_start_date'=>$key['pub_event_start_date'],
                'pub_event_end_date'=>$key['pub_event_end_date'],
                'pub_event_image'=>$pub_event_image,
                'pub_event_venue'=>$key['pub_event_venue'],
                'city_id'=>$key['city_id'],
                'pub_event_desc'=>$key['pub_event_desc'],
                'pub_event_contact_number'=>$key['pub_event_contact_number'],
                'pub_event_charge_type'=>$key['pub_event_charge_type'],
                'pub_events_featured'=>$key['pub_events_featured'],
                "pub_event_gallery"=>${"arr1".$pub_event_id}
                );


        }
         $rows['data']=$arr;
    }else
    {
        $rows["message"] = "no event";
    }
    echoResponse(200, $rows);

});

$app->post('/offerList', function(){
    global $db;
    $rows = $db->select("offers","*",array());
    if($rows["status"]=="success")
    {
        $rows["message"] = "successfully.";
        foreach ($rows["data"] as $key) 
        {
            $offer_template_id= $key['offer_template_id'];
            $rows1 = $db->select("offer_templates","*",array('offer_template_id'=>$offer_template_id,'offer_template_status'=>1));
           // print_r($rows1);exit;
            foreach ($rows1['data'] as $key1) 
            {   
                if($key1['offer_template_image'])
                {
                    $offer_template_image= base_url."uploads/offer_templates/img/".$key1['offer_template_image'];
                }else
                {
                    $offer_template_image="";
                }
                $arr1=array(
                    "offer_template_id"=>$key1['offer_template_id'],
                    "offer_template_name"=>$key1['offer_template_name'],
                    "offer_template_desc"=>$key1['offer_template_desc'],
                    "offer_template_image"=>$offer_template_image
                    );
            }
            $arr[]=array(
                'offer_id'=>$key['offer_id'],
                'offer_template_id'=>$key['offer_template_id'],
                'pub_id'=>$key['pub_id'],
                'offer_start_date'=>$key['offer_start_date'],
                'offer_end_date'=>$key['offer_end_date'],
                'offer'=>$arr1,
                );


        }
         $rows['data']=$arr;

    }else
    {
        $rows["message"] = "no event";
    }
    echoResponse(200, $rows);

});


$app->post('/GetOfferTemplates', function(){
    global $db;
    $rows = $db->select("offer_templates","*",array());
    if($rows["status"]=="success")
    {
        $rows["message"] = "successfully.";
    }else
    {
        $rows["message"] = "no offer templates";
    }
    echoResponse(200, $rows);

});




$app->post('/CheckIn', function()use ($app) 
{ 
    $user_id = $app->request()->params('user_id');
    $pub_id = $app->request()->params('pub_id');
    $checkin_status = $app->request()->params('checkin_status');
    global $db;
    $data=array('user_id' =>$user_id,'pub_id' =>$pub_id,'checkin_status' =>$checkin_status,'checkin_date_time'=>dateTime);
   
    $rows1 = $db->insert("checkin", $data, array());
        if($rows1["status"]=="success")
        {
            $rows1["message"] = "successfully checkin";
        }else
            {
                $rows1["message"] = "some error";
            }
    echoResponse(200, $rows1);

});

$app->post('/GetRatingCategory', function()
{
    global $db;
    $rows = $db->select("rating_category","*",array("rating_category_status"=>1));
    if($rows["status"]=="success")
    {
        $rows["message"] = "successfully.";
        foreach ($rows['data'] as $key) 
        {
            //print_r($key);exit;
            $arr[] =array(
                'rating_category_id'=>$key['rating_category_id'],
                'rating_category_name'=>$key['rating_category_name']
                );

        }
        $rows['data']=$arr;
    }else
    {
        $rows["message"] = "no category";
    }
    echoResponse(200, $rows);

});


$app->post('/AddTestimonial', function()use ($app) 
{
    $json1 = file_get_contents('php://input');
    $data = json_decode($json1);

    global $db;
   // print_r($data);exit;

    $ins_data = array('pub_id'=>$data->pub_id,
                    'pub_review_content'=>$data->comment,
                    'name'=>$data->name,
                    'email'=>$data->email,
                    'mobile'=>$data->mobile,
                    'city'=>$data->city,
                    //'pub_review_pic1'=>$pub_review_pic1,
                    //'pub_review_pic2'=>$pub_review_pic2,
                    //'pub_review_pic3'=>$pub_review_pic3,
                    'pub_review_date_time'=>dateTime,                    

        );
    //print_r($ins_data);exit;
    $rows1 = $db->insert("pub_review", $ins_data, array());
    if($rows1["status"]=="success")
    {
        $pub_review_id=$rows1["data"];
        foreach ($data->rating as $key) 
        {
            //echo $key->cat_id;exit;
            $ins_cat = array(
                "rating_category_id"=>$key->cat_id,
                "pub_review_rate"=>$key->data,
                "pub_review_id"=>$pub_review_id
            );
            $rows2 = $db->insert("pub_review_rating", $ins_cat, array());
        }
        $rows2["message"] = "successfully.";
        echoResponse(200, $rows2);

    }else
    {
    $rows1["message"] = "Review Not added";
    echoResponse(200, $rows1);
    }      

});




function echoResponse($status_code, $response) {
    global $app;
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response,JSON_NUMERIC_CHECK);
}

$app->run();
?>