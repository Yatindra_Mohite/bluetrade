<?php
require '.././libs/Slim/Slim.php';
require_once 'dbHelper.php';
require_once 'auth.php';
require_once 'gcm.php';

//GCM key = AIzaSyDelbOBvjrceX1_6j2vZA9p0NcFMEiCGE8

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app = \Slim\Slim::getInstance();
$db = new dbHelper();

date_default_timezone_set("Asia/Kolkata");
$base_url = "http://base3.engineerbabu.com:8282/blue_trade_api/"; 
$dateTime = date("Y-m-d H:i:s"); 
$militime=round(microtime(true) * 1000);
define('dateTime', $dateTime);
define('base_url', $base_url);
define('militime', $militime);


$app->post('/signup',function() use ($app){
	$json1 = file_get_contents('php://input');
	if(!empty($json1))
	{
	    $data = json_decode($json1);
		$email= $data->user_email;
		$password = $data->user_password;
		
		if(!empty($email) && !empty($password) && !empty($data->user_name) && strlen($password) >= 6)
		{
			global $db;
			$code = '1234'; //substr(randomuniqueCode(),0,6);
			$data->user_password = sha1($password);
			$condition = array('user_email'=>$email);
			$query_login = $db->select("user","*",$condition);
			//sendsms($mobile,$Otp);
			if($query_login["status"] == "success")
			{	
				if($query_login['data'][0]['admin_status'] == 1)
				{
					if($query_login['data'][0]['email_status']==1)
					{
			            $query_login['status'] = "failed";
						$query_login['message'] ="Email already exists! please login.";
						unset($query_login['data']);
						echoResponse(200,$query_login);
			    	}
			    	else
					{
						$updateuser = $db->update("user",$data,array('user_id'=>$query_login['data'][0]['user_id']),array());
						$query_login['status']="unverified_user";
			            $query_login["message"] = "Please check your email we have already sent verification link on your registered email id (".$email.") OR click on resend button for email verification link.";
						unset($query_login['data']);
			            echoResponse(200, $query_login);
					}
				}else
				{
					$query_login['status'] = "failed";
					$query_login['message'] = "Your BlueTrade account has been temporarily suspended as a security precaution.";
					unset($query_login['data']);
					echoResponse(200,$query_login);	
				}
			}
			else
			{ 
				$data->email_code = $code;
				$data->create_date = date("Y-m-d H:i:s");
				$insert_user = $db->insert("user",$data,array());
				if($insert_user["status"] == "success")
				{
					$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'1'),array());
		            if($sel_temp['status'] =="success")
		            {
		                foreach($sel_temp['data'] as $key3)
		                {
		                    $content = $key3['content'];
		                }
		            }else
	              	{
	                    $content = ''; 
	              	}
		            $subject = "Blue Trade App: Verification Link";
	               	
			        $message=stripcslashes($content);
	              	$message=str_replace("{date}",date("d"),$message);
	              	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
	              	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
	              	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	              	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);
		          	$message=str_replace("{email}",$email,$message);
	              	$message=str_replace("{link}","<a href=".base_url."api/v1/Authentication.php/VerifyEmail?secretid=".base64_encode($insert_user['data'])."&secret_key=".hash('sha256',$code).">CLICK HERE</a>",$message);
		            $email_from ='no-reply@bluetrade.com';
		        	$headers  = 'MIME-Version: 1.0' . "\r\n";
		            $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
		            $headers .= 'From: '.$email_from. '\r\n';
		            $cc = '';
		           
		        	@mail($email, $subject, $message,$headers);
	           		$insert_user['status']="success";
		            $insert_user["message"] = "Successfully registered & verification link has been sent to your registered email address (".$email.").";
					$insert_user['data'] = array('user_id'=>$insert_user['data']);
		            echoResponse(200, $insert_user);
			   	}else
				{
					$insert_user['status']="failed";
		            $insert_user["message"] = "Something went wrong! Please try again later.";
					unset($insert_user['data']);
		            echoResponse(200, $insert_user);
				}
			}
		}
		else
		{
			$check_otp['status']="failed";
			$check_otp['message']= "Invalid Request parameter";
			echoResponse(200,$check_otp);
		}
	}else
	{
		$check_otp['status']="failed";
		$check_otp['message'] ="No Request parameter";
		echoResponse(200,$check_otp);
	}
});

$app->post('/resend_email_link', function() use ($app)
{
	$json1 = file_get_contents('php://input');
	if(!empty($json1))
	{
		$data = json_decode($json1);
		if(!empty($data->user_email))
		{
			$code = '1234'; //substr(randomuniqueCode(),0,6);
			global $db;
			$query_login = $db->select("user","*",array('user_email'=>$data->user_email));
			if($query_login['status']=="success")
			{		
				if($query_login['data'][0]['admin_status'] == 1)
				{
					$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'1'),array());
		            if($sel_temp['status'] =="success")
		            {
		                foreach($sel_temp['data'] as $key3)
		                {
		                    $content = $key3['content'];
		                }
		            }else
		          	{
		                $content = ''; 
		          	}
		            $subject = "Blue Trade App: Verification Link";
		           	
			        $message=stripcslashes($content);
		          	$message=str_replace("{date}",date("d"),$message);
		          	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
		          	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
		          	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
		          	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);
		          	$message=str_replace("{email}",$data->user_email,$message);
		          	$message=str_replace("{link}","<a href=".base_url."api/v1/Authentication.php/VerifyEmail?secretid=".base64_encode($query_login['data'][0]['user_id'])."&secret_key=".hash('sha256',$code).">CLICK HERE</a>",$message);
		            $email_from ='no-reply@bluetrade.com';
		        	$headers  = 'MIME-Version: 1.0' . "\r\n";
		            $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
		            $headers .= 'From: '.$email_from. '\r\n';
		            $cc = '';
					$update = $db->update("user",array("email_code"=>$code),array("user_id"=>$query_login['data'][0]['user_id']),array());           
		        	@mail($data->user_email, $subject, $message,$headers);
	           		$query_login['status']="success";
		            $query_login["message"] = "Verification link has been sent to your registered email address (".$data->user_email.").";
					$query_login['data'] = array('user_id'=>$query_login['data'][0]['user_id']);
		            echoResponse(200, $query_login);
		       	}else
				{
					$query_login['status'] = "failed";
					$query_login['message'] = "Your BlueTrade account has been temporarily suspended as a security precaution.";
					unset($query_login['data']);
					echoResponse(200,$query_login);	
				}
			}else
			{
				$query_login['status'] = "failed";
				$query_login['message'] = "Email address does not exists";
				unset($query_login['data']);
				echoResponse(200,$query_login);
			}
		}else
		{
			$check_otp['status']="failed";
			$check_otp['message']= "Invalid Request parameter";
			echoResponse(200,$check_otp);
		}
	}else
	{
		$check_otp['status']="failed";
		$check_otp['message'] ="No Request parameter";
		echoResponse(200,$check_otp);
	}
});

$app->post('/Mobile_verification', function() use ($app){
    $json1 = file_get_contents('php://input');
	if(!empty($json1))
	{
		$data = json_decode($json1);
	    if($data->user_id != '' && $data->user_id != 0 && $data->verify_code != '' && strlen($data->verify_code) == 4)
	    {
	    	global $db;
		    $code = '1234';
		    $rows = $db->select("BlueTrade_User","*",array('User_id'=>$data->user_id));
		    if($rows["status"]=="success")
		    {
		    	//echo hash('sha256', $data->verify_code);exit;
		      	if($rows["data"][0]['Otp_code'] == hash('sha256', $data->verify_code))
		      	{
		      		$update = $db->update("BlueTrade_User",array('Otp_code'=>'','mobile_status'=>1,'Update_at'=>militime),array('User_id'=>$data->user_id),array());
		      		if($update['status']=="success")
		      		{
		      			$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'1'),array());
			            if($sel_temp['status'] =="success")
			            {
			                foreach($sel_temp['data'] as $key3)
			                {
			                    $content = $key3['content'];
			                }
			            }else
		              	{
		                    $content = ''; 
		              	}
			            $subject = "Blue Trade App: Verification Link";
		               	
				        $message=stripcslashes($content);
		              	$message=str_replace("{date}",date("d"),$message);
		              	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
		              	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
		              	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
		              	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);
			          	$message=str_replace("{email}",$rows["data"][0]['Email'],$message);
		              	$message=str_replace("{link}","<a href=".base_url."api/v1/blue_trade_api.php/VerifyEmail?secretid=".base64_encode($data->user_id)."&secret_key=".hash('sha256',$code).">CLICK HERE</a>",$message);
			            $email_from ='no-reply@bluetrade.com';
			        	$headers  = 'MIME-Version: 1.0' . "\r\n";
			            $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
			            $headers .= 'From: '.$email_from. '\r\n';
			            $cc = '';
			           
			        	if(@mail($rows["data"][0]['Email'], $subject, $message,$headers))
				           {
				           		$update['status']="success";
					            $update["message"] = "Mobile Number Successfully Verified";
								unset($update['data']);
					            echoResponse(200, $update);
				           }else
				           {
				           		$update['status'] = "failed";
								$update['message'] ="failed";
								unset($update['data']);
								echoResponse(200,$update);
				           }
		      		}
		      	}else
		      	{
		      		$rows['status']="failed";
		            $rows["message"] = "Otp not matched.";
					unset($rows['data']);
		            echoResponse(200, $rows);
		      	}     
		    }else
		    {
		       $rows["status"] = 'failed';
		       $rows["message"]= "Invalid Request";
		       unset($rows["data"]);
		       echoResponse(200, $rows);
		    }
	    }else
	    {
	    	$check_otp['message']= "Invalid Request parameter";
			echoResponse(200,$check_otp);
	    }
	}else
	{
		$check_otp['message'] ="No Request parameter";
		echoResponse(200,$check_otp);
	}
}); //OLD

$app->post('/login',function() use ($app){
	$array_output =array();
	$json1 = file_get_contents('php://input');
	if(!empty($json1))
	{
	    $data = json_decode($json1);
		$email = $data->user_email;
		$password = $data->user_password;
		global $db;
		if(!empty($email) && !empty($password))
		{ 
			$token = bin2hex(openssl_random_pseudo_bytes(16));
			$token = sha1($token.militime);
			$query_login = $db->select("user","*",array("user_email"=>$email,"user_password"=>sha1($password)));
			if($query_login["status"] == "success")
			{	
				if($query_login['data'][0]['admin_status']==1)
				{
					if($query_login['data'][0]['email_status']==1)
					{
						$update = $db->update("user",array('user_device_type'=>$data->user_device_type,'user_device_id'=>$data->user_device_id,'user_device_token'=>$data->user_device_token,'user_token'=>$token),array("user_email"=>$email),array());
						if(!empty($query_login['data'][0]['user_image'])){
							$image = base_url.'uploads/user_image/'.$query_login['data'][0]['user_image'];
						}else{
							$image = '';
						}
						$arr = array(
									'user_id'=>$query_login['data'][0]['user_id'],	
									'user_mobile'=>$query_login['data'][0]['user_mobile'],	
									'user_mobile_code'=>$query_login['data'][0]['user_mobile_code'],	
									'user_name'=>$query_login['data'][0]['user_name'],	
									'user_image'=>$image,
									'user_email'=>$query_login['data'][0]['user_email'],	
									'user_dob'=>$query_login['data'][0]['user_dob'],	
									'user_gender'=>$query_login['data'][0]['user_gender'],	
									'user_token'=>$token	
									);
						$query_login['status'] ="success";
						$query_login['message'] ="Successfully Login";
						$query_login['data'] = $arr;
						echoResponse(200,$query_login);
					}else
					{
						$query_login['status']="unverified_user";
			            $query_login["message"] = "Please check your email we have already sent verification link on your registered email id (".$email.") OR click on resend button for email verification link.";
						$query_login['name']=$query_login['data'][0]['user_name'];
						unset($query_login['data']);
						echoResponse(200,$query_login);
					}
				}else
				{
					$query_login['status'] = "failed";
					$query_login['message'] = "Your BlueTrade account has been temporarily suspended as a security precaution.";
					unset($query_login['data']);
					echoResponse(200,$query_login);
				}	
			}
			else
			{ 
				$query_login['status'] = "failed";
				$query_login['message'] ="Invalid Credential.";
				unset($query_login['data']);
				echoResponse(200,$query_login);
			}
		}else
		{
			$array_output['status'] = "failed";
			$array_output['message'] ="Invalid Request parameter";
			echoResponse(200,$array_output);
		}
	}else
	{
		$array_output['status'] = "failed";
		$array_output['message'] ="No Request parameter";
		echoResponse(200,$array_output);
	}
});

$app->post('/forgot_password', function() use ($app){

	$json1 = file_get_contents('php://input');
	if(!empty($json1))
	{
		$json_output  = array();
		$data = json_decode($json1);
		if(!empty($data->user_email))
		{
			$email = $data->user_email;
			$condition = array('user_email'=>$email);
			global $db;
			$rows = $db->select("user","user_name,user_id,user_email,admin_status,email_status",$condition);
			//print_r($rows);exit;
		    if($rows['status'] == "success")
		    {
				if($rows['data'][0]['admin_status'] == 1)
				{	
					if($rows['data'][0]['email_status'] == 1)
					{
						$userid = $rows['data'][0]['user_id'];
			            $sel_temp = $db->select("email_template_descriptions","*",array('id'=>'2'),array());
			                
			                if($sel_temp['status'] =="success")
			                {
			                    foreach($sel_temp['data'] as $key3)
			                    {
			                      $content = $key3['content'];
			                    }
			                }else
			                {
			                 $content = '';
			                }
				        $subject = "Blue Trade App: Forgot Password";
				           
				        $message=stripcslashes($content);
				        $message=str_replace("{date}",date("d"),$message);
				        $message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
				        $message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
				        $message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
			            $message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);
				        $message=str_replace("{email}",$email,$message);
				        $message=str_replace("{link}","<a href=".base_url."api/v1/web/temp.php?auth_key=".base64_encode($userid)."&id=".militime."&uniqid=".hash('sha256', '1234').">CLICK HERE</a>",$message);
				          //$message=str_replace("{password}",$code,$message);
				           // echo $message; 
				        $email_from='no-reply@bluetrade.com';
				        $headers  = 'MIME-Version: 1.0' . "\r\n";
				        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				        $headers .= 'From: '.$email_from.'\r\n';            // Mail it
				           
				         @mail($email, $subject, $message, $headers);
				          $rows3 = $db->update("user", array("email_code"=>hash('sha256', '1234')), $condition,array());
				          $json_output["status"] = "success";
				          $json_output["message"] = "Reset password link has been sent to your registered email address (".$email.").";
				          $json_output['name'] = $rows['data'][0]['user_name'];
				          unset($rows['data']);
		    		}else{
				        $json_output['status']="unverified_user";
				        $json_output["message"] = "Please check your email we have already sent verification link on your registered email id (".$email.") OR click on resend button for email verification link.";
				        $json_output['name'] = $rows['data'][0]['user_name'];
				        unset($rows['data']);
		    		}
	    			echoResponse(200, $json_output);
		    	}else{
			        $json_output["status"] = "failed";
			        $json_output["message"] = "Your BlueTrade account has been temporarily suspended as a security precaution.";
				    unset($rows['data']);
		    		echoResponse(200, $json_output);
		    	}
		    }else{

		        $json_output["status"] = "failed";
		        $json_output["message"] = "Email does not exists.";
			    unset($rows['data']);
		    	echoResponse(200, $json_output);
		    }
		}else
		{
			$json_output['status']="failed";
			$json_output['message']= "Invalid Request parameter";
			echoResponse(200,$json_output);
		}
	}else
	{
		$json_output['status']="failed";
		$json_output['message'] ="No Request parameter";
		echoResponse(200,$json_output);
	}
});

$app->post('/update_profile', function() use ($app)
{
  	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
         $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['user_id'];
        	$currntmobile = $check['data'][0]['user_mobile_code'].$check['data'][0]['user_mobile'];
        	
        	$data['user_name'] = $app->request()->params('user_name');
        	$user_mobile_code = $app->request()->params('user_mobile_code');
        	$user_mobile = $app->request()->params('user_mobile');
        	$data['user_dob'] = $app->request()->params('user_dob');
        	$data['user_gender'] = $app->request()->params('user_gender');
        	global $db;
       		$status = 'false';
       		if(!empty($user_mobile_code) && !empty($user_mobile))
       		{
	        	if($currntmobile != $user_mobile_code.$user_mobile)
	        	{
	        		$code='1234';
	        		$checkno = $db->select("user","user_id",array('user_mobile_code'=>$user_mobile_code,'user_mobile'=>$user_mobile,'mobile_status'=>1));
	        		if($checkno['status']=="success")
	        		{
	        			//Mobile no alrady exists please try another no.
	        			$checkno['status'] = "failed";
			            $checkno['message'] = "Mobile no. already exists!";
			            unset($checkno["data"]);
						echoResponse(200,$checkno);
						exit;
	        		}else
	        		{
	        			$status = 'true';
	        		}
	        	}
        		else
        		{
	        		$code='';
	        		$status = 'true';
        		}
	        	if($status=='true')
	        	{
	        		if($code=='')
	        		{
		        		$url = '';
						if(isset($_FILES['user_image']['name']) && $_FILES['user_image']['name'] != '')
						{
							$image = md5(militime.$_FILES['user_image']['name']).'.jpg';
							move_uploaded_file($_FILES['user_image']['tmp_name'], '../../uploads/user_image/'.$image);
							$data['user_image'] = $image;
						}else
						{
							$image = $check['data'][0]['user_image'];
						}
						if($image != '')
						{
							$url = base_url.'uploads/user_image/'.$image;
						}
						$data['update_date'] = dateTime;
	        			$update_info =  $db->update('user',$data,array('user_id'=>$user_id),array());
	        		}else
	        		{
	        			$update_info =  $db->update('user',array('mobile_code'=>$code,'update_date'=>dateTime),array('user_id'=>$user_id),array());
	        		}
	        	}
				if($update_info['status'] == "success")
				{
					$update_info['status'] = "success";
		            $update_info['message'] = "successfully Updated";
		            //$update_info["data"] = array('User_name'=>$name,'user_image'=>$url);
				}
				else
				{
					$update_info['status'] = "failed";
		            $update_info['message'] = "Something went wrong! Please try again later.";
		            unset($update_info["data"]);
				}
				echoResponse(200,$update_info);
       		}else
       		{
       			$json_output['status']="failed";
				$json_output['message']= "Invalid Request parameter";
				echoResponse(200,$json_output);
       		}
	    }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/update_profile_otp', function() use ($app)
{
  	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
         $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['user_id'];
        	$currntmobile = $check['data'][0]['user_mobile_code'].$check['data'][0]['user_mobile'];
        	$user_mobile_code = $app->request()->params('user_mobile_code');
        	$user_mobile = $app->request()->params('user_mobile');
        	$user_otp = $app->request()->params('user_otp');
        	$data['user_name'] = $app->request()->params('user_name');
        	$data['user_dob'] = $app->request()->params('user_dob');
        	$data['user_gender'] = $app->request()->params('user_gender');
        	global $db;
       		$status = 'false';
       		if(!empty($user_mobile_code) && !empty($user_mobile) && !empty($user_otp))
       		{
	        	if($user_otp == $check['data'][0]['mobile_code'])
	        	{
	        		$url = '';
					if(isset($_FILES['user_image']['name']) && $_FILES['user_image']['name'] != '')
					{
						$image = md5(militime.$_FILES['user_image']['name']).'.jpg';
						move_uploaded_file($_FILES['user_image']['tmp_name'], '../../uploads/user_image/'.$image);
						$data['user_image'] = $image;
					}else
					{
						$image = $check['data'][0]['user_image'];
					}
					if($image != '')
					{
						$url = base_url.'uploads/user_image/'.$image;
					}
					$data['user_mobile_code'] = $user_mobile_code;
					$data['user_mobile'] = $user_mobile;
	        		$data['mobile_code'] = '';
	        		$data['mobile_status'] = 1;
	        		$data['update_date'] = dateTime;
	        		$update_info =  $db->update('user',$data,array('user_id'=>$user_id),array());
					if($update_info['status'] == "success")
					{
						$update_info['status'] = "success";
			            $update_info['message'] = "Profile successfully updated.";
			            //$update_info["data"] = array('User_name'=>$name,'user_image'=>$url);
					}
					else
					{
						$update_info['status'] = "failed";
			            $update_info['message'] = "Something went wrong! Please try again later.";
			            unset($update_info["data"]);
					}
					echoResponse(200,$update_info);
	        	}else
	        	{
	        		$json_output['status']="failed";
					$json_output['message']= "Invalid OTP.";
					echoResponse(200,$json_output);
	        	}
       		}else
       		{
       			$json_output['status']="failed";
				$json_output['message']= "Invalid Request parameter";
				echoResponse(200,$json_output);
       		}
	    }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/resend_otp_profile', function() use ($app)
{
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
			$json_output = array();
			$jsonbody = file_get_contents('php://input');
			if(!empty($jsonbody))
			{
				global $db;
				$jsondecode = json_decode($jsonbody);
				if(!empty($jsondecode->code) && !empty($jsondecode->mobile))
				{
			   		$otp = '1234';
			   		$checkmobile =$db->select('user',"user_id",array('user_mobile_code'=>$jsondecode->code,'user_mobile'=>$jsondecode->mobile,'mobile_status'=>1));
			   		if($checkmobile['status']!='success')
			   		{
				        $insert_enquiry = $db->update('user',array('mobile_code'=>$otp,'update_date'=>dateTime),array('user_id'=>$check['data'][0]['user_id']),array());
				        if($insert_enquiry['status'] == 'success')
				        {
				        	$json_output['status'] = "success";
				            $json_output['message'] = "OTP successfully sent.";
				       	}
				        else
				        {
				        	$json_output['status'] = "failed";
				            $json_output['message'] = "Something went wrong! Please try again later.";
				        }
				        echoResponse(200,$json_output);
			   		}else
			   		{
			   			$json_output['status'] = "failed";
				        $json_output['message'] = "Mobile number already registered.";
			   			echoResponse(200,$json_output);
			   		}
				}else
				{
					$json_output['status']="failed";
					$json_output['message']= "Invalid request parameter";
					echoResponse(200,$json_output);
				}
			}else
			{
				$json_output['status']="failed";
				$json_output['message']= "No request parameter";
				echoResponse(200,$json_output);
			}
	    }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/change_password', function() use ($app)
{
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
			if($check['data'][0]['admin_status'] == 1)
			{
				$json_output = array();
				$jsonbody = file_get_contents('php://input');
				if(!empty($jsonbody))
				{
					global $db;
					$jsondecode = json_decode($jsonbody);
					if(!empty($jsondecode->old_password) && !empty($jsondecode->new_password))
					{
				   		if(sha1($jsondecode->old_password) == $check['data'][0]['user_password'])
				   		{
					        $insert_enquiry = $db->update('user',array('user_password'=>sha1($jsondecode->new_password),'update_date'=>dateTime),array('user_id'=>$check['data'][0]['user_id']),array());
					        if($insert_enquiry['status'] == 'success')
					        {
					        	$json_output['status'] = "success";
					            $json_output['message'] = "Password successfully changed.";
					            unset($insert_enquiry['data']);
					        }
					        else
					        {
					        	$json_output['status'] = "failed";
					            $json_output['message'] = "Something went wrong! Please try again later.";
					            unset($insert_enquiry['data']);
					        }
					        echoResponse(200,$json_output);
				   		}else
				   		{
				   			$json_output['status'] = "failed";
					        $json_output['message'] = "Old password does not match.";
				   			echoResponse(200,$json_output);
				   		}
					}else
					{
						$json_output['status']="failed";
						$json_output['message']= "Invalid request parameter";
						echoResponse(200,$json_output);
					}
				}else
				{
					$json_output['status']="failed";
					$json_output['message']= "No request parameter";
					echoResponse(200,$json_output);
				}
			}else
			{
				$json_output["status"] = "failed";
		        $json_output["message"] = "Your BlueTrade account has been temporarily suspended as a security precaution.";
			  	echoResponse(200, $json_output);
			}
	    }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/support', function() use ($app)
{
	$json_output = array();
	$jsonbody = file_get_contents('php://input');
	if(!empty($jsonbody))
	{
		$jsondecode = json_decode($jsonbody);
		global $db;
		if(!empty($jsondecode->email) && !empty($jsondecode->message))
		{
	   		$jsondecode->create_date = date('Y-m-d H:i:s');
	        $insert_enquiry = $db->insert('support',$jsondecode,array());
	        if($insert_enquiry['status'] == 'success')
	        {
	        	$json_output['status'] = "success";
	            $json_output['message'] = "Successfully added.";
	            unset($insert_enquiry['data']);
	        }
	        else
	        {
	        	$json_output['status'] = "failed";
	            $json_output['message'] = "Something went wrong! Please try again later.";
	            unset($insert_enquiry['data']);
	        }
	        echoResponse(200,$json_output);
		}else
		{
			$json_output['status']="failed";
			$json_output['message']= "Invalid request parameter";
			echoResponse(200,$json_output);
		}
	}else
	{
		$json_output['status']="failed";
		$json_output['message']= "No request parameter";
		echoResponse(200,$json_output);
	}
});




$app->post('/check_version',function() use ($app){
	$json_output = array();
	$json = file_get_contents('php://input');
	$json_array = json_decode($json);
	if(isset($json_array->version_code))
	{
		$appversion = $json_array->version_code;
		$type = $json_array->type;
		global $db;
		if($type == 'android')
		{
			$query_login = $db->customQuery("SELECT id FROM app_version WHERE min_android_version <= '$appversion'");
		}else
		{
			$query_login = $db->customQuery("SELECT id FROM app_version WHERE min_ios_version <= '$appversion'");
		}
		if($query_login["status"] == "success")
		{	
			$json_output['status'] = "success";
			$json_output['message'] ="Successfully";
			unset($query_login['data']);
			echoResponse(200,$json_output);
		}else{
			$json_output['status'] = "failed";
			$json_output['message'] ="We no longer support this version, Please update the application.";
			unset($query_login['data']);
			echoResponse(200,$json_output);
		}
	}else
	{
		$json_output['status']="failed";
		$json_output['message'] ="No Request parameter";
		
		echoResponse(200,$json_output);
	}
     
});






//Old api created by nimmi
/*$app->post('/ads_by_location', function() use ($app){
 	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
			$lat = $app->request()->params('lat');
			$lng = $app->request()->params('lng');
			$distance = $app->request()->params('distance');
			global $db;
			$finalarr =array();
			$feat_arr = array();
			$trend_arr  = array();
			$hot_deal_arr = array();
		    $get_featured_ads = $db->customQuery("select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance  FROM Ads HAVING distance <= '$distance' AND `ads_status` = '0'  order by distance ASC limit 0,6");
		        if($get_featured_ads['status']=="success")
		        {
	              	foreach($get_featured_ads['data'] as $key_acc)
	              	{
		                $table_id = $key_acc['table_id'];
		                $img_arr = array();
		                $all_image =  $db->customQuery("select Ads_image.ads_image from `Ads_image` where `ad_id` = '$table_id'");
		                if($all_image['status']=="success")
		                {
							foreach($all_image['data'] as $img)
							{
								$img_arr[] = base_url.'uploads/ads_image/'.$img['ads_image'];
							}
		                }
		                $key_acc['Ads_image'] = $img_arr;
		                $feat_arr[] = $key_acc;
	              	}
		        }
		        $finalarr = array(
		              'featured' => $feat_arr,
		              'trending' => $feat_arr,
		              'hot_deal' => $feat_arr
		            );
		        if(!empty($finalarr))
		        {
		          	$get_featured_ads['status'] = "success";
		            $get_featured_ads['message'] = "successfully";  
		            $get_featured_ads["data"] = $finalarr;
		            echoResponse(200,$get_featured_ads);
		        }
		        else
		        {
		            $get_featured_ads['status'] = "failed";
		            $get_featured_ads['message'] = "failed";
		            unset($get_featured_ads["data"]);
		            echoResponse(200, $get_featured_ads);
		        }
	    }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});*/

//New api created by yatindra
$app->post('/ads_by_location', function() use ($app){
 	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
			$lat = $app->request()->params('lat');
			$lng = $app->request()->params('lng');
			$distance = $app->request()->params('distance');
			global $db;
			$finalarr =array();
			$feat_arr = array();
			$trend_arr  = array();
			$hot_deal_arr = array();
		    //$get_featured_ads = $db->customQuery("select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance  FROM Ads HAVING distance <= '$distance' AND `ads_status` = '0'  order by distance ASC limit 0,6");
		    $featureddata=$db->customQuery("SELECT *, (1.609344 * (( 3959 * acos( cos( radians($lat) ) * cos( radians(lat) ) * cos( radians(lng) - radians($lng)) + sin(radians($lat)) * sin( radians(lat)))))) AS distance FROM Ads where lat !='' and lng !='' and featured=1 and ads_status=0 having distance <= '$distance' order by distance asc limit 0,6 ");
			$trandingdata=$db->customQuery("SELECT *, tranding.views, (1.609344 * (( 3959 * acos( cos( radians($lat) ) * cos( radians(lat) ) * cos( radians(lng) - radians($lng)) + sin(radians($lat)) * sin( radians(lat)))))) AS distance FROM Ads LEFT JOIN tranding on tranding.ads_id = Ads.table_id where lat !='' and lng !='' and ads_status=0 having distance <= '$distance' order by tranding.views DESC, distance asc limit 0,6");
			$hotdealsdata=$db->customQuery("SELECT *, (1.609344 * (( 3959 * acos( cos( radians($lat) ) * cos( radians(lat) ) * cos( radians(lng) - radians($lng)) + sin(radians($lat)) * sin( radians(lat)))))) AS distance FROM Ads where lat !='' and lng !='' and ads_status=0 having distance <= '$distance' order by distance asc limit 0,6");
	        if($trandingdata['status']=="success")
	        {
              	foreach($featureddata['data'] as $key_featur)
              	{
	                $table_id = $key_featur['table_id'];
	                $img_arr = array();
	                $all_image =  $db->customQuery("select Ads_image.ads_image from `Ads_image` where `ad_id` = '$table_id'");
	                if($all_image['status']=="success")
	                {
						foreach($all_image['data'] as $img)
						{
							$img_arr[] = base_url.'uploads/ads_image/'.$img['ads_image'];
						}
	                }
	                $key_featur['Ads_image'] = $img_arr;
	                $feat_arr[] = $key_featur;
              	}
	        }
		    if($trandingdata['status']=="success")
	        {
              	foreach($trandingdata['data'] as $key_trend)
              	{
	                $table_id = $key_trend['table_id'];
	                $img_arr1 = array();
	                $all_image =  $db->customQuery("select Ads_image.ads_image from `Ads_image` where `ad_id` = '$table_id'");
	                if($all_image['status']=="success")
	                {
						foreach($all_image['data'] as $img)
						{
							$img_arr1[] = base_url.'uploads/ads_image/'.$img['ads_image'];
						}
	                }
	                $key_trend['Ads_image'] = $img_arr1;
	                $trend_arr[] = $key_trend;
              	}
	        }
	        if($hotdealsdata['status']=="success")
	        {
              	foreach($hotdealsdata['data'] as $key_hotdeal)
              	{
	                $table_id = $key_hotdeal['table_id'];
	                $img_arr2 = array();
	                $all_image =  $db->customQuery("select Ads_image.ads_image from `Ads_image` where `ad_id` = '$table_id'");
	                if($all_image['status']=="success")
	                {
						foreach($all_image['data'] as $img)
						{
							$img_arr2[] = base_url.'uploads/ads_image/'.$img['ads_image'];
						}
	                }
	                $key_hotdeal['Ads_image'] = $img_arr2;
	                $hot_deal_arr[] = $key_hotdeal;
              	}
	        }
	        $finalarr = array(
		              'featured' => $feat_arr,
		              'trending' => $trend_arr,
		              'hot_deal' => $hot_deal_arr
		            );
	        if(!empty($finalarr))
	        {
	          	$get_featured_ads['status'] = "success";
	            $get_featured_ads['message'] = "successfully";  
	            $get_featured_ads["data"] = $finalarr;
	            echoResponse(200,$get_featured_ads);
	        }
	        else
	        {
	            $get_featured_ads['status'] = "failed";
	            $get_featured_ads['message'] = "failed";
	            unset($get_featured_ads["data"]);
	            echoResponse(200, $get_featured_ads);
	        }
	    }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/insert_ads', function() use ($app){
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
           $user_id = $check['data'][0]['User_id'];
		   $category_id = $app->request()->params('category_id');
		   $sub_category_id = $app->request()->params('sub_category_id');
	 	   $sub_category_name = $app->request()->params('sub_category_name');
	 	   $category_name = $app->request()->params('category_name');
	       $ad_title = $app->request()->params('ad_title');
	       $ad_description = $app->request()->params('ad_description');
	       $user_name = $app->request()->params('user_name');
	       $user_email = $app->request()->params('user_email');
	       $user_mobile = $app->request()->params('user_mobile');
	       $address = $app->request()->params('address');
	       $price = $app->request()->params('price');
	       $currency = $app->request()->params('currency');
	       $lat = $app->request()->params('lat');
	       $lng = $app->request()->params('lng');
	       global $db;
        	$insert_info =  $db->insert('Ads',array('user_id'=>$user_id,'category_id'=>$category_id,'currency'=>$currency,'sub_category_id'=>$sub_category_id,'sub_category_name'=>$sub_category_name,'category_name'=>$category_name,'ad_title'=>$ad_title,'ad_description'=>$ad_description,'user_name'=>$user_name,'user_mobile'=>$user_mobile,'user_email'=>$user_email,'address'=>$address,'price'=>$price,'lat'=>$lat,'lng'=>$lng,'creat_at'=>militime,'posted_date'=>date('d-m-Y')),array());
           	if($insert_info['status']  == 'success')
           	{
           		if(!empty($_FILES["image"]["name"]) || isset($_FILES["image"]["name"]))
          		{
               		$image = $_FILES["image"]["name"];
            	}
          		else
          		{
             		 $image = array();
          		}
          		$img_arr = array();
          			for($i=0; $i<count($image); $i++)
	          		{
	          			$image1 = md5(militime.$image[$i]).'.png';
	          			move_uploaded_file($_FILES["image"]["tmp_name"][$i],"../../uploads/ads_image/".$image1);
	             		$ins_img = $db->insert('Ads_image',array('ad_id'=>$insert_info['data'],'user_id'=>$user_id,'ads_image'=>$image1),array());
	          			$img_arr[] = base_url.'uploads/ads_image/'.$image1;
	          		}
      		 	if($insert_info['status']=="success")
		        {
		            $insert_info['status'] = "success";
		            $insert_info['message'] = "successfully inserted";
		            $insert_info["data"] = array('category_id'=>$category_id,'sub_category_id'=>$sub_category_id,'category_name'=>$category_name,'sub_category_name'=>$sub_category_name,'ad_title'=>$ad_title,'ad_description'=>$ad_description,'user_name'=>$user_name,'user_mobile'=>$user_mobile,'user_email'=>$user_email,'address'=>$address,'price'=>$price,'currency'=>$currency,'lat'=>$lat,'lng'=>$lng,'creat_at'=>date('d-m-Y'),'ads_image'=>$img_arr);
		        }
		        else
		        {
		            $insert_info['status'] = "failed";
		            $insert_info['message'] = "Data Not Inserted Successfully";
		            unset($insert_info["data"]);
		        }
		      echoResponse(200, $insert_info);
	    	}
        	else
        	{
        		$insert_info['status'] = "failed";
	            $insert_info['message'] = "Data Not Inserted...!";
	            unset($insert_info["data"]);
		      echoResponse(200, $insert_info);
        	}
        }else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/update_ads', function() use ($app){
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
           $user_id = $check['data'][0]['User_id'];
		   $ads_id = $app->request()->params('ads_id');
		   $category_id = $app->request()->params('category_id');
		   $sub_category_id = $app->request()->params('sub_category_id');
	 	   $sub_category_name = $app->request()->params('sub_category_name');
	 	   $category_name = $app->request()->params('category_name');
	       $ad_title = $app->request()->params('ad_title');
	       $ad_description = $app->request()->params('ad_description');
	       $user_name = $app->request()->params('user_name');
	       $user_email = $app->request()->params('user_email');
	       $user_mobile = $app->request()->params('user_mobile');
	       $address = $app->request()->params('address');
	       $price = $app->request()->params('price');
	       $currency = $app->request()->params('currency');
	       $lat = $app->request()->params('lat');
	       $lng = $app->request()->params('lng');
	       global $db;
        	$data = array();
        	$select_ad = $db->select("Ads","table_id",array('table_id'=>$ads_id,'user_id'=>$user_id));
        	if($select_ad['status']=="success")
        	{	
          		$img_arr = array();
        		//$updateinfo =  $db->update('Ads',array('ad_title'=>$ad_title,'ad_description'=>$ad_description,'user_mobile'=>$user_mobile,'address'=>$address,'price'=>$price,'lat'=>$lat,'lng'=>$lng,'update_at'=>militime),array('table_id'=>$ads_id),array());
        		$updateinfo =  $db->update('Ads',array('category_id'=>$category_id,'sub_category_id'=>$sub_category_id,'sub_category_name'=>$sub_category_name,'category_name'=>$category_name,'ad_title'=>$ad_title,'ad_description'=>$ad_description,'user_name'=>$user_name,'user_mobile'=>$user_mobile,'user_email'=>$user_email,'address'=>$address,'currency'=>$currency,'price'=>$price,'lat'=>$lat,'lng'=>$lng,'update_at'=>militime),array('table_id'=>$ads_id),array());
        		//$insert_info =  $db->insert('Ads',array('user_id'=>$user_id,'category_id'=>$category_id,'sub_category_id'=>$sub_category_id,'sub_category_name'=>$sub_category_name,'category_name'=>$category_name,'ad_title'=>$ad_title,'ad_description'=>$ad_description,'user_name'=>$user_name,'user_mobile'=>$user_mobile,'user_email'=>$user_email,'address'=>$address,'price'=>$price,'lat'=>$lat,'lng'=>$lng,'creat_at'=>militime,'posted_date'=>date('d-m-Y')),array());
        		if($updateinfo['status']=='success')
        		{
    		   		$selimg = $db->select("Ads_image",'ads_image',array('ad_id'=>$ads_id));
               		if($selimg['status']=="success")
               		{
               			foreach ($selimg['data'] as $key) {

	               				unlink('../../uploads/ads_image/'.$key['ads_image']);
	               			}
	               			$deletee = $db->delete("Ads_image",array('ad_id'=>$ads_id),array());
               		}
               		if(!empty($_FILES["image"]["name"]) && isset($_FILES["image"]["name"]))
      				{
               			$image = $_FILES["image"]["name"];
	               		for($i=0; $i<count($image); $i++)
		          		{
		          			$image1 = md5(militime.$image[$i]).'.png';
		          			move_uploaded_file($_FILES["image"]["tmp_name"][$i],"../../uploads/ads_image/".$image1);
		             		$ins_img = $db->insert('Ads_image',array('ad_id'=>$ads_id,'user_id'=>$user_id,'ads_image'=>$image1),array());
		          			$img_arr[] = base_url.'uploads/ads_image/'.$image1;
		          		}
           			}
        			$select_newad = $db->select("Ads","*",array('table_id'=>$ads_id,'user_id'=>$user_id));
        			if($select_newad['status']=='success')
        			{
           				$select_newad['data'][0]['Ads_image'] = $img_arr;
        				$data11 = $select_newad['data'][0];
        			}
	            	$select_newad['status'] = "success";
		            $select_newad['message'] = "successfully inserted";
		            $select_newad["data"] = $data11;
	            }else
	            {
					$select_newad['status'] = "failed";
		            $select_newad['message'] = "Something went wrong! please try again later.";
		            unset($select_newad["data"]);
	            }
            	echoResponse(200, $select_newad);
        	}else
        	{
        		$select_ad['status'] = "failed";
	            $select_ad['message'] = "Ads not found.";
	            unset($select_ad["data"]);
            	echoResponse(200, $select_ad);
        	}
	    }else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/edit_profile', function() use ($app){

  $headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
         $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$user_name = $check['data'][0]['User_name'];
        	$user_mobile = $check['data'][0]['Contact_num'];
        	$user_password = $check['data'][0]['Password'];

        	$name = $app->request()->params('name');
        	$mobile = $app->request()->params('contact');
        	$old_password = $app->request()->params('old_password');
        	$new_password = $app->request()->params('new_password');
        	
        	global $db;
        	if(empty($name))
        	{
        		$name = $user_name;
        	}
        	
        	if($new_password == '')
        	{
        		$new_password = $user_password;
        	}else
        	{
        		$new_password = hash('sha256', $new_password);
        	}

        	if($old_password != '' && hash('sha256',$old_password) != $user_password)
        	{
        		$check['status'] = "failed";
	            $check['message'] = "Old Password Not Matched";
	            unset($check["data"]);
				echoResponse(200,$check);
        	}else
        	{	
				$url = '';
				if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
				{
					$image = md5(militime.$_FILES['image']['name']).'.jpg';
					move_uploaded_file($_FILES['image']['tmp_name'], '../../uploads/user_image/'.$image);
				}else
				{
					$image = $check['data'][0]['user_image'];
				}
				if($image != '')
				{
					$url = base_url.'uploads/user_image/'.$image;
				}
				if($mobile != $user_mobile)
	        	{
	        		$code='1234';
	        		$checkno = $db->select("BlueTrade_User","User_id",array('Contact_num'=>$mobile,'mobile_status'=>1));
	        		if($checkno['status']=="success")
	        		{
	        			//Mobile no alrady exists please try another no.
	        			$checkno['status'] = "failed";
			            $checkno['message'] = "Mobile no. already exists!";
			            unset($checkno["data"]);
						echoResponse(200,$checkno);
						exit;
	        		}else
	        		{
	        			$type = 'verify_mobile';
	        			$update_info =  $db->update('BlueTrade_User',array('User_name'=>$name,'Otp_code'=>md5($code),'temp_mobile_no'=>$mobile,'user_image'=>$image,'Password'=>$new_password,'Update_at'=>militime),array('User_id'=>$user_id),array());
	        		}
	        	}
	        	else
	        	{
	        		$type = '';
	        		$update_info =  $db->update('BlueTrade_User',array('User_name'=>$name,'user_image'=>$image,'Password'=>$new_password,'Update_at'=>militime),array('User_id'=>$user_id),array());
	        	}
				if($update_info['status'] == "success")
				{
					$update_info['status'] = "success";
		            $update_info['message'] = "successfully Updated";
		            $update_info['type'] = $type;
		            $update_info["data"] = array('User_name'=>$name,'user_image'=>$url);
				}
				else
				{
					$update_info['status'] = "failed";
		            $update_info['message'] = "Data Not Updated...!";
		            unset($update_info["data"]);
				}
				echoResponse(200,$update_info);
			}
	    }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});


$app->post('/enquiry', function() use ($app)
{

  	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
         $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$name = $app->request()->params('name');
        	$email = $app->request()->params('email');
        	$mobile = $app->request()->params('mobile');
        	//print_r($mobile); exit;
        	$message = $app->request()->params('message');
        	global $db;

	        $insert_enquiry = $db->insert('enquiry',array('user_id'=>$user_id,'name'=>$name,'email'=>$email,'mobile'=>$mobile,'message'=>$message),array());
	        if($insert_enquiry['status'] == 'success')
	        {
	        	$insert_enquiry['status'] = "success";
	            $insert_enquiry['message'] = "Successfully Inserted";
	            $insert_enquiry['data'] = array('user_id'=>$user_id,'name'=>$name,'email'=>$email,'mobile'=>$mobile,'message'=>$message); 
	        }
	        else
	        {
	        	$insert_enquiry['status'] = "failed";
	            $insert_enquiry['message'] = "Data Not Inserted..!";
	            unset($insert_enquiry['data']);
	        }
	            echoResponse(200,$insert_enquiry);

        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }

});

$app->post('/view_ads', function() use ($app)
{
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$category_type = $app->request()->params('category_type');
        	$category_id = $app->request()->params('category_id');
        	$sub_category_id = $app->request()->params('sub_category_id');
        	$sort = $app->request()->params('sort');
        	$is_asc = $app->request()->params('is_asc');
        	$lat = $app->request()->params('lat');
        	$lng = $app->request()->params('lng');
        	$distance = $app->request()->params('distance');
        	$page_no = $app->request()->params('page_no');
	        $per_set = 10;
	        $from  = ($page_no-1)*$per_set;
        	global $db;
        	$scat_id = $cat_id = '';
        	if($category_id != 0)
        	{
        		$cat_id = "AND `category_id` = '$category_id'";
        	}
        	if($sub_category_id != 0)
        	{
        		$scat_id = "AND sub_category_id = '$sub_category_id'";
        	}
        	if($sort == 'date')
        	{	
        		$sort = "creat_at";
        	}
        	if($is_asc == 0)
        	{
        		$is_asc1 = "ORDER BY ".$sort." ASC";
        	}
        	else
        	{
        		$is_asc1 ="ORDER BY ".$sort." DESC";
        	}
        	if($category_type == 'feature')
        	{
        		$ads = $db->customQuery("select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance from `Ads` HAVING distance <= '$distance' ".$cat_id." ".$scat_id." AND featured = 1 AND `ads_status` = '0' ".$is_asc1." limit $from,$per_set");
        	}elseif($category_type=='trending')
        	{
				$ads=$db->customQuery("SELECT *, tranding.views, (1.609344 * (( 3959 * acos( cos( radians($lat) ) * cos( radians(lat) ) * cos( radians(lng) - radians($lng)) + sin(radians($lat)) * sin( radians(lat)))))) AS distance FROM Ads INNER JOIN tranding on tranding.ads_id = Ads.table_id where lat !='' and lng !='' ".$cat_id." ".$scat_id." and ads_status=0 having distance <= '$distance' ".$is_asc1." limit $from,$per_set");
        	}else //hotdeal
        	{
        		$ads = $db->customQuery("select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance from `Ads` HAVING distance <= '$distance' ".$cat_id." ".$scat_id." AND `ads_status` = '0' ".$is_asc1." limit $from,$per_set");
        	}
        	 //print_r($is_asc1); exit;
       		//echo "select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance from `Ads` HAVING distance <= '$distance' AND ".$cat_id." ".$is_asc1." limit $from,$per_set"; exit;
        	//$ads = $db->customQuery("select * from `Ads` where ".$cat_id." ".$is_asc1." limit $from,$per_set");
	        if($ads['status'] == 'success')
	        {
	        	foreach($ads['data'] as $key_acc)
	            {
	            	$table_id = $key_acc['table_id'];
	            	$img_arr = array();
	            	$all_image =  $db->customQuery("select Ads_image.ads_image from `Ads_image` where `ad_id` = '$table_id'");
	            	if($all_image['status']=="success")
	            	{
		            	foreach($all_image['data'] as $img)
		            	{
		            		
		            		$img_arr[] = base_url.'uploads/ads_image/'.$img['ads_image']; 
		            	}
	            	}
	              	$key_acc['Ads_image'] = $img_arr;
	            	$feat_arr[] = $key_acc;
	            	//$finalarr[] =  array('info'=>$key_acc,'imageinfo'=>$img_arr);  
	            }
	        	$ads['status'] = "success";
	            $ads['message'] = "Successfully";
	            $ads['data'] = $feat_arr;
	        }
	        else
	        {
	        	$ads['status'] = "failed";
	            $ads['message'] = "Data Not Found..!";
	            unset($ads['data']);
	        }
	            echoResponse(200,$ads);

        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/search_ads', function() use ($app)
{
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$search_key = $app->request()->params('search_key');
        	$s_key = strtok($search_key, " ");
        	$s = strlen($s_key);
        	//print_r($s); exit;
        	if($s >3 && !empty($s))
        	{
        		$ss = substr($s_key,0,-2);
        	}
        	else
        	{
        		$ss = $s_key;
        	}
        	//echo $ss = substr($s_key,0,-1); exit;
        	$lat = $app->request()->params('lat');
        	$lng = $app->request()->params('lng');
        	$distance = $app->request()->params('distance');
        	$page_no = $app->request()->params('page_no');
	        $per_set = 10;
	        $from  = ($page_no-1)*$per_set;
        	global $db;
        	//echo "select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance from `Ads` HAVING distance <= '$distance' AND  ( `ad_title` LIKE '%$search_key%' || `ad_description` LIKE '%$search_key%' ) limit $from,$per_set"; exit;
        	//$ads = $db->customQuery("select * from `Ads` where `ad_title` LIKE '%$search_key%' && `ad_description` LIKE '%$search_key%' limit $from,$per_set");
        	$ads = $db->customQuery("select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance from `Ads` HAVING distance <= '$distance' AND  ( `ad_title` LIKE '%$ss%' || `ad_description` LIKE '%$ss%' ) limit $from,$per_set");
	        if($ads['status'] == 'success')
	        {
	        	foreach($ads['data'] as $key_acc)
	            {
	            	$table_id = $key_acc['table_id'];
	            	$img_arr = array();
	            	$all_image =  $db->customQuery("select Ads_image.ads_image from `Ads_image` where `ad_id` = '$table_id'");
	            	if($all_image['status']=="success")
	            	{
		            	foreach($all_image['data'] as $img)
		            	{
		            		$img_arr[] = base_url.'uploads/ads_image/'.$img['ads_image']; 
		            	}
	            	}
	            	$key_acc['Ads_image'] = $img_arr;
	            	$feat_arr[] = $key_acc;
	            }
	        	$ads['status'] = "success";
	            $ads['message'] = "Successfully";
	            $ads['data'] = $feat_arr;
	        }
	        else
	        {
	        	$ads['status'] = "failed";
	            $ads['message'] = "Data Not Found...!";
	            unset($ads['data']);
	        }
	            echoResponse(200,$ads);

        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});


$app->post('/my_ads', function() use ($app)
{
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$category_id = $app->request()->params('category_id');
        	$sort = $app->request()->params('sort');
        	$is_asc = $app->request()->params('is_asc');
        	$status = $app->request()->params('ads_status');
        	$page_no = $app->request()->params('page_no');
	        $per_set = 10;
	        $from  = ($page_no-1)*$per_set;
        	global $db;
        	if($category_id != 0)
        	{
        		$cat_id = "`category_id` = '$category_id'";
        	}else
        	{
        		$cat_id = "1=1";
        	}
        	if($sort == 'price')
        	{
        		$sort = "price";
        	}
        	elseif($sort == 'date')
        	{	
        		$sort = "creat_at";
        	}
        	if($is_asc == 0)
        	{
        		$is_asc1 = "ORDER BY ".$sort." ASC";
        	}
        	else
        	{
        		$is_asc1 ="ORDER BY ".$sort." DESC";
        	}

        	$ads = $db->customQuery("select * from `Ads` where `ads_status` = '$status' AND ".$cat_id." AND `User_id` = $user_id  ".$is_asc1." limit $from,$per_set");
        	
	        if($ads['status'] == 'success')
	        {
	        	foreach($ads['data'] as $key_acc)
	            {
	            	$table_id = $key_acc['table_id'];
	            	$img_arr = array();
	            	$all_image =  $db->customQuery("select Ads_image.ads_image from `Ads_image` where `ad_id`='$table_id'");
	            	if($all_image['status']=="success")
	            	{
		            	foreach($all_image['data'] as $img)
		            	{
		            		$img_arr[] = base_url.'uploads/ads_image/'.$img['ads_image'] ; 
		            	}
	            	}
	            	$key_acc['Ads_image'] = $img_arr;
	            	$feat_arr[] = $key_acc;
	            }
	        	$ads['status'] = "success";
	            $ads['message'] = "Successfully";
	            $ads['data'] = $feat_arr;
	        }
	        else
	        {
	        	$ads['status'] = "failed";
	            $ads['message'] = "No Data Found...!";
	            unset($ads['data']);
	        }
	            echoResponse(200,$ads);
        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});


$app->post('/New_mobile_verification', function() use ($app){
    $headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
		    $user_id = $check['data'][0]['User_id'];
		    $json1 = file_get_contents('php://input');
			if(!empty($json1))
			{
				$data = json_decode($json1);
			    if($data->verify_code != '' && strlen($data->verify_code) == 4)
			    {
			    	global $db;
				    $rows = $db->select("BlueTrade_User","*",array('User_id'=>$user_id,'temp_mobile_no'=>$data->mobile));
				    if($rows["status"]=="success")
				    {
				      	if($rows["data"][0]['Otp_code'] == md5($data->verify_code))
				      	{
				      		$update = $db->update("BlueTrade_User",array('Otp_code'=>'','Contact_num'=>$data->mobile,'Update_at'=>militime),array('User_id'=>$user_id),array());
				      		if($update['status']=="success")
				      		{
				      			$update['status']="success";
					            $update["message"] = "Mobile Number Successfully Verified";
								unset($update['data']);
					            echoResponse(200, $update);
				      		}else
				      		{
				      			$update['status']="failed";
					            $update["message"] = "Something went wrong! Please try again later";
								unset($update['data']);
					            echoResponse(200, $update);
				      		}
				      	}else
				      	{
				      		$rows['status']="failed";
				            $rows["message"] = "Otp not matched.";
							unset($rows['data']);
				            echoResponse(200, $rows);
				      	}     
				    }else
				    {
				       $rows["status"] = 'failed';
				       $rows["message"]= "Mobile number not matched";
				       unset($rows["data"]);
				       echoResponse(200, $rows);
				    }
			    }else
			    {
        			$check_otp['status'] = "failed";
			    	$check_otp['message']= "Invalid Request parameter";
					echoResponse(200,$check_otp);
			    }
			}else
			{
        		$check_otp['status'] = "failed";
				$check_otp['message'] ="No Request parameter";
				echoResponse(200,$check_otp);
			}
		}else
		{
			$check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
	}
});


$app->post('/help', function() use ($app)
{
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
         $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$name = $app->request()->params('name');
        	$email = $app->request()->params('email');
        	$ad_title = $app->request()->params('ad_title');
        	$mobile = $app->request()->params('mobile');
        	$message = $app->request()->params('message');
        	if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
			{
				$image = md5(militime.$_FILES['image']['name']).'.jpg';
				move_uploaded_file($_FILES['image']['tmp_name'], '../../uploads/ads_image/'.$image);
			}else
			{
				$image = "";
			}
			//print_r($image); exit;
        	global $db;

	        $insert_help = $db->insert('help',array('user_id'=>$user_id,'user_name'=>$name,'user_email'=>$email,'user_mobile'=>$mobile,'message'=>$message,'ad_title'=>$ad_title,'image'=>$image),array());
	        if($insert_help['status'] == 'success')
	        {
	        	$insert_help['status'] = "success";
	            $insert_help['message'] = "Successfully Inserted";
	            $insert_help['data'] = array('user_id'=>$user_id,'name'=>$name,'email'=>$email,'ad_title'=>$ad_title,'mobile'=>$mobile,'message'=>$message,'image'=>$image); 
	        }
	        else
	        {
	        	$insert_help['status'] = "failed";
	            $insert_help['message'] = "Data Not Inserted..!";
	            unset($insert_help['data']);
	        }
	            echoResponse(200,$insert_help);
        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }

});


$app->post('/deactivate_ads', function() use ($app)
{
  $headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$id = $app->request()->params('id');
        	global $db;
	        $deactivate = $db->update('Ads',array('ads_status' =>'1'),array('table_id' => $id ),array());
	        if($deactivate['status'] == 'success')
	        {
	        	$deactivate['status'] = "success";
	            $deactivate['message'] = "Successfully Deactivate";
	            //$deactivate['data'] = array('user_id'=>$user_id,'name'=>$name,'email'=>$email,'ad_title'=>$ad_title,'mobile'=>$mobile,'message'=>$message,'image'=>$image); 
	        }
	        else
	        {
	        	$deactivate['status'] = "failed";
	            $deactivate['message'] = "Not Deactivate..!";
	            unset($deactivate['data']);
	        }
	            echoResponse(200,$deactivate);
        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }

});

$app->post('/Get_category',function() use ($app){
	
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
         $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	global $db;
			$orderby = "ORDER BY order_no ASC";
			$query_login = $db->select2("category","*",array('parent_id'=>0,'category_status'=>1),$orderby);
			if($query_login["status"] == "success")
			{	
				$finalarr =array();
				foreach($query_login['data'] as $key_acc)
	            {
	            	if($key_acc['image']!='')
	            	{
	            		$image = base_url.'uploads/category_image/'.$key_acc['image'];
	            	}
	            	$finalarr[] = array(
	            			'category_id' => $key_acc['category_id'],
	            			'category_name' => $key_acc['category_name'],
	          				'category_image'=>$image
	            		);
	            }	
	            $check_otp['status'] = "success";
				$check_otp['message'] ="Category List";
				$check_otp['data'] = $finalarr;
				echoResponse(200,$check_otp);
			}else{
				$check_otp['status'] = "failed";
				$check_otp['message'] ="No data found";
				echoResponse(200,$check_otp);
			}
        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/Get_subcategory',function() use ($app){
	
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	global $db;
			$category_id = $app->request()->params('category_id');
			$orderby = "ORDER BY order_no ASC";
			$query_login = $db->select("category","*",array('parent_id'=>$category_id,'category_status'=>1));
			if($query_login["status"] == "success")
			{	
				$finalarr =array();
				foreach($query_login['data'] as $key_acc)
	            {
	            	$finalarr[] = array(
	            			'sub_cateid' => $key_acc['category_id'],
	            			'subcat_name' => $key_acc['category_name'],
	          			);
	            }	
	            $check_otp['status'] = "success";
				$check_otp['message'] ="Subcategory List";
				$check_otp['data'] = $finalarr;
				echoResponse(200,$check_otp);
			}else{
				$check_otp['status'] = "failed";
				$check_otp['message'] ="No data found";
				echoResponse(200,$check_otp);
			}
        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/Ads_click_count',function() use ($app){
	
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	global $db;
			$ads_id = $app->request()->params('ads_id');
			$query_login = $db->select("Ads","table_id",array('table_id'=>$ads_id,'ads_status'=>0));
			if($query_login["status"] == "success")
			{	
				$query_viewer = $db->select("view_ads_user","user_id",array('user_id'=>$check['data'][0]['User_id'],'ads_id'=>$ads_id));
				if($query_viewer['status']=='success')
				{

				}else
				{
					$insert_view_user = $db->insert("view_ads_user",array('user_id'=>$check['data'][0]['User_id'],'ads_id'=>$ads_id,'view_date'=>date('Y-m-d H:i:s')),array());
					$update_view = $db->customupdate("UPDATE tranding SET views = views + 1 WHERE ads_id = '$ads_id'");
				}
		        $queryresp['status'] = "success";
				$queryresp['message'] ="successfully";
				//$queryresp['data'] = $finalarr;
				echoResponse(200,$queryresp);
			}else{
				$check_otp['status'] = "failed";
				$check_otp['message'] ="No Ads found";
				echoResponse(200,$check_otp);
			}
        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});
/*$app->post('/sendmsg', function() use ($app) {
global $db;
	$token = $db->select("select","Device_token",array('Email_id'=>"harsh.parashar@ebabu.co"));
	iOSPushNotification($token['data'][0]['Device_token'],"testing",'This is testing notification');

//    sendsms('9755116566','hii');
});
*/

$app->post('/darshancategory',function() use ($app){
	
	$json1 = file_get_contents('php://input');
	if(!empty($json1)){
		 $data = json_decode($json1);
		global $db;
		 $parent_id= $data->parent_id;
		 	$condition2 = array('parent_id'=>$parent_id);
			$query_login = $db->select("category","*",$condition2);
			if($query_login["status"] == "success")
			{	$finalarr =array();
				foreach($query_login['data'] as $key_acc)
	            {
	            	$new = array(
	            			'category_id' => $key_acc['category_id'],
	            			'category_name' => $key_acc['category_name'],
	            			'parent_id' => $key_acc['parent_id'],

	            		);
	            	$finalarr[] = $new;
	            }	
	            $check_otp['status'] = "failed";
				$check_otp['message'] ="No data found";
				$check_otp['data'] = $finalarr;
				echoResponse(200,$check_otp);
			}else{
				$check_otp['status'] = "failed";
				$check_otp['message'] ="No data found";
				echoResponse(200,$check_otp);
			}
	}else{	
		$check_otp['message'] ="No Request parameter";
		echoResponse(200,$check_otp);
	}
});

function sendsms($mobileNumber,$message1)
    {
        $authKey = "128888A4N4rOlh5808a257";
        //$mobileNumber = "9999999";
        $senderId = "btrade";
        $message = urlencode($message1);
        //Define route 
        $route = "4";
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        $url="https://control.msg91.com/api/sendhttp.php?";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    
        //get response
        $output = curl_exec($ch);
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
       // echo $output;exit;
    }

function distance($lat1, $lon1, $lat2, $lon2, $unit)
{
	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);
	if ($unit == "K") {
	return ($miles * 1.609344);
	} else if ($unit == "N") {
	  return ($miles * 0.8684);
	} else {
	    return $miles;
	}
}
function echoResponse($status_code, $response) {
    global $app;
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response);
}


$app->run();
?>
