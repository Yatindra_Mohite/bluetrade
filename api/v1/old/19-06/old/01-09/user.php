<?php
require '.././libs/Slim/Slim.php';
require_once 'dbHelper.php';
require_once 'auth.php';
require_once 'gcm.php';
//GCM key = AIzaSyDelbOBvjrceX1_6j2vZA9p0NcFMEiCGE8

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app = \Slim\Slim::getInstance();
$db = new dbHelper();

date_default_timezone_set("Asia/Kolkata");
$base_url = "http://base3.engineerbabu.com:8282/great_lakes/"; 
$dateTime = date("Y-m-d H:i:s", time()); 
$militime=round(microtime(true) * 1000);
define('dateTime', $dateTime);
define('base_url', $base_url);
define('militime', $militime);


$app->post('/SocialSignIn',function() use ($app){
	$json1 = file_get_contents('php://input');
	if(!empty($json1))
	{
	    $data = json_decode($json1);
		$username= $data->name;
		//$mobile = $data->mobile;
		$email= $data->email;
		$device_token= $data->device_token;
		$device_type = $data->device_type;
		$user_type = $data->user_type;
		$profile_pic = $data->image;

		global $db;
		if(!empty($email))
		{
			$token = bin2hex(openssl_random_pseudo_bytes(16));
			$token = $token.militime;

			$user_data = array( 'name'=>$username,
								'email'=>$email,
								//'mobile'=>$mobile,
								'device_token'=>$device_token,
								'device_type'=>$device_type,
								'user_type'=>$user_type,
								'image'=>$profile_pic,
								'user_status'=>1,
								'token'=>$token,
								'create_at'=>militime,
								'update_at'=>militime
							  );
			$condition = array('email'=>$email);
			$query_login = $db->select("user","*",$condition);
			if($query_login["status"] == "success")
			{	
				if($query_login['data'][0]['user_status'] != 2)
				{
					unset($user_data['create_at']);
					$user_data['user_id']=$query_login['data'][0]['user_id'];
					$row =$db->update("user",$user_data,array('email'=>$email),array());
					$user_data['mobile'] = $query_login['data'][0]['mobile'];
					$user_data['city'] = $query_login['data'][0]['city'];
					$query_login['status'] ="success";
					$query_login['message'] ="Login Successfully";
					$query_login['data']= $user_data;
					echoResponse(200,$query_login);
				}else
				{
					$query_login['status'] ="success";
					$query_login['message'] ="Your Great Lakes account and has been temporarily suspended as a security precaution.";
					unset($query_login['data']);
					echoResponse(200,$query_login);
				}
			}
			else
			{ 
				$insert_user = $db->insert("user",$user_data,array());
				if($insert_user["status"] == "success")
				{
					$user_data['user_id']=$insert_user['data'];
					$user_data['city'] = '';
					$user_data['mobile'] = '';
					$insert_user['data'] =$user_data;
					$insert_user['status'] ="success";
					$insert_user['message'] ="Login Successfully";
					echoResponse(200,$insert_user);
				}else
				{
					$insert_user['status'] = "failed";
					$insert_user['message'] ="something wrong!!";
					echoResponse(200,$insert_user);
				}
			}
		}else
		{
			$insert_user['status'] = "false";
			$insert_user['message'] ="Invalid Request parameter!!";
			echoResponse(200,$insert_user);
		}
	}else
	{
		$insert_user['status'] = "false";
		$insert_user['message'] ="No Request parameter!!";
		echoResponse(200,$insert_user);
	}	
});

$app->post('/SignUp',function() use ($app){
	$json1 = file_get_contents('php://input');
	if(!empty($json1))
	{
	    $data = json_decode($json1);
		$username= $data->name;
		$mobile = $data->mobile_no;
		$email= $data->email;
		$city= $data->city;
		$device_token= $data->device_token;
		$device_type = $data->device_type;
		$password = $data->password;
		
		if(!empty($email) && !empty($mobile) && !empty($password))
		{
			global $db;

			$user_data = array( 'name'=>$username,
								'email'=>$email,
								'mobile'=>$mobile,
								'city'=>$city,
								"device_token"=>$device_token,
								"device_type"=>$device_type,
								'password'=>md5($password),
								'create_at'=>militime,
								'update_at'=>militime
								);
			
			$code = substr(randomuniqueCode(),0,6);
			$condition = array('email'=>$email);
			$query_login = $db->select("user","*",$condition);
			if($query_login["status"] == "success")
			{	
				if($query_login['data'][0]['user_status']==0)
				{
		            $sel_temp = $db->select("email_template_descriptions","*",array('id'=>'1'),array());
		            if($sel_temp['status'] =="success")
		            {
		                foreach($sel_temp['data'] as $key3)
		                {
		                    $content = $key3['content'];
		                }
		            }else
	              	{
	                    $content = ''; 
	              	}
		       	    $subject = "Great Lakes App: Verification Link";
		             
	              	$message=stripcslashes($content);
	              	$message=str_replace("{date}",date("d"),$message);
	              	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
	              	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
	              	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	              	$message=str_replace("img/great_lakes_logo.png",base_url.'template/assets/layouts/layout4/img/great_lakes_logo.png',$message);
	              	$message=str_replace("{email}",$email,$message);
			       	$message=str_replace("{link}","<a href=".base_url."api/v1/user.php/verifyEmail?secretkey=".base64_encode($query_login['data'][0]['user_id'])."&secret_key=".base64_encode($code)."'>CLICK HERE</a>",$message);
		            $email_from='no-repply@greatlakesapp.com';
	                $headers  = 'MIME-Version: 1.0' . "\r\n";
	                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	                $headers .= 'From: '.$email_from. '\r\n';            // Mail it
	                $cc = '';
		         //print_r($message);exit;  
		            $rows3 = $db->update("user", array("code"=>$code,'update_at'=>militime), $condition,array());
		            if($rows3['status']=="success")
		            {
		                //$senddd = Send_Mail($email_from,$email,$cc,$subject,$message);
			           if(@mail($email, $subject,$message,$headers))
			           {
			           		$rows3['status']="success";
				            $rows3["message"] = "Verification Link has been sent to your registered email address";
							unset($user_data['password']);
							unset($user_data['create_at']);
							unset($user_data['update_at']);
				            $rows3['data'] = $user_data;
				            echoResponse(200, $rows3);
			           }else
			           {
			           		$rows3['status'] = "failed";
							$rows3['message'] ="failed";
							unset($rows3['data']);
							echoResponse(200,$rows3);
			           }
		
		            }else
			        {
			        	$rows3['status'] = "failed";
						$rows3['message'] ="failed";
						unset($rows3['data']);
						echoResponse(200,$rows3);
			        }
		    	}else if($query_login['data'][0]['user_status']==2)
				{
					$query_login['status'] ="success";
					$query_login['message'] ="Your Great Lakes account and has been temporarily suspended as a security precaution.";
					unset($query_login['data']);
					echoResponse(200,$query_login);
				}else
				{
					$query_login['status'] = "failed";
					$query_login['message'] ="Already Exist";
					unset($query_login['data']);
					echoResponse(200,$query_login);
				}
			}
			else
			{ 
				$insert_user = $db->insert("user",$user_data,array());
				if($insert_user["status"] == "success")
				{
					$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'1'),array());
		            if($sel_temp['status'] =="success")
		            {
		                foreach($sel_temp['data'] as $key3)
		                {
		                    $content = $key3['content'];
		                }
		            }else
	              	{
	                    $content = ''; 
	              	}
		            $subject = "Education Finder App: Verification Link";
	               	
			        $message=stripcslashes($content);
	              	$message=str_replace("{date}",date("d"),$message);
	              	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
	              	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
	              	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	              	$message=str_replace("logo/Education-Finder_logo.png",base_url.'template/assets/layouts/layout4/img/great_lakes_logo.png',$message);	
		          	$message=str_replace("{email}",$email,$message);
	              	$message=str_replace("{link}","<a href=".base_url."api/v1/user.php/verifyEmail?secretkey=".base64_encode($insert_user['data'])."&secret_key=".base64_encode($code)."'>CLICK HERE</a>",$message);
		            $email_from ='no-reply@greatlakes.com';
		        	$headers  = 'MIME-Version: 1.0' . "\r\n";
		            $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
		            $headers .= 'From: '.$email_from. '\r\n';
		            $cc = '';
		            $rows3 = $db->update("user", array("code"=>$code), $condition,array());
		        	if($rows3['status']=="success")
		        	{
			        	if(@mail($email, $subject, $message,$headers))
				           {
				           		$insert_user['status']="failed";
					            $insert_user["message"] = "Verification Link has been sent to your registered email address";
								$insert_user['user_id'] = $insert_user['data'];
								unset($user_data['password']);
								$insert_user['data'] = $user_data;
					            echoResponse(200, $insert_user);
				           }else
				           {
				           		$insert_user['status'] = "failed";
								$insert_user['message'] ="failed";
								unset($insert_user['data']);
								echoResponse(200,$insert_user);
				           }
		      		}else
		      		{
		      			$insert_user['status'] = "failed";
						$insert_user['message'] ="failed";
						unset($insert_user['data']);
						echoResponse(200,$insert_user);
		      		}
		            //$senddd = Send_Mail($email_from,$email,$cc,$subject,$message);
				}else
				{
					$insert_user['status']="failed";
		            $insert_user["message"] = "Registration failed";
					unset($insert_user['data']);
		            echoResponse(200, $insert_user);
				}
			}
		}
		else
		{
			$check_otp['status'] = "failed";
			$check_otp['message']= "Empty Request parameter!!";
			echoResponse(200,$check_otp);
		}
	}else
	{
		$insert_user['status'] = "false";
		$insert_user['message'] ="No Request parameter!!";
		echoResponse(200,$insert_user);
	}
});

$app->get('/verifyEmail', function() use ($app){
    $user_id = base64_decode($app->request()->params('secretkey'));
    $code = base64_decode($app->request()->params('secret_key'));

    $condition = array('user_id'=>$user_id);
	global $db;
    $rows = $db->select("user","*",$condition);
    if($rows["status"]=="success")
    {
            $verify_code = $rows['data'][0]['code'];
            if($code == $verify_code && $rows['data'][0]['user_status'] == 0)
            {
                $sel_temp = $db->select("email_template_descriptions","*",array('id'=>'3'),array());

                if($sel_temp['status'] =="success")
                {
                    foreach($sel_temp['data'] as $key3)
                    {
                    $content = $key3['content'];
                    }
                }else
                {
                    $content = ''; 
                }

                $message=stripcslashes($content);
              	$message=str_replace("{date}",date("d"),$message);
              	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
              	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
              	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
              	$message=str_replace("img/great_lakes_logo.png",base_url.'template/assets/layouts/layout4/img/great_lakes_logo.png',$message);

                $rows3 = $db->update("user", array("user_status"=>1,'code'=>''), $condition,array());

                echo $message;exit;
            }else
            {
                $sel_temp = $db->select("email_template_descriptions","*",array('id'=>'4'),array());
                if($sel_temp['status'] =="success")
                {
                    foreach($sel_temp['data'] as $key3)
                    {
                    	$content = $key3['content'];
                    }
                }else
                {
                    $content = ''; 
                }

                $message=stripcslashes($content);
                $message=str_replace("{date}",date("d"),$message);
              	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
              	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
              	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
              	$message=str_replace("img/great_lakes_logo.png",base_url.'template/assets/layouts/layout4/img/great_lakes_logo.png',$message);

                echo $message;exit;
            }
    }else
    {
       $rows["message"]= "Invalid Request";
       unset($rows["data"]);
       unset($rows["status"]);
       echoResponse(200, $rows);
    }
});

$app->post('/login',function() use ($app){
	$json1 = file_get_contents('php://input');
	if(!empty($json1))
	{
	    $data = json_decode($json1);
		$email = $data->email;
		$password = $data->password;
		$device_token = $data->device_token;
		$device_type = $data->device_type;

		global $db;
		if(!empty($email) && !empty($password))
		{ 
			$code = substr(randomuniqueCode(),0,6);
			$token = bin2hex(openssl_random_pseudo_bytes(16));
			$token = $token.militime;

			$query_login = $db->select("user","*",array("email"=>$email,"password"=>md5($password)));
			
			if($query_login["status"] == "success")
			{	
				if($query_login['data'][0]['user_status'] != 2)
				{
					if($query_login['data'][0]['user_status']==1 )
					{
						$update = $db->update("user",array('token'=>$token,'device_token'=>$device_token,'device_type'=>$device_type),array("email"=>$email),array());
						
						foreach ($query_login['data'] as $key) {
							if($key['user_type']==0){
								$image = base_url.'upload/user_image/'.$key['image'];
							}else{
								$image = $key['image'];
							}
							$key['image'] = $image;
							$arr = $key;
						}
						$query_login['status'] ="success";
						$query_login['message'] ="Login successfully";
						$query_login['data'] = $arr;
						echoResponse(200,$query_login);
					}else
					{
						$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'1'),array());
			            if($sel_temp['status'] =="success")
			            {
			                foreach($sel_temp['data'] as $key3)
			                {
			                    $content = $key3['content'];
			                }
			            }else
		              	{
		                    $content = ''; 
		              	}
			       	    $subject = "Great Lakes App: Verification Link";
			             
		              	$message=stripcslashes($content);
		              	$message=str_replace("{date}",date("d"),$message);
		              	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
		              	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
		              	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
		              	$message=str_replace("img/great_lakes_logo.png",base_url.'template/assets/layouts/layout4/img/great_lakes_logo.png',$message);
		              	$message=str_replace("{email}",$email,$message);
				       	$message=str_replace("{link}","<a href=".base_url."api/v1/user.php/verifyEmail?secretkey=".base64_encode($query_login['data'][0]['user_id'])."&secret_key=".base64_encode($code)."'>CLICK HERE</a>",$message);
			            $email_from='no-repply@greatlakesapp.com';
		                $headers  = 'MIME-Version: 1.0' . "\r\n";
		                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		                $headers .= 'From: '.$email_from. '\r\n';            // Mail it
		                $cc = '';
			         //print_r($message);exit;  
			            $rows3 = $db->update("user", array("code"=>$code,'update_at'=>militime),array("email"=>$email),array());
			            if($rows3['status']=="success")
			            {
			                //$senddd = Send_Mail($email_from,$email,$cc,$subject,$message);
				           if(@mail($email,$subject,$message,$headers))
				           {
				           		$rows3['status']="failed";
					            $rows3["message"] = "Verification Link has been sent to your registered email address";
								unset($rows3['data']);
							    echoResponse(200, $rows3);
				           }else
				           {
				           		$rows3['status'] = "failed";
								$rows3['message'] ="failed";
								unset($rows3['data']);
								echoResponse(200,$rows3);
				           }
			            }else
				        {
				        	$rows3['status'] = "failed";
							$rows3['message'] ="failed";
							unset($rows3['data']);
							echoResponse(200,$rows3);
				        }
					}
				}else
				{
					$query_login['status'] ="failed";
					$query_login['message'] ="Your Great Lakes account and has been temporarily suspended as a security precaution.";
					unset($query_login['data']);
					echoResponse(200,$query_login);
				}
			}
			else
			{ 
				$query_login['status'] = "failed";
				$query_login['message'] ="Invalid Credential";
				unset($query_login['data']);
				echoResponse(200,$query_login);
			}
		}else
		{
			$insert_user['status'] = "failed";
			$insert_user['message'] ="Invalid Request parameter!!";
			echoResponse(200,$insert_user);
		}
	}else
	{
		$insert_user['status'] = "failed";
		$insert_user['message'] ="No Request parameter!!";
		echoResponse(200,$insert_user);
	}
});

$app->post('/ForgetPassword', function() use ($app){

	$email = $app->request()->params('email');
	$condition = array('email'=>$email);
	global $db;
	$rows = $db->select("user","*",$condition);
	//print_r($rows);exit;
    if($rows['status'] == "success")
    {
		if($rows['data'][0]['user_status'] == 1)
		{	
			if($rows['data'][0]['user_type'] == 0)
			{
				$userid = $rows['data'][0]['user_id'];
	            $sel_temp = $db->select("email_template_descriptions","*",array('id'=>'2'),array());
	                
	                if($sel_temp['status'] =="success")
	                {
	                    foreach($sel_temp['data'] as $key3)
	                    {
	                      $content = $key3['content'];
	                    }
	                }else
	                {
	                 $content = ''; 
	                }
		        $subject = "Greatlakes App: Forget Password";
		           
		        $message=stripcslashes($content);
		        $message=str_replace("{date}",date("d"),$message);
		        $message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
		        $message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
		        $message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	            $message=str_replace("img/great_lakes_logo.png",base_url.'template/assets/layouts/layout4/img/great_lakes_logo.png',$message);
		        $message=str_replace("{email}",$email,$message);
		        $message=str_replace("{link}","<a href=".base_url."api/v1/web/temp.php?auth_key=".base64_encode($userid)."&id=".militime."'>CLICK HERE</a>",$message);
		          //$message=str_replace("{password}",$code,$message);
		           // echo $message; exit;
		        $email_from='no-repply@greatlakes.com';
		        $headers  = 'MIME-Version: 1.0' . "\r\n";
		        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		        $headers .= 'From: '.$email_from.'\r\n';            // Mail it
		           
		         @mail($email, $subject, $message, $headers);
		          $rows3 = $db->update("user", array("code"=>'1234'), $condition,array());
		          $rows["message"] = "Link has been sent to your registered email address.";
		          unset($rows['data']);
    		}else{
		        $rows["status"] = "failed";
		        $rows["message"] = "This feature is not applicable for you";
    			unset($rows['data']);
    		}
    	}else{
    		if($rows['data'][0]['user_status'] == 2)
    		{
    			$rows['status'] ="failed";
				$rows['message'] ="Your Great Lakes account and has been temporarily suspended as a security precaution.";
				unset($rows['data']);
    		}else
    		{
    			$rows["status"] = "failed";
		        $rows["message"] = "Please verify your email address first";
	    		unset($rows['data']);	
    		}
		}
    }else{

        $rows["status"] = "failed";
        $rows["message"] = "Email does not exist";
    	unset($rows['data']);
	}
    echoResponse(200, $rows);
});

$app->post('/ChangePassword', function() use ($app) { 
    
  $headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status']=="true")
		{
	        $user_id=$check['data'][0]['user_id'];
	        global $db;
		    $current_password = md5($app->request()->params('current_password'));
		    $new_password = $app->request()->params('new_password');
         	$condition = array('user_id'=>$user_id,'user_type'=>0);
          
          	$rows1 = $db->select("user","*",$condition);
            if($rows1['status']=="success")
            {
            	$password = $rows1['data'][0]['password']; 
				
				if($current_password == $password)
				{
					$newpassword = md5($new_password);
					$data = array(
					        'password'=>$newpassword,
					        'update_at'=>militime
					      );

					$rows = $db->update("user",$data,$condition,array());
					if($rows['status']=='success')
					{
					    $rows["status"] = "success";
					    $rows["message"] = "Your password has been successfully changed.";
					    unset($rows['data']);
					}else
					{
					    $rows["status"] = "failed";
					    $rows["message"] = "failed";
					    unset($rows['data']);
					}
					echoResponse(200, $rows);
				}else
				{
					$rows1["status"] = "failed";
					$rows1["message"] = "The old password you entered was incorrect.";
					unset($rows1['data']);
					echoResponse(200, $rows1);
				}    
  			}else
  			{
  				$rows1["status"] = "failed";
        		$rows1["message"] = "This feature is not applicable for you";
        		unset($rows1['data']);
				echoResponse(200, $rows1);
  			}
  		}
		else
		{
			$msg['status'] = "false";
			$msg['message'] = "Invalid Token";
			echoResponse(200,$msg);
		}
	}
	else
	{
		$msg['status'] = "false";
		$msg['message'] = "Unauthorised access";
		echoResponse(200,$msg);
	}
});

$app->post('/UpdateProfile', function() use ($app) { 
    
  $headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status']=="true")
		{
	        $user_id=$check['data'][0]['user_id'];
	        global $db;
		    $name = $app->request()->params('name');
			$mobile = $app->request()->params('mobile');
			$city= $app->request()->params('city');
		 	$condition = array('user_id'=>$user_id,'user_status'=>1);
          
          	$rows1 = $db->select("user","*",$condition);
            if($rows1['status']=="success")
            {
          		//$mobileno = $db->select("user","mobile",array('mobile'=>$mobile));
				$mobileno = $db->customQuery("SELECT mobile FROM user WHERE mobile = '$mobile' AND user_id != '$user_id'");
          		if($mobileno['status'] == "success")
          		{
          			$mobileno["status"] = "failed";
					$mobileno["message"] = "Mobile number already exist! Please try another.";
					unset($mobileno['data']);
					echoResponse(200, $mobileno);
				}else
          		{	
          			if(!isset($_FILES["file"]) || $_FILES["file"]=='')
					{
					  	$location = $rows1['data'][0]['image'];
					}
					else
					{
						$image=$_FILES["file"]["name"];
						$image=md5(militime.$image).'.png';
						move_uploaded_file($_FILES["file"]["tmp_name"],"upload/user_image/".$image);
						$location=$image;
					}

            		$user_data = array('name'=>$name,'mobile'=>$mobile,'image'=>$location,'city'=>$city,'update_at'=>militime);

            		$rows = $db->update("user",$user_data,$condition,array());
					if($rows['status']=='success')
					{
					    if (strpos($location,'http') !== false)
                            {
                                $img = $location;
                            }else
                            {
                       			if($location == '') $img = '';
                           		else
                       			$img = base_url.'api/v1/upload/user_image/'.$location;
                            }

					    $rows["status"] = "success";
					    $rows["message"] = "Successfully Updated";
					    $rows['data'] = array('name'=>$name,'mobile'=>$mobile,'image'=>$img,'city'=>$city,'update_at'=>militime);
					}else
					{
					    $rows["status"] = "failed";
					    $rows["message"] = "failed";
					    unset($rows['data']);
					}
					echoResponse(200, $rows);
          		}
         	}else
  			{
  				$rows1["status"] = "failed";
        		$rows1["message"] = "Your Great Lakes account and has been temporarily suspended as a security precaution.";
        		unset($rows1['data']);
				echoResponse(200, $rows1);
  			}
  		}
		else
		{
			$msg['status'] = "false";
			$msg['message'] = "Invalid Token";
			echoResponse(200,$msg);
		}
	}
	else
	{
		$msg['status'] = "false";
		$msg['message'] = "Unauthorised access";
		echoResponse(200,$msg);
	}
});

$app->get('/GLC_category',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			global $db;
			$category = $db->select("GLC_category","category_id,category_name",array('del_status'=>1));
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					
					$arr[] = $key;
				}
				$query_login['status'] = "success";
				$query_login['message'] = "Successfull";
				$query_login['data'] = $arr;
				echoResponse(200,$query_login);
			}else
			{
				unset($query_login['data']);
				$query_login['status'] = "failed";
				$query_login['message'] = "Category Not Found";
				echoResponse(200,$query_login);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/GLC_Subcategory',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$categoryid = $app->request->params('categoryid');
			global $db;
			$category = $db->select("GLC_subcategory","subcate_id,sub_cate_name",array('category_id'=>$categoryid,'del_status'=>1));
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					$arr[] = $key;
				}
				$query_login['status'] = "success";
				$query_login['message'] = "Successfull";
				$query_login['data'] = $arr;
				echoResponse(200,$query_login);
			}else
			{
				unset($query_login['data']);
				$query_login['status'] = "failed";
				$query_login['message'] = "Sub Category Not Found";
				echoResponse(200,$query_login);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});
	
$app->get('/LatestGLC_List',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$sub_category_id = $app->request->params('sub_category_id');
			$create_at = $app->request->params('create_at');
			global $db;
			$create = '';
			if($create_at != 0)
			{
				$create = "AND created_at < '$create_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$category = $db->customQuery("SELECT * FROM News_detail WHERE sub_cate_id = '$sub_category_id' AND del_status = 1 ".$create." ORDER BY news_id DESC LIMIT 10");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					$seconds = $key['created_at'] / 1000;
					$date = date("d-m-Y", $seconds);
					$key['news_date'] = $date;
					$image = base_url.'uploads/news_image/'.$key['image'];
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Latest@GLC List Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/LatestGLC_List_PulltoRefresh',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$sub_category_id = $app->request->params('sub_category_id');
			$create_at = $app->request->params('create_at');
			global $db;
			$create = '';
			if($create_at != 0)
			{
				$create = "AND created_at > '$create_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$category = $db->customQuery("SELECT * FROM News_detail WHERE sub_cate_id = '$sub_category_id' AND del_status = 1 ".$create." ORDER BY news_id DESC LIMIT 10");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					$seconds = $key['created_at'] / 1000;
					$date = date("d-m-Y", $seconds);
					$key['news_date'] = $date;
					$image = base_url.'uploads/news_image/'.$key['image'];
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Latest@GLC List Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/Event_List',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$create_at = $app->request->params('create_at');
			global $db;
			$create = '';
			if($create_at != 0)
			{
				$create = "AND event_militime < '$create_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$category = $db->customQuery("SELECT * FROM GLC_event WHERE event_status = 1 ".$create." ORDER BY event_militime DESC LIMIT 2");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					
					$image = base_url.'uploads/event_image/'.$key['event_image'];
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "Event Not Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/Event_List_PulltoRefresh',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$create_at = $app->request->params('create_at');
			global $db;
			$create = '';
			if($create_at != 0)
			{
				$create = "AND event_militime > '$create_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$category = $db->customQuery("SELECT * FROM GLC_event WHERE event_status = 1 ".$create." ORDER BY event_militime DESC LIMIT 2");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					
					$image = base_url.'uploads/event_image/'.$key['event_image'];
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "Event Not Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/Illuminati_List',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$sub_category_id = $app->request->params('sub_category_id');
			$create_at = $app->request->params('create_at');
			global $db;
			$create = '';
			if($create_at != 0)
			{
				$create = "AND created_at < '$create_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$category = $db->customQuery("SELECT * FROM GLC_illuminati WHERE category_id = '$sub_category_id' AND del_status = 1 ".$create." ORDER BY illuminati_id DESC LIMIT 10");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					$image = base_url.'uploads/illuminati_image/'.$key['illuminati_image'];
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Illuminati List Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/Illuminati_List_PulltoRefresh',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$sub_category_id = $app->request->params('sub_category_id');
			$create_at = $app->request->params('create_at');
			global $db;
			$create = '';
			if($create_at != 0)
			{
				$create = "AND created_at > '$create_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$category = $db->customQuery("SELECT * FROM GLC_illuminati WHERE category_id = '$sub_category_id' AND del_status = 1 ".$create." ORDER BY illuminati_id DESC LIMIT 10");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					$image = base_url.'uploads/illuminati_image/'.$key['illuminati_image'];
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Illuminati List Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->post('/DeleteAccount', function() use ($app)
{
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
	    $check = token_auth($headers['secret_key']);
	    global $db;
	    if($check['status']=="true")
	    {
	        $user_id = $check['data'][0]['user_id'];
	        $id = $app->request->params('id');
	        $deleteid = $db->update("user",array('user_status'=>0,'token'=>'','update_at'=>militime),array('user_id'=>$user_id),array());
	        
	        if($deleteid['status']=="success")
	        {
	            $deleteid["message"] = "Successfully Deleted";
	        }else
	        {
	            $deleteid["status"] = "failed";
	            $deleteid["message"] = "Account not deleted";
	        }
	        echoResponse(200, $deleteid);
	    }else
	    {
	        $check["message"] = "Invalid Token";
	        echoResponse(200, $check);
	    }
	}else{
	    $check['message'] = "Unauthorised access";
	    echoResponse(200,$check);    
	}
});

$app->post('/AppVersion', function() use ($app){
    $app_version = $app->request()->params('app_version');
    global $db;

   $version = $db->select("app_version","*",array('version'=>$app_version));
    if($version['status']=="success")
    {
            $version['status']='success';
            $version['message']='Successfull';
            unset($version['data']);       
            echoResponse(200, $version);
	}else
    {
            $version['status']='failed';
            $version['message']='Plz Update Old Version';
            unset($version['data']);       
            echoResponse(200, $version);   
    }   
});

$app->get('/GetPage', function() use ($app){
    
    $page_id = $app->request()->params('page_id');
    $condition = array('id'=>$page_id);
  	global $db;
    $rows = $db->select("pages","*",$condition);
    if($rows['status'] == 'success')
    {
            $data = array(
                        'page_id'=>$rows['data'][0]['id'],
                        'title'=>$rows['data'][0]['page_title'],
                        'description'=>$rows['data'][0]['desc']
                        );
        $rows["message"] = "Successfull";
        $rows["data"] = $data;
    }else
    {
        $rows["status"] = "failed";
        $rows["message"] = "failed";
        unset($rows["data"]);
    }
    echoResponse(200, $rows);
});

$app->get('/Admission',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$sub_category_id = $app->request->params('sub_category_id');
			global $db;
			$category = $db->select("GLC_admission","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1));
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					if($sub_category_id == 0)
					{
						$image = base_url.'uploads/admission_image/'.$key['image'];
						$key['image'] = $image;
						$key['url'];
						unset($key['del_status']);
					}else
					{
						unset($key['image']);
						unset($key['url']);
						unset($key['del_status']);
					}
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Admission Detail Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/Whats_Glc',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$sub_category_id = $app->request->params('sub_category_id');
			$category_id = $app->request->params('category_id');
			global $db;
			$image='';
			$category = $db->select("Whats_GLC","*",array('sub_cate_id'=>$sub_category_id,'sub_cate_category'=>$category_id,'del_status'=>1));
			if($category['status']=="success")
			{
				if($category['data'][0]['image']!='')
				{
					$image = base_url.'uploads/whatglc_image/'.$category['data'][0]['image'];
				}
				unset($category['data'][0]['del_status']);
				$arr = array(
						'title'=>$category['data'][0]['title'],
						'description'=>$category['data'][0]['description'],
						'url'=>$category['data'][0]['url'],
						'image'=>$image,
						);
				
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Record Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/Embedded_campus',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$created_at = $app->request->params('created_at');
			global $db;
			$create = '';
			if($created_at != 0)
			{
				$create = "AND created_at < '$created_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$image = '';
			$category = $db->customQuery("SELECT * FROM Embedded_campus WHERE del_status = 1 ".$create." ORDER BY campus_id DESC LIMIT 2");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					if($key['image'] != '')
					{
						$image = base_url.'uploads/whatsglc_image/'.$key['image'];
					}
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Embedded_campus List Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/Embedded_campus_PulltoRefresh',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$created_at = $app->request->params('created_at');
			global $db;
			$create = '';
			if($created_at != 0)
			{
				$create = "AND created_at > '$created_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$image = '';
			$category = $db->customQuery("SELECT * FROM Embedded_campus WHERE del_status = 1 ".$create." ORDER BY campus_id DESC LIMIT 2");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					if($key['image'] != '')
					{
						$image = base_url.'uploads/whatsglc_image/'.$key['image'];
					}
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Embedded_campus List Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/QuizList',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$userid = $check['data'][0]['user_id'];
			$created_at = $app->request->params('created_at');
			global $db;
			$create = '';
			if($created_at != 0)
			{
				$create = "AND created_at < '$created_at'";
			}
			$curentdt = date('d-m-Y');
			$image = '';
			$status = 0;
			$counnt = 0;
			$category = $db->customQuery("SELECT * FROM GLC_Quiz WHERE is_active = 1 AND `quiz_end_date` >= '$curentdt' ".$create." ORDER BY quiz_id DESC LIMIT 2");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){

					$que_count = $db->customQuery("SELECT count('quiz_id') as quecount FROM GLC_Quiz_Question WHERE `quiz_id` = ".$key['quiz_id']."");
					if($que_count['status']=="success")
					{
						$counnt = $que_count['data'][0]['quecount'];
					}
					$questatus = $db->customQuery("SELECT attempt_id FROM attempt_ques_ans WHERE `user_id`='$userid' AND `quiz_id` = ".$key['quiz_id']."");
					if($questatus['status']=="success")
					{
						$status = 1;					
					}
					$qtime = $counnt * 20;
					if($qtime < 60) { $quiztime = gmdate("s", $qtime); $m = 'Sec';}
					elseif ($qtime >= 60 && $qtime < 3600) { $quiztime = gmdate("i:s", $qtime); $m = 'Min';}
					else { $quiztime = gmdate("H:i:s", $qtime); $m = 'Hours';}
					$key['q_time'] = $quiztime.' '.$m;
					$key['q_status'] = $status;
					$key['q_count'] = $counnt;
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Quiz list found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/PulltoRefreshQuizList',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$userid = $check['data'][0]['user_id'];
			$created_at = $app->request->params('created_at');
			global $db;
			$create = '';
			if($created_at != 0)
			{
				$create = "AND created_at > '$created_at'";
			}
			$curentdt = date('d-m-Y');
			$image = '';
			$status = 0;
			$counnt = 0;
			$category = $db->customQuery("SELECT * FROM GLC_Quiz WHERE is_active = 1 AND `quiz_end_date` >= '$curentdt' ".$create." ORDER BY quiz_id DESC LIMIT 2");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){

					$que_count = $db->customQuery("SELECT count('quiz_id') as quecount FROM GLC_Quiz_Question WHERE `quiz_id` = ".$key['quiz_id']."");
					if($que_count['status']=="success")
					{
						$counnt = $que_count['data'][0]['quecount'];
					}
					$questatus = $db->customQuery("SELECT attempt_id FROM attempt_ques_ans WHERE `user_id`='$userid' AND `quiz_id` = ".$key['quiz_id']."");
					if($questatus['status']=="success")
					{
						$status = 1;					
					}
					$qtime = $counnt * 20;
					if($qtime < 60) { $quiztime = gmdate("s", $qtime); $m = 'Sec';}
					elseif ($qtime >= 60 && $qtime < 3600) { $quiztime = gmdate("i:s", $qtime); $m = 'Min';}
					else { $quiztime = gmdate("H:i:s", $qtime); $m = 'Hours';}
					$key['q_time'] = $quiztime.' '.$m;
					$key['q_status'] = $status;
					$key['q_count'] = $counnt;
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Quiz list found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/Quiz_Question_Answer',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			global $db;
			$quizid = $app->request->params('quizid');
			$category = $db->customQuery("SELECT * FROM GLC_Quiz_Question WHERE quiz_id = '$quizid' ORDER BY quiz_question_id ASC");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					unset($key['correct_answer']);
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Quiz list found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->post('/User_attempt_questions',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$json1 = file_get_contents('php://input');
			if(!empty($json1))
			{
				global $db;
				$data = json_decode($json1);
				$userid = $check['data'][0]['user_id'];
				$qcount = 0;
				$countrightans = array();
				for ($i=0; $i < count($data->quizdata); $i++)
                {
					$insert = $db->insert("attempt_ques_ans",array('user_id'=>$userid,'quiz_id'=>$data->quizid,'ques_id'=>$data->quizdata[$i]->question_id,'ans_id'=>$data->quizdata[$i]->answer_id,'created_at'=>militime),array());
				
					$counttt = $db->select("GLC_Quiz_Question","correct_answer",array('quiz_id'=>$data->quizid,'quiz_question_id'=>$data->quizdata[$i]->question_id,'correct_answer'=>$data->quizdata[$i]->answer_id));
					if($counttt['status']=="success")
					{
						$countrightans[] = $counttt['data'][0]['correct_answer'];
					}
					$c[] = $data->quizdata[$i]->question_id;
				}
				$query=$db->customQuery("SELECT count('quiz_question_id') as quizcount FROM `GLC_Quiz_Question` WHERE `quiz_id` = '".$data->quizid."'");
				$countq = count($countrightans);
				$att_q = count($c);
				$qcount = $query['data'][0]['quizcount'];

				if($insert['status']=="success")
				{
					$inserttotal = $db->insert("GLC_quiz_user",array('user_id'=>$userid,'quiz_id'=>$data->quizid,'total_ques_count'=>$qcount,'total_attempt_ques'=>$att_q,'total_correct_answer'=>$countq,'created_at'=>militime),array());
				
					$insert['status'] = "success";
					$insert['message'] = "Successfully Submited";
					unset($insert['data']);
					echoResponse(200,$insert);
				}else
				{
					unset($insert['data']);
					$insert['status'] = "failed";
					$insert['message'] = "Error to submited";
					echoResponse(200,$insert);
				}
			}else
			{
				$check['status'] = "failed";
				$check['message'] = "No Request Parameter";
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/Application_status',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$userid = $check['data'][0]['user_id'];
			global $db;
			$application = $db->customQuery("SELECT * FROM user WHERE user_id = '$userid'");
			if($application['status']=="success")
			{
				$arr = array(
						'application_id'	=>	$application['data'][0]['application_id'],
						'applicant_name'	=>	$application['data'][0]['applicant_name'],
						'contact'			=>	$application['data'][0]['contact'],
						'application_status'=>	$application['data'][0]['application_status'],
						'appointdate'		=>	$application['data'][0]['appoint_date'],
						'appointtime'		=>	$application['data'][0]['appoint_time']
						);
				$application['status'] = "success";
				$application['message'] = "Successfull";
				$application['data'] = $arr;
				echoResponse(200,$application);
			}else
			{
				unset($application['data']);
				$application['status'] = "failed";
				$application['message'] = "No data found";
				echoResponse(200,$application);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

function distance($lat1, $lon1, $lat2, $lon2, $unit)
{
	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);
	if ($unit == "K") {
	return ($miles * 1.609344);
	} else if ($unit == "N") {
	  return ($miles * 0.8684);
	} else {
	    return $miles;
	}
}
function echoResponse($status_code, $response) {
    global $app;
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response,JSON_NUMERIC_CHECK);
}
$app->run();
?>