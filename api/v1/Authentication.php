<?php
require '.././libs/Slim/Slim.php';
require_once 'dbHelper.php';
require_once 'auth.php';
require_once 'gcm.php';

//GCM key = AIzaSyDelbOBvjrceX1_6j2vZA9p0NcFMEiCGE8

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app = \Slim\Slim::getInstance();
$db = new dbHelper();

date_default_timezone_set("Asia/Kolkata");
$base_url = "http://base3.engineerbabu.com:8282/blue_trade_api/"; 
$dateTime = date("Y-m-d H:i:s", time()); 
$militime=round(microtime(true) * 1000);
define('dateTime', $dateTime);
define('base_url', $base_url);
define('militime', $militime);



$app->get('/VerifyEmail', function() use ($app){
    $user_id = base64_decode($app->request()->params('secretid'));
    $code = $app->request()->params('secret_key');

    $condition = array('user_id'=>$user_id);
	global $db;
	$content = '';
    $rows = $db->select("user","*",$condition);
    
    if($rows["status"]=="success")
    {
        if($code != '' && $code != 'null')
        {
        	$verify_code = hash('sha256',$rows['data'][0]['email_code']);

        	if($verify_code == '' && $rows['data'][0]['email_status']==1)
	        {
	        	// already verified
	        	/*$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'7'),array());
	            if($sel_temp['status'] =="success")
	            {
	                $content = $sel_temp['data'][0]['content'];
	            }
	            $message=stripcslashes($content);
	          	$message=str_replace("{date}",date("d"),$message);
	          	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
	          	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
	          	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	          	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);*/
	          	$message = "<img src=".base_url."api/v1/images/BlueTrade_Popup_02.jpg style='width:80%; height:90%; margin-left: 12%;'>";
	            echo $message;exit;
	        }elseif($code == $verify_code)
	        {
	            /*$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'3'),array());
	            if($sel_temp['status'] =="success")
	            {
	                $content = $sel_temp['data'][0]['content'];
	            }
	            $message=stripcslashes($content);
	          	$message=str_replace("{date}",date("d"),$message);
	          	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
	          	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
	          	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	          	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);
	            */
	            $rows3 = $db->update("user",array("email_status"=>1,'email_code'=>''),$condition,array());
	            $message = "<img src=".base_url."api/v1/images/BlueTrade_Popup_01.jpg style='width:80%; height:90%; margin-left: 12%;'>";
	            echo $message;exit;
	            //echo $message;exit;
	        }else
	        {
	            /*$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'4'),array());
	            if($sel_temp['status'] =="success")
	            {
	                $content = $sel_temp['data'][0]['content'];
	            }
	            $message=stripcslashes($content);
	            $message=str_replace("{date}",date("d"),$message);
	          	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
	          	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
	          	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	          	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);*/
	          	$message = "<img src=".base_url."api/v1/images/BlueTrade_Popup_02.jpg style='width:80%; height:90%; margin-left: 12%;'>";
	            echo $message;exit;
	            //echo $message;exit;
	        }	
	    }else
	    {
	    	$message = "<img src=".base_url."api/v1/images/BlueTrade_Popup_02.jpg style='width:80%; height:90%; margin-left: 12%;'>";
	        echo $message;exit;	
	    }
    }else
    {
        $message = "<img src=".base_url."api/v1/images/BlueTrade_Popup_02.jpg style='width:80%; height:90%; margin-left: 12%;'>";
	    echo $message;exit;
    }
});

function echoResponse($status_code, $response) {
    global $app;
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response,JSON_NUMERIC_CHECK);
}


$app->run();
?>