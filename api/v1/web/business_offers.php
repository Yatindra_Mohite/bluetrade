<!-- Content Wrapper. Contains page content -->
<style type="text/css">

.deletephoto{
color:red; 
}

.setspan{
color:red;
font-weight: 100%;
font-size: 16px;
}
</style>
<script type="text/javascript">
  $("#nav_business_page").addClass('treeview active');
  $("#nav_business_offers").addClass('active');
</script>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Business Offers
      </h1>
      <?php echo breadcrumbs($breadcrumbs); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <span id="paramassage"></span>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <button type="button" id="testimonialAdd" class="btn btn-primary testimonialAdd" data-toggle="modal" data-target="">Add Content</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
                  <label for="inputEmail3" class="control-label col-sm-2">Select User</label>
                   <div class="col-sm-3">
                    <select class="form-control" name="users" id="users" onchange="getUserList()" required>
                      <option value="">Select Franchise</option>
                      <?php
                      foreach ($getUsers as $getUser) 
                      {
                        ?>
                        <option value="<?php echo $getUser->user_id; ?>"><?php echo $getUser->username; ?></option>
                      <?php
                        }
                      ?>
                    </select>
                    <span id="user_type1" class="setspan" ></span>
                  </div>
              </div>
              <div id="abc"></div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
        <!-- Modal -->
            <div class="modal fade " id="openAboutModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
              
                  <span id="comassage"></span>
                  <form method="POST" name="homeForm" id="homeForm" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-header modal-header-primary">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <h2 class="modal-title" id="myModalLabel">Add Business Offers</h2>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                        <div class="col-sm-9">
                          <input type="hidden" name="offers_id" id="offers_id" class="form-control">
                          <input type="hidden" name="user_name" id="user_name" class="form-control">
                        </div>
                      </div>
                      <div class="form-group userid">
                        <label for="inputEmail3" class="control-label col-sm-3">Franchise</label>
                         <div class="col-sm-9">
                          <select class="form-control" name="user_id" id="user_id" required>
                            <option value="">Select Franchise</option>
                            <?php
                            foreach ($getUsers as $getUser) 
                            {
                              ?>
                              <option value="<?php echo $getUser->user_id; ?>"><?php echo $getUser->username; ?></option>
                            <?php
                              }
                            ?>
                          </select>
                          <span id="user_type1" class="setspan" ></span>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Offers Content:</label>
                        <div class="col-sm-9">
                          <textarea  class="form-control" name="offers_description" id="offers_description" rows="10"></textarea>
                          <span id="footer_contenterror" class="setspan" ></span>
                        </div>
                      </div>                
                      <div class="form-group">
                      <label class="control-label col-sm-3" for="email">Offers Image:</label>
                        <div class="col-sm-5">
                          <input type="file" name="image" id="image" class="btn btn-block" style="background: #f6eded;" onchange="readURL(this)" >
                          <span id="imageerror" class="setspan" ></span>
                        </div>
                        <div class="col-sm-4">
                          <img class="home_image_show">                     
                            <div class="singleImage" id="singleImage"></div>
                        </div> 
                      </div>          
                      <div class="modal-footer">
                        <button type="button"  class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" name="submit" id="update"  class="btn btn-primary update">Save changes</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!-- end of modal -->
    </section>
  </div>
<?php if(isset($scripts)){
    foreach($scripts as $script){  ?>
      <script type="text/javascript" src="<?php echo ADMIN_ASSETS_URL.$script.'.js';?>"></script>
<?php }} ?>

<script type="text/javascript">
   $(function () {
    //$("#example1").DataTable();
    $('#example3').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true
    });
  });

   $('body').on('click','.status_checks',function(){ 
  var status = jQuery(this).attr('status');
  ///var status = (jQuery(this).attr('status')) ? '0' : '1'; 
  if(status == 1){
    status = 0;
  }else{
    status = 1;
  }
  var msg = (status=='0')? 'Un Approved' : 'Approve'; 
  var msg = (status=='1')? 'Approve' : 'Un Approved'; 
  if(confirm("Are you sure to "+ msg))
  { 
      //var current_element = $(this); 
      //alert(current_element);
      //var id = $(current_element).attr('data');
      var user_id = $('#users').val();
      var id = jQuery(this).attr('id');
      //alert(id);
      url = "<?php echo base_url().'admin/update_status'?>"; 
          $.ajax({
            type:"POST",
            url: url, 
            data: {"bo_id":id,"status":status,user_id:user_id}, 
            success: function(data) { 
              //alert(data);
            $("#abc").html(data);
            event.preventDefault();
            //location.reload();
      } });
   }  
 });



  function getUserList(){
    var user_id = $('#users').val();
    //alert(user_id);return false;
    base_url = "<?php echo base_url(); ?>";
    if (user_id != ""){
        $.post(base_url +'admin/getBusinessOffersList', {user_id:user_id}, function(data){
            $("#abc").html(data);
            //console.log(data);
        })
    }else{
      $("#abc").html('');
    }//end if
  }

   /*Function for retrive data in on delete popup */
  $('body').on('click', '.delete', function(event) {
    event.preventDefault();
    if (confirm('Are you sure to Delete ?')) {
      var offers_id = jQuery(this).attr('id');
      jQuery.ajax({
        type: "POST",
        "dataSrc": "Data",
        url: "<?php echo base_url().'admin/business_offers_delete_record'?>",
        dataType: 'json',
        data: {offers_id: offers_id,table:'business_offers',where:'bo_id'},
        success: function(res) {
          if(res==1){
            $('#paramassage').html('<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Delete!</strong> tesimonial Successfully.</div>') 
            $('#tr_'+offers_id).fadeOut('slow');
        setTimeout(function(){ 
          $('#paramassage').html('');
        },2000); 
          }else{
            alert('error');
          }
        }
      });
    }
  });

/* Function for retrive data on edit model popup */

  $('body').on("click", '.testimonialAdd', function(event) { 
    $('#homeForm' ).trigger("reset");
    var img=''; 
    $(".home_image_show").attr("src", img).width('0px').height('0px');
    $("#offers_description").val('');
    $("#user_id").val('');
    $("#image").val('');
    $("#offers_id").val('');
    $('.singleImage').html('');
    $('.userid').show();
    $('#openAboutModel').modal('show');
  });

  $('body').on("click", '.edit', function(event) { 
    $('#homeForm' ).trigger("reset");
    $('.userid').hide();
    $('.singleImage').html('');
    $('.home_image_show').html('');
    var base_url="<?php echo base_url(); ?>";
    var image = $(this).attr('data-image');
    var offers_description = $(this).attr('data-content');
    var user_id = $(this).attr('data-userid');
    var user_name = $(this).attr('data-username');
    var id = $(this).data('id');
    $("#offers_description").val(offers_description);
    var path=base_url+'assets/img/offers_list/thumb/';
    var headerImage11=path+image;
    $(".home_image_show").attr("src", headerImage11).width('100px').height('80px').css('display','block');
    $("#offers_id").val(id);
    $("#user_name").val(user_name);
    $("#user_id").val(user_id);
    $('#openAboutModel').modal('show');
  });

  $('body').on("click", '.update', function() {
    var id=$('#offers_id').val();
    var offers_description=$('#offers_description').val();
    var image=$('#image').val();

  // if($("#image").val() ==""){
  //   if($(".home_image_show").val() ==""){
  //     $('#Imageerror').html('image is required'); 
  //     setTimeout(function(){ 
  //       $('#Imageerror').html('');
  //     },2000);
  //     return false;
  //   }
  // }
  if(image != '')
  { 
    var valid_extensions = /(.jpg|.jpeg|.png)$/i;

    if(valid_extensions.test(image))
    { 
    }
    else
    {
      $('#imageerror').html("Please upload files having extensions: <b>" + valid_extensions + "</b> only."); 
      
      setTimeout(function(){ 
        $('#imageerror').html('');
      },2000);
      
      return false;
    }
  }
  if(offers_description==""){
    $('#footer_contenterror').html('testimonial content is required'); 
    setTimeout(function(){ 
      $('#footer_contenterror').html('');
    },2000);
    return false;
  }
  if(user_id==""){
    $('#user_type1').html('Please select user type.'); 
    setTimeout(function(){ 
      $('#user_type1').html('');
    },2000);
    return false;
  }
  var data = new FormData($('#homeForm')[0]);
  if(id !=""){
    url ="update_business_offers";
  }else{
    url ="add_business_offers";
  }
  $.ajax({ 
    type:"POST",
    url:url,
    data:data,
    dataType: 'json',
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success:function(result)
    {
      if(result==1){
        $('#paramassage').html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Error!</strong> Tesimonial already exists.</div>') 
        setTimeout(function(){ 
          $('#paramassage').html('');
        },2000); 
        $('#homeForm').trigger("reset");
        $('#openAboutModel').modal('hide');
        return false;
      }else{
        if(id !==""){
          $("#tr_"+id).html(result);
          $('#paramassage').html('<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Success!</strong> You Performation Action Successfully.</div>') 
          setTimeout(function(){ 
            $('#paramassage').html('');
          },2000); 
          $('#homeForm').trigger("reset");
          $('#openAboutModel').modal('hide');
          return true;
        }
        else{
          $("#example3 tbody").append(result);
          $('#paramassage').html('<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Success!</strong> You Performation Action Successfully.</div>') 
          setTimeout(function(){ 
            $('#paramassage').html('');
          },2000); 
          $('#openAboutModel').modal('hide');
          $('#homeForm' ).trigger("reset");
          return true;
        } 
      }   
    }
  });
});

</script>
<script type="text/javascript">
function readURL(input) {
if (input.files && input.files[0]) {
var reader = new FileReader();

var imagesPreview = $(".singleImage");

if(imagesPreview){
  $(".home_image_show").hide();        
}else{
  $(".home_image_show").show();
  $(".singleImage").hide();
}
imagesPreview.html("");
reader.onload = function (e) {
  var img = $("<img />");
  img.attr("style", "height:100px;width:108px");
  img.attr("src", e.target.result);
  imagesPreview.append(img);
}
reader.readAsDataURL(input.files[0]);
}
}
</script>
<script type="text/javascript">
  $( document ).ready(function() {
    var user_id = '<?php echo $this->uri->segment('3'); ?>'
    if(user_id != ''){
      var base_url = "<?php echo base_url(); ?>";
      if (user_id != ""){
          $.post(base_url +'admin/getBusinessOffersList', {user_id:user_id}, function(data){
              $("#abc").html(data);
              //console.log(data);
          })
      }else{
        $("#abc").html('');
      }
    }
});
</script>
