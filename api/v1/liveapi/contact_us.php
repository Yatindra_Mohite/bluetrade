<script type="text/javascript">
  $("#nav_contact").addClass('active');
</script>
<style type="text/css">
    .red{
        color:red;
    }
    .error{
        color:red;
    }
    .form-area
    {
        background-color: #FAFAFA;
        padding: 0px 40px 52px;
        margin: 20px 0px 60px;
        border: 1px solid GREY;
    }
</style>
<!--start search-->
	<section class="adviser_search">
		<div class="container">
			<div class="row">
				<form action="">
				<!--col3-->
				<div class="col-sm-3">
					<p class="looking_p">Looking for an Advisor, search here:</p>
				</div>
				<!--col3-->
				
				<!--col3-->
				<div class="col-sm-3">
					<div class="form-group form_select">
						<select class="form-control" name="type" id="type">
              <option value="">Service Type</option>
              <option value="1">Provided</option>
              <option value="2">Supported</option>
          </select>
					</div>
          <div id="error"></div>
				</div>
				<!--col3-->
				
				<!--col3-->
				<div class="col-sm-3">
					<div class="form-group">
						<div class="input-group location_marker">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
						  <input class="form-control" id="search" type="text" placeholder="Suburb or Postcode">
						   <span class="input-group-addon"><i class="fa fa-bullseye fa-fw" aria-hidden="true"></i></span>
						</div>
					</div>
          <div id="error2"></div>
				</div>
				<!--col3-->
				
				<!--col3-->
				<div class="col-sm-3">
					<a href="javascript:void(0)" onclick="searchSubmit();" class="search_button"><i class="fa fa-search" aria-hidden="true"></i> Search</a>
				</div>
				<!--col3-->
				
				</form>
			</div>
		</div>
	</section>
	<!--end search-->
	  
	  
         <section class="landing_page">
            <div class="container">
              <div  class="outer_body">
                <div class="inner_body">
                  <div class="row">
                      <div class="col-md-6">
                        <div class="result_map">
                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28525.496997443257!2d153.06553253689236!3d-26.658498289696826!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b9377b0e10b3b2d%3A0x502a35af3deaba0!2sMaroochydore+QLD+4558%2C+Australia!5e0!3m2!1sen!2sin!4v1495786304715" width="100%" height="547" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-area">  
                              <form role="form" method="POST" id="contactFrom" action="<?php echo base_url() ?>site/submit_contact_us">
                              <br style="clear:both">
                                          <h3 style="margin-bottom: 25px; text-align: center;">Contact Form</h3>
                          <?php if($this->session->flashdata('success') != null) : ?>
                            <?php echo $this->session->flashdata('success');?>
                            <?php endif; ?>
                                  <div class="form-group">
                                  <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo set_value('name') ?>"><?php echo form_error('name'); ?>
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo set_value('email') ?>"><?php echo form_error('email'); ?>
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="<?php echo set_value('mobile') ?>"><?php echo form_error('mobile'); ?>
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" value="<?php echo set_value('subject') ?>"><?php echo form_error('subject'); ?>
                                </div>
                                          <div class="form-group">
                                          <textarea class="form-control" type="textarea" name ="message" id="message" placeholder="Message" maxlength="140" rows="7"><?php echo set_value('message'); ?> </textarea><?php echo form_error('message'); ?>
                                              <span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>                    
                                          </div>
                                  
                              <button type="submit" class="btn btn-primary pull-right">Submit Form</button>
                              </form>
                          </div>
                      </div>
                  </div>
                     <!--<div class="srow_margin ">-->
                     <!--</div>-->
                 </div>
              </div> 
            </div>
          </section>

<script type="text/javascript">
  $(document).ready(function(){ 
    $('#characterLeft').text('140 characters left');
    $('#message').keydown(function () {
        var max = 140;
        var len = $(this).val().length;
        if (len >= max) {
            $('#characterLeft').text('You have reached the limit');
            $('#characterLeft').addClass('red');
            $('#btnSubmit').addClass('disabled');            
        } 
        else {
            var ch = max - len;
            $('#characterLeft').text(ch + ' characters left');
            $('#btnSubmit').removeClass('disabled');
            $('#characterLeft').removeClass('red');            
        }
    });    
});

</script>