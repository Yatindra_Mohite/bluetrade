<?php
require '.././libs/Slim/Slim.php';
require_once 'dbHelper.php';
require_once 'auth.php';
require_once 'gcm.php';

//GCM key = AIzaSyDelbOBvjrceX1_6j2vZA9p0NcFMEiCGE8

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app = \Slim\Slim::getInstance();
$db = new dbHelper();

date_default_timezone_set("Asia/Kolkata");
$base_url = "http://base3.engineerbabu.com:8282/blue_trade_api/"; 
$dateTime = date("Y-m-d H:i:s", time()); 
$militime=round(microtime(true) * 1000);
define('dateTime', $dateTime);
define('base_url', $base_url);
define('militime', $militime);


$app->post('/Registration',function() use ($app){
	$json1 = file_get_contents('php://input');
	if(!empty($json1))
	{
	    $data = json_decode($json1);
		$username= $data->username;
		$mobile = $data->mobile;
		$email= $data->email;
		$password = $data->password;
		
		if(!empty($email) && !empty($mobile) && !empty($password))
		{
			global $db;
			$code = '1234'; //substr(randomuniqueCode(),0,6);
			$Otp = '1234';
			$user_data = array('User_name'=>$username,
				'Email'=>$email,
				'Contact_num'=>$mobile,
				'Password'=>hash('sha256', $password),
				'Verify_code'=>hash('sha256', $code),
				'Otp_code'=>hash('sha256', $Otp),
				'Create_at'=>militime,
				'Update_at'=>militime
			);
			$condition = array('Contact_num'=>$mobile);
			$condition2 = array('Email'=>$email);
			$query_login = $db->select("BlueTrade_User","*",$condition2);
			//sendsms($mobile,$Otp);
			if($query_login["status"] == "success")
			{	
					if($query_login['data'][0]['admin_status'] == 1)
					{
						if($query_login['data'][0]['mobile_status']==1 && $query_login['data'][0]['email_status']==1)
						{
				            $query_login['status'] = "failed";
							$query_login['message'] ="Mobile Number & Email Address Already Exists.";
							unset($query_login['data']);
							echoResponse(200,$query_login);
				    	}
				    	else
						{
							if($query_login['data'][0]['mobile_status']==0)
							{
								$otpcode = hash('sha256', $Otp);
								$passwordd = hash('sha256', $password);
								$rows3 = $db->update("BlueTrade_User", array('Otp_code'=>$otpcode,'User_name'=>$username,'Password'=>$passwordd,'Contact_num'=>$mobile,'Update_at'=>militime),array('User_id'=>$query_login['data'][0]['User_id']),array());
    							//sendsms($mobile,$Otp);
    							sendsms($mobile,'Your OTP for BlueTrade is: 1234');
								$query_login['status']="success";
					            $query_login["message"] = "Successfully Registered";
								$query_login['data'] = array('user_id'=>$query_login['data'][0]['User_id'],'User_name'=>$username,'Email'=>$email,'Contact_num'=>$mobile);
					            echoResponse(200, $query_login);
							}
							elseif($query_login['data'][0]['email_status']==0)
							{
								$query_login['status']="success";
					            $query_login["message"] = "Please Check Your Email if you have already Verification Email OR Click On Resend button For Email Verification Link";
								$query_login['data'] = array('user_id'=>$query_login['data'][0]['User_id'],'User_name'=>$username,'Email'=>$email,'Contact_num'=>$mobile);
					            echoResponse(200, $query_login);
							}
						}
					}else
					{
						$query_login['status'] = "failed";
						$query_login['message'] = "Your BlueTrade account and has been temporarily suspended as a security precaution.";
						unset($query_login['data']);
						echoResponse(200,$query_login);	
					}
			}
			else
			{ 
				$insert_user = $db->insert("BlueTrade_User",$user_data,array());
				if($insert_user["status"] == "success")
				{
					sendsms($mobile,'Your OTP for BlueTrade is: 1234');
					$insert_user['status']="success";
		            $insert_user["message"] = "Successfully Registered";
					$insert_user['data'] = array('user_id'=>$insert_user['data'],'User_name'=>$username,'Email'=>$email,'Contact_num'=>$mobile);
				   	echoResponse(200, $insert_user);
					/*$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'1'),array());
		            if($sel_temp['status'] =="success")
		            {
		                foreach($sel_temp['data'] as $key3)
		                {
		                    $content = $key3['content'];
		                }
		            }else
	              	{
	                    $content = ''; 
	              	}
		            $subject = "Blue Trade App: Verification Link";
	               	
			        $message=stripcslashes($content);
	              	$message=str_replace("{date}",date("d"),$message);
	              	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
	              	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
	              	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	              	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);
		          	$message=str_replace("{email}",$email,$message);
	              	$message=str_replace("{link}","<a href=".base_url."api/v1/blue_trade_api.php/VerifyEmail?secretid=".base64_encode($insert_user['data'])."&secret_key=".hash('sha256',$code).">CLICK HERE</a>",$message);
		            $email_from ='no-reply@bluetrade.com';
		        	$headers  = 'MIME-Version: 1.0' . "\r\n";
		            $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
		            $headers .= 'From: '.$email_from. '\r\n';
		            $cc = '';
		           
		        	if(@mail($email, $subject, $message,$headers))
			           {
			           		sendsms($mobile,'Your OTP for BlueTrade is: 1234');
	    					$insert_user['status']="success";
				            $insert_user["message"] = "Successfully Registered";
							$insert_user['data'] = array('user_id'=>$insert_user['data'],'User_name'=>$username,'Email'=>$email,'Contact_num'=>$mobile);
				            echoResponse(200, $insert_user);
			           }else
			           {
			           		$insert_user['status'] = "failed";
							$insert_user['message'] ="Something went wrong! Please try again later";
							unset($insert_user['data']);
							echoResponse(200,$insert_user);
			           }*/
		      		//$senddd = Send_Mail($email_from,$email,$cc,$subject,$message);
				}else
				{
					$insert_user['status']="failed";
		            $insert_user["message"] = "Registration failed.";
					unset($insert_user['data']);
		            echoResponse(200, $insert_user);
				}
			}
		}
		else
		{
			$check_otp['message']= "Invalid Request parameter";
			echoResponse(200,$check_otp);
		}
	}else
	{
		$check_otp['message'] ="No Request parameter";
		echoResponse(200,$check_otp);
	}
});

$app->post('/Resend_verification_link', function() use ($app)
{
	$email = $app->request()->params('email');
	$code = '1234'; //substr(randomuniqueCode(),0,6);
	global $db;
	$query_login = $db->select("BlueTrade_User","*",array('Email'=>$email));
	if($query_login['status']=="success")
	{		
			$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'1'),array());
            if($sel_temp['status'] =="success")
            {
                foreach($sel_temp['data'] as $key3)
                {
                    $content = $key3['content'];
                }
            }else
          	{
                $content = ''; 
          	}
            $subject = "Blue Trade App: Verification Link";
           	
	        $message=stripcslashes($content);
          	$message=str_replace("{date}",date("d"),$message);
          	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
          	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
          	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
          	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);
          	$message=str_replace("{email}",$email,$message);
          	$message=str_replace("{link}","<a href=".base_url."api/v1/blue_trade_api.php/VerifyEmail?secretid=".base64_encode($query_login['data'][0]['User_id'])."&secret_key=".hash('sha256',$code).">CLICK HERE</a>",$message);
            $email_from ='no-reply@bluetrade.com';
        	$headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
            $headers .= 'From: '.$email_from. '\r\n';
            $cc = '';
			           
        	if(@mail($email, $subject, $message,$headers))
           	{
           		$query_login['status']="success";
	            $query_login["message"] = "Email Verification Link has been successfully sent";
				$query_login['data'] = array('user_id'=>$query_login['data'][0]['User_id'],'User_name'=>$query_login['data'][0]['User_name'],'Email'=>$email,'Contact_num'=>$query_login['data'][0]['Contact_num']);
	            echoResponse(200, $query_login);
           	}else
           	{
           		$query_login['status'] = "failed";
				$query_login['message'] ="Something went wrong! Please try again later";
				unset($query_login['data']);
				echoResponse(200,$query_login);
           	}	
	}else
	{
		$query_login['status'] = "failed";
		$query_login['message'] = "Email Address does not exists";
		unset($query_login['data']);
		echoResponse(200,$query_login);
	}
});

$app->post('/Mobile_verification', function() use ($app){
    $json1 = file_get_contents('php://input');
	if(!empty($json1))
	{
		$data = json_decode($json1);
	    if($data->user_id != '' && $data->user_id != 0 && $data->verify_code != '' && strlen($data->verify_code) == 4)
	    {
	    	global $db;
		    $code = '1234';
		    $rows = $db->select("BlueTrade_User","*",array('User_id'=>$data->user_id));
		    if($rows["status"]=="success")
		    {
		    	//echo hash('sha256', $data->verify_code);exit;
		      	if($rows["data"][0]['Otp_code'] == hash('sha256', $data->verify_code))
		      	{
		      		$update = $db->update("BlueTrade_User",array('Otp_code'=>'','mobile_status'=>1,'Update_at'=>militime),array('User_id'=>$data->user_id),array());
		      		if($update['status']=="success")
		      		{
		      			$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'1'),array());
			            if($sel_temp['status'] =="success")
			            {
			                foreach($sel_temp['data'] as $key3)
			                {
			                    $content = $key3['content'];
			                }
			            }else
		              	{
		                    $content = ''; 
		              	}
			            $subject = "Blue Trade App: Verification Link";
		               	
				        $message=stripcslashes($content);
		              	$message=str_replace("{date}",date("d"),$message);
		              	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
		              	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
		              	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
		              	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);
			          	$message=str_replace("{email}",$rows["data"][0]['Email'],$message);
		              	$message=str_replace("{link}","<a href=".base_url."api/v1/blue_trade_api.php/VerifyEmail?secretid=".base64_encode($data->user_id)."&secret_key=".hash('sha256',$code).">CLICK HERE</a>",$message);
			            $email_from ='no-reply@bluetrade.com';
			        	$headers  = 'MIME-Version: 1.0' . "\r\n";
			            $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
			            $headers .= 'From: '.$email_from. '\r\n';
			            $cc = '';
			           
			        	if(@mail($rows["data"][0]['Email'], $subject, $message,$headers))
				           {
				           		$update['status']="success";
					            $update["message"] = "Mobile Number Successfully Verified";
								unset($update['data']);
					            echoResponse(200, $update);
				           }else
				           {
				           		$update['status'] = "failed";
								$update['message'] ="failed";
								unset($update['data']);
								echoResponse(200,$update);
				           }
		      		}
		      	}else
		      	{
		      		$rows['status']="failed";
		            $rows["message"] = "Otp not matched.";
					unset($rows['data']);
		            echoResponse(200, $rows);
		      	}     
		    }else
		    {
		       $rows["status"] = 'failed';
		       $rows["message"]= "Invalid Request";
		       unset($rows["data"]);
		       echoResponse(200, $rows);
		    }
	    }else
	    {
	    	$check_otp['message']= "Invalid Request parameter";
			echoResponse(200,$check_otp);
	    }
	}else
	{
		$check_otp['message'] ="No Request parameter";
		echoResponse(200,$check_otp);
	}
});


$app->get('/VerifyEmail', function() use ($app){
    $user_id = base64_decode($app->request()->params('secretid'));
    $code = $app->request()->params('secret_key');

    $condition = array('User_id'=>$user_id);
	global $db;
	$content = '';
    $rows = $db->select("BlueTrade_User","*",$condition);
    
    if($rows["status"]=="success")
    {
        if($code != '' && $code != 'null')
        {
        	$verify_code = $rows['data'][0]['Verify_code'];
        	if($verify_code == '' && $rows['data'][0]['email_status']==1)
	        {
	        	// already verified
	        	/*$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'7'),array());
	            if($sel_temp['status'] =="success")
	            {
	                $content = $sel_temp['data'][0]['content'];
	            }
	            $message=stripcslashes($content);
	          	$message=str_replace("{date}",date("d"),$message);
	          	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
	          	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
	          	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	          	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);*/
	          	$message = "<img src=".base_url."api/v1/images/BlueTrade_Popup_02.jpg style='width:80%; height:90%; margin-left: 12%;'>";
	            echo $message;exit;
	        }elseif($code == $verify_code)
	        {
	            /*$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'3'),array());
	            if($sel_temp['status'] =="success")
	            {
	                $content = $sel_temp['data'][0]['content'];
	            }
	            $message=stripcslashes($content);
	          	$message=str_replace("{date}",date("d"),$message);
	          	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
	          	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
	          	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	          	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);
	            */
	            $rows3 = $db->update("BlueTrade_User",array("email_status"=>1,'Verify_code'=>''),$condition,array());
	            $message = "<img src=".base_url."api/v1/images/BlueTrade_Popup_01.jpg style='width:80%; height:90%; margin-left: 12%;'>";
	            echo $message;exit;
	            //echo $message;exit;
	        }else
	        {
	            /*$sel_temp = $db->select("email_template_descriptions","*",array('id'=>'4'),array());
	            if($sel_temp['status'] =="success")
	            {
	                $content = $sel_temp['data'][0]['content'];
	            }
	            $message=stripcslashes($content);
	            $message=str_replace("{date}",date("d"),$message);
	          	$message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
	          	$message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
	          	$message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	          	$message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);*/
	          	$message = "<img src=".base_url."api/v1/images/BlueTrade_Popup_02.jpg style='width:80%; height:90%; margin-left: 12%;'>";
	            echo $message;exit;
	            //echo $message;exit;
	        }	
	    }else
	    {
	    	$message = "<img src=".base_url."api/v1/images/BlueTrade_Popup_02.jpg style='width:80%; height:90%; margin-left: 12%;'>";
	        echo $message;exit;	
	    }
    }else
    {
        $message = "<img src=".base_url."api/v1/images/BlueTrade_Popup_02.jpg style='width:80%; height:90%; margin-left: 12%;'>";
	    echo $message;exit;
    }
});

$app->post('/Login',function() use ($app){
	$json1 = file_get_contents('php://input');
	if(!empty($json1))
	{
	    $data = json_decode($json1);
		$email = $data->email;
		$password = $data->password;
		$device_token = $data->device_token;
		$device_id = $data->device_id;
		global $db;
		if(!empty($email) && !empty($password))
		{ 
			$token = bin2hex(openssl_random_pseudo_bytes(16));
			$token = $token.militime;
			$query_login = $db->select("BlueTrade_User","*",array("Email"=>$email,"Password"=>hash('sha256',$password)));
			
			if($query_login["status"] == "success")
			{	
				if($query_login['data'][0]['email_status']==1 && $query_login['data'][0]['mobile_status']==1)
				{
					$update = $db->update("BlueTrade_User",array('Token'=>$token,'Device_token'=>$device_token,'Device_id'=>$device_id,'Update_at'=>militime),array("Email"=>$email),array());
					
						if(!empty($query_login['data'][0]['user_image'])){
							$image = base_url.'uploads/user_image/'.$query_login['data'][0]['user_image'];
						}else{
							$image = '';
						}
						//print_r($image); exit;
						$arr = array(
								'User_name'=>$query_login['data'][0]['User_name'],	
								'Email'=>$query_login['data'][0]['Email'],	
								'Contact_num'=>$query_login['data'][0]['Contact_num'],	
								'token'=>$token,	
								'user_image'=>$image
								);
					$query_login['status'] ="success";
					$query_login['message'] ="Successfully Login";
					$query_login['data'] = $arr;
					echoResponse(200,$query_login);
				}elseif($query_login['data'][0]['mobile_status']==0)
				{
					$query_login['status'] ="failed";
					$query_login['message'] ="Account does not exists.";
					unset($query_login['data']);
					echoResponse(200,$query_login);
				}elseif($query_login['data'][0]['email_status']==0)
				{
					$query_login['status'] ="failed";
					$query_login['message'] ="Please Check Your Email if you have already Verification Email OR Click On Resend For Email Verification Link.";
					unset($query_login['data']);
					echoResponse(200,$query_login);
				}
			}
			else
			{ 
				$query_login['status'] = "failed";
				$query_login['message'] ="Invalid Credential";
				unset($query_login['data']);
				echoResponse(200,$query_login);
			}
		}else
		{
			$insert_user['message'] ="Invalid Request parameter";
			echoResponse(200,$insert_user);
		}
	}else
	{
		$insert_user['message'] ="No Request parameter";
		echoResponse(200,$insert_user);
	}
});

$app->post('/ForgetPassword', function() use ($app){

	$email = $app->request()->params('email');
	$condition = array('Email'=>$email);
	global $db;
	$rows = $db->select("BlueTrade_User","User_id,Email,admin_status,email_status",$condition);
	//print_r($rows);exit;
    if($rows['status'] == "success")
    {
		if($rows['data'][0]['admin_status'] == 1)
		{	
			if($rows['data'][0]['email_status'] == 1)
			{
				$userid = $rows['data'][0]['User_id'];
	            $sel_temp = $db->select("email_template_descriptions","*",array('id'=>'2'),array());
	                
	                if($sel_temp['status'] =="success")
	                {
	                    foreach($sel_temp['data'] as $key3)
	                    {
	                      $content = $key3['content'];
	                    }
	                }else
	                {
	                 $content = ''; 
	                }
		        $subject = "Blue Trade App: Forget Password";
		           
		        $message=stripcslashes($content);
		        $message=str_replace("{date}",date("d"),$message);
		        $message=str_replace("images/line-break-3.jpg",base_url.'api/v1/images/email_img/line-break-3.jpg',$message);
		        $message=str_replace("images/line-break-2.jpg",base_url.'api/v1/images/email_img/line-break-2.jpg',$message);
		        $message=str_replace("images/ribbon.jpg",base_url.'api/v1/images/ribbon.jpg',$message);
	            $message=str_replace("logo/bluetrade_logo.png",base_url.'uploads/logo/bluetrade_logo.png',$message);
		        $message=str_replace("{email}",$email,$message);
		        $message=str_replace("{link}","<a href=".base_url."api/v1/web/temp.php?auth_key=".base64_encode($userid)."&id=".militime."&uniqid=".hash('sha256', '1234').">CLICK HERE</a>",$message);
		          //$message=str_replace("{password}",$code,$message);
		           // echo $message; 
		        $email_from='no-reply@bluetrade.com';
		        $headers  = 'MIME-Version: 1.0' . "\r\n";
		        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		        $headers .= 'From: '.$email_from.'\r\n';            // Mail it
		           
		         @mail($email, $subject, $message, $headers);
		          $rows3 = $db->update("BlueTrade_User", array("Verify_code"=>hash('sha256', '1234')), $condition,array());
		          $rows["message"] = "Link has been sent to your registered email address.";
		          unset($rows['data']);
    		}else{
		        $rows["status"] = "failed";
		        $rows["message"] = "Please verify your email address first.";
		        unset($rows['data']);
    		}
    	}else{
	        $rows["status"] = "failed";
	        $rows["message"] = "Your BlueTrade account and has been temporarily suspended as a security precaution.";
		    unset($rows['data']);
    	}
    }else{

        $rows["status"] = "failed";
        $rows["message"] = "Email does not exist";
	    unset($rows['data']);
    }
    echoResponse(200, $rows);
});

$app->post('/ads_by_location', function() use ($app){
 	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
			$lat = $app->request()->params('lat');
			$lng = $app->request()->params('lng');
			$distance = $app->request()->params('distance');
			global $db;
			$finalarr =array();
			$feat_arr = array();
			$trend_arr  = array();
			$hot_deal_arr = array();
		        $get_featured_ads = $db->customQuery("select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance  FROM Ads HAVING distance <= '$distance' AND `ads_status` = '0'  order by distance ASC limit 0,6");
		        if($get_featured_ads['status']=="success")
		        {
	              	foreach($get_featured_ads['data'] as $key_acc)
	              	{
		                $table_id = $key_acc['table_id'];
		                $img_arr = array();
		                $all_image =  $db->customQuery("select Ads_image.ads_image from `Ads_image` where `ad_id` = '$table_id'");
		                if($all_image['status']=="success")
		                {
							foreach($all_image['data'] as $img)
							{
								$img_arr[] = base_url.'uploads/ads_image/'.$img['ads_image'];
							}
		                }
		                $key_acc['Ads_image'] = $img_arr;
		                $feat_arr[] = $key_acc;
	              	}
		        }
		        $finalarr = array(
		              'featured' => $feat_arr,
		              'trending' => $feat_arr,
		              'hot_deal' => $feat_arr
		            );
		        if(!empty($finalarr))
		        {
		          $get_featured_ads['status'] = "success";
		            $get_featured_ads['message'] = "successfully";  
		            $get_featured_ads["data"] = $finalarr;
		            echoResponse(200,$get_featured_ads);
		        }
		        else
		        {
		            $get_featured_ads['status'] = "failed";
		            $get_featured_ads['message'] = "failed";
		            unset($get_featured_ads["data"]);
		            echoResponse(200, $get_featured_ads);
		        }
	    }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/insert_ads', function() use ($app){
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
           $user_id = $check['data'][0]['User_id'];
		   $category_id = $app->request()->params('category_id');
	 	   $category_name = $app->request()->params('category_name');
	       $ad_title = $app->request()->params('ad_title');
	       $ad_description = $app->request()->params('ad_description');
	       $user_name = $app->request()->params('user_name');
	       $user_email = $app->request()->params('user_email');
	       $user_mobile = $app->request()->params('user_mobile');
	       $address = $app->request()->params('address');
	       $price = $app->request()->params('price');
	       $lat = $app->request()->params('lat');
	       $lng = $app->request()->params('lng');
	       global $db;
        	$insert_info =  $db->insert('Ads',array('user_id'=>$user_id,'category_id'=>$category_id, 'category_name'=>$category_name,'ad_title'=>$ad_title,'ad_description'=>$ad_description,'user_name'=>$user_name,'user_mobile'=>$user_mobile,'user_email'=>$user_email,'address'=>$address,'price'=>$price,'lat'=>$lat,'lng'=>$lng,'creat_at'=>militime,'posted_date'=>date('d-m-Y')),array());
           	if($insert_info['status']  == 'success')
           	{
           		if(!empty($_FILES["image"]["name"]) || isset($_FILES["image"]["name"]))
          		{
               		$image = $_FILES["image"]["name"];
            	}
          		else
          		{
             		 $image = array();
          		}
          		$img_arr = array();
          			for($i=0; $i<count($image); $i++)
	          		{
	          			$image1 = md5(militime.$image[$i]).'.png';
	          			move_uploaded_file($_FILES["image"]["tmp_name"][$i],"../../uploads/ads_image/".$image1);
	             		$ins_img = $db->insert('Ads_image',array('ad_id'=>$insert_info['data'],'user_id'=>$user_id,'ads_image'=>$image1),array());
	          			$img_arr[] = base_url.'uploads/ads_image/'.$image1;
	          		}
      		 	if($insert_info['status']=="success")
		        {
		            $insert_info['status'] = "success";
		            $insert_info['message'] = "successfully inserted";
		            $insert_info["data"] = array('category_id'=>$category_id, 'category_name'=>$category_name,'ad_title'=>$ad_title,'ad_description'=>$ad_description,'user_name'=>$user_name,'user_mobile'=>$user_mobile,'user_email'=>$user_email,'address'=>$address,'price'=>$price,'lat'=>$lat,'lng'=>$lng,'creat_at'=>date('d-m-Y'),'ads_image'=>$img_arr);
		        }
		        else
		        {
		            $insert_info['status'] = "failed";
		            $insert_info['message'] = "Data Not Inserted Successfully";
		            unset($insert_info["data"]);
		        }
		      echoResponse(200, $insert_info);
	    	}
        	else
        	{
        		$insert_info['status'] = "failed";
	            $insert_info['message'] = "Data Not Inserted...!";
	            unset($insert_info["data"]);
		      echoResponse(200, $insert_info);
        	}
        }else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/edit_profile', function() use ($app){

  $headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
         $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$user_name = $check['data'][0]['User_name'];
        	$user_mobile = $check['data'][0]['Contact_num'];
        	$user_password = $check['data'][0]['Password'];

        	$name = $app->request()->params('name');
        	$mobile = $app->request()->params('contact');
        	$old_password = hash('sha256', $app->request()->params('old_password'));
        	$new_password = hash('sha256', $app->request()->params('new_password'));
        	global $db;
        	if(!empty($name))
        	{
        		$name = $name;
        	}
        	else
        	{
        		$name = $user_name;
        	}
        	if($new_password == '')
        	{
        		$new_password = $user_password;
        	}

        	if($old_password != $user_password)
        	{
        		$check['status'] = "failed";
	            $check['message'] = "Old Password Not Matched";
	            unset($check["data"]);
				echoResponse(200,$check);
        	}else
        	{	
				$url = '';
				if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
				{
					$image = md5(militime.$_FILES['image']['name']).'.jpg';
					move_uploaded_file($_FILES['image']['tmp_name'], '../../uploads/user_image/'.$image);
				}else
				{
					$image = $check['data'][0]['user_image'];
				}
				if($image != '')
				{
					$url = base_url.'uploads/user_image/'.$image;
				}
				if($mobile != $user_mobile)
	        	{
	        		$code='1234';
	        		$update_info =  $db->update('BlueTrade_User',array('User_name'=>$name,'Otp_code'=>md5($code),'temp_mobile_no'=>$mobile,'user_image'=>$image,'Password'=>$new_password,'Update_at'=>militime),array('User_id'=>$user_id),array());
	        	}
	        	else
	        	{
	        		$update_info =  $db->update('BlueTrade_User',array('User_name'=>$name,'user_image'=>$image,'Password'=>$new_password,'Update_at'=>militime),array('User_id'=>$user_id),array());
	        	}
				if($update_info['status'] == "success")
				{
					$update_info['status'] = "success";
		            $update_info['message'] = "successfully Updated";
		            $update_info["data"] = array('User_name'=>$name,'user_image'=>$url);
				}
				else
				{
					$update_info['status'] = "failed";
		            $update_info['message'] = "Data Not Updated...!";
		            unset($update_info["data"]);
				}
				echoResponse(200,$update_info);
			}
	    }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});


$app->post('/enquiry', function() use ($app)
{

  $headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
         $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$name = $app->request()->params('name');
        	$email = $app->request()->params('email');
        	$mobile = $app->request()->params('mobile');
        	//print_r($mobile); exit;
        	$message = $app->request()->params('message');
        	global $db;

	        $insert_enquiry = $db->insert('enquiry',array('user_id'=>$user_id,'name'=>$name,'email'=>$email,'mobile'=>$mobile,'message'=>$message),array());
	        if($insert_enquiry['status'] == 'success')
	        {
	        	$insert_enquiry['status'] = "success";
	            $insert_enquiry['message'] = "Successfully Inserted";
	            $insert_enquiry['data'] = array('user_id'=>$user_id,'name'=>$name,'email'=>$email,'mobile'=>$mobile,'message'=>$message); 
	        }
	        else
	        {
	        	$insert_enquiry['status'] = "failed";
	            $insert_enquiry['message'] = "Data Not Inserted..!";
	            unset($insert_enquiry['data']);
	        }
	            echoResponse(200,$insert_enquiry);

        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }

});

$app->post('/view_ads', function() use ($app)
{
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$category_id = $app->request()->params('category_id');
        	$sort = $app->request()->params('sort');
        	$is_asc = $app->request()->params('is_asc');
        	$lat = $app->request()->params('lat');
        	$lng = $app->request()->params('lng');
        	$distance = $app->request()->params('distance');
        	$page_no = $app->request()->params('page_no');
	        $per_set = 10;
	        $from  = ($page_no-1)*$per_set;
        	global $db;
        	if($category_id != 0)
        	{
        		$cat_id = "`category_id` = '$category_id'";
        	}else
        	{
        		$cat_id = "1=1";
        	}
        	if($sort == 'price')
        	{
        		$sort = "price";
        	}
        	elseif($sort == 'date')
        	{	
        		$sort = "creat_at";
        	}
        	if($is_asc == 0)
        	{
        		$is_asc1 = "ORDER BY ".$sort." ASC";
        	}
        	else
        	{
        		$is_asc1 ="ORDER BY ".$sort." DESC";
        	}
        	 //print_r($is_asc1); exit;
       		//echo "select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance from `Ads` HAVING distance <= '$distance' AND ".$cat_id." ".$is_asc1." limit $from,$per_set"; exit;
        	//$ads = $db->customQuery("select * from `Ads` where ".$cat_id." ".$is_asc1." limit $from,$per_set");
        	$ads = $db->customQuery("select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance from `Ads` HAVING distance <= '$distance' AND ".$cat_id." AND `ads_status` = '0'  ".$is_asc1."   limit $from,$per_set");
	        if($ads['status'] == 'success')
	        {
	        	foreach($ads['data'] as $key_acc)
	            {
	            	$table_id = $key_acc['table_id'];
	            	$img_arr = array();
	            	$all_image =  $db->customQuery("select Ads_image.ads_image from `Ads_image` where `ad_id` = '$table_id'");
	            	if($all_image['status']=="success")
	            	{
		            	foreach($all_image['data'] as $img)
		            	{
		            		
		            		$img_arr[] = base_url.'uploads/ads_image/'.$img['ads_image']; 
		            	}
	            	}
		          	$key_acc['Ads_image'] = $img_arr;
	            	$feat_arr[] = $key_acc;
	            	//$finalarr[] =  array('info'=>$key_acc,'imageinfo'=>$img_arr);  
	            }
	        	$ads['status'] = "success";
	            $ads['message'] = "Successfully";
	            $ads['data'] = $feat_arr;
	        }
	        else
	        {
	        	$ads['status'] = "failed";
	            $ads['message'] = "Data Not Found..!";
	            unset($ads['data']);
	        }
	            echoResponse(200,$ads);

        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});

$app->post('/search_ads', function() use ($app)
{
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$search_key = $app->request()->params('search_key');
        	$s_key = strtok($search_key, " ");
        	$s = strlen($s_key);
        	//print_r($s); exit;
        	if($s >3 && !empty($s))
        	{
        		$ss = substr($s_key,0,-2);
        	}
        	else
        	{
        		$ss = $s_key;
        	}
        	//echo $ss = substr($s_key,0,-1); exit;
        	$lat = $app->request()->params('lat');
        	$lng = $app->request()->params('lng');
        	$distance = $app->request()->params('distance');
        	$page_no = $app->request()->params('page_no');
	        $per_set = 10;
	        $from  = ($page_no-1)*$per_set;
        	global $db;
        	//echo "select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance from `Ads` HAVING distance <= '$distance' AND  ( `ad_title` LIKE '%$search_key%' || `ad_description` LIKE '%$search_key%' ) limit $from,$per_set"; exit;
        	//$ads = $db->customQuery("select * from `Ads` where `ad_title` LIKE '%$search_key%' && `ad_description` LIKE '%$search_key%' limit $from,$per_set");
        	$ads = $db->customQuery("select Ads.*,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(lat) )* cos( radians(lng) - radians('$lng')) + sin(radians('$lat'))* sin( radians(lat))))) AS distance from `Ads` HAVING distance <= '$distance' AND  ( `ad_title` LIKE '%$ss%' || `ad_description` LIKE '%$ss%' ) limit $from,$per_set");
	        if($ads['status'] == 'success')
	        {
	        	foreach($ads['data'] as $key_acc)
	            {
	            	$table_id = $key_acc['table_id'];
	            	$img_arr = array();
	            	$all_image =  $db->customQuery("select Ads_image.ads_image from `Ads_image` where `ad_id` = '$table_id'");
	            	if($all_image['status']=="success")
	            	{
		            	foreach($all_image['data'] as $img)
		            	{
		            		$img_arr[] = base_url.'uploads/ads_image/'.$img['ads_image']; 
		            	}
	            	}
	            	$key_acc['Ads_image'] = $img_arr;
	            	$feat_arr[] = $key_acc;
	            }
	        	$ads['status'] = "success";
	            $ads['message'] = "Successfully";
	            $ads['data'] = $feat_arr;
	        }
	        else
	        {
	        	$ads['status'] = "failed";
	            $ads['message'] = "Data Not Found...!";
	            unset($ads['data']);
	        }
	            echoResponse(200,$ads);

        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});


$app->post('/my_ads', function() use ($app)
{
	$headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$category_id = $app->request()->params('category_id');
        	$sort = $app->request()->params('sort');
        	$is_asc = $app->request()->params('is_asc');
        	$status = $app->request()->params('ads_status');
        	$page_no = $app->request()->params('page_no');
	        $per_set = 10;
	        $from  = ($page_no-1)*$per_set;
        	global $db;
        	if($category_id != 0)
        	{
        		$cat_id = "`category_id` = '$category_id'";
        	}else
        	{
        		$cat_id = "1=1";
        	}
        	if($sort == 'price')
        	{
        		$sort = "price";
        	}
        	elseif($sort == 'date')
        	{	
        		$sort = "creat_at";
        	}
        	if($is_asc == 0)
        	{
        		$is_asc1 = "ORDER BY ".$sort." ASC";
        	}
        	else
        	{
        		$is_asc1 ="ORDER BY ".$sort." DESC";
        	}

        	$ads = $db->customQuery("select * from `Ads` where `ads_status` = '$status' AND ".$cat_id." AND `User_id` = $user_id  ".$is_asc1." limit $from,$per_set");
        	
	        if($ads['status'] == 'success')
	        {
	        	foreach($ads['data'] as $key_acc)
	            {
	            	$table_id = $key_acc['table_id'];
	            	$img_arr = array();
	            	$all_image =  $db->customQuery("select Ads_image.ads_image from `Ads_image` where `ad_id`='$table_id'");
	            	if($all_image['status']=="success")
	            	{
		            	foreach($all_image['data'] as $img)
		            	{
		            		$img_arr[] = base_url.'uploads/ads_image/'.$img['ads_image'] ; 
		            	}
	            	}
	            	$key_acc['Ads_image'] = $img_arr;
	            	$feat_arr[] = $key_acc;
	            }
	        	$ads['status'] = "success";
	            $ads['message'] = "Successfully";
	            $ads['data'] = $feat_arr;
	        }
	        else
	        {
	        	$ads['status'] = "failed";
	            $ads['message'] = "No Data Found...!";
	            unset($ads['data']);
	        }
	            echoResponse(200,$ads);
        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }
});


$app->post('/New_mobile_verification', function() use ($app){
    $headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
		    $user_id = $check['data'][0]['User_id'];
		    $json1 = file_get_contents('php://input');
			if(!empty($json1))
			{
				$data = json_decode($json1);
			    if($data->verify_code != '' && strlen($data->verify_code) == 4)
			    {
			    	global $db;
				    $rows = $db->select("BlueTrade_User","*",array('User_id'=>$user_id,'temp_mobile_no'=>$data->mobile));
				    if($rows["status"]=="success")
				    {
				      	if($rows["data"][0]['Otp_code'] == md5($data->verify_code))
				      	{
				      		$update = $db->update("BlueTrade_User",array('Otp_code'=>'','Contact_num'=>$data->mobile,'Update_at'=>militime),array('User_id'=>$user_id),array());
				      		if($update['status']=="success")
				      		{
				      			$update['status']="success";
					            $update["message"] = "Mobile Number Successfully Verified";
								unset($update['data']);
					            echoResponse(200, $update);
				      		}else
				      		{
				      			$update['status']="failed";
					            $update["message"] = "Something went wrong! Please try again later";
								unset($update['data']);
					            echoResponse(200, $update);
				      		}
				      	}else
				      	{
				      		$rows['status']="failed";
				            $rows["message"] = "Otp not matched.";
							unset($rows['data']);
				            echoResponse(200, $rows);
				      	}     
				    }else
				    {
				       $rows["status"] = 'failed';
				       $rows["message"]= "Mobile number not matched";
				       unset($rows["data"]);
				       echoResponse(200, $rows);
				    }
			    }else
			    {
        			$check_otp['status'] = "failed";
			    	$check_otp['message']= "Invalid Request parameter";
					echoResponse(200,$check_otp);
			    }
			}else
			{
        		$check_otp['status'] = "failed";
				$check_otp['message'] ="No Request parameter";
				echoResponse(200,$check_otp);
			}
		}else
		{
			$check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
	}
});


$app->post('/help', function() use ($app)
{

  $headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
         $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$name = $app->request()->params('name');
        	$email = $app->request()->params('email');
        	$ad_title = $app->request()->params('ad_title');
        	$mobile = $app->request()->params('mobile');
        	$message = $app->request()->params('message');
        	if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
			{
				$image = md5(militime.$_FILES['image']['name']).'.jpg';
				move_uploaded_file($_FILES['image']['tmp_name'], '../../uploads/ads_image/'.$image);
			}else
			{
				$image = "";
			}
			//print_r($image); exit;
        	global $db;

	        $insert_help = $db->insert('help',array('user_id'=>$user_id,'user_name'=>$name,'user_email'=>$email,'user_mobile'=>$mobile,'message'=>$message,'ad_title'=>$ad_title,'image'=>$image),array());
	        if($insert_help['status'] == 'success')
	        {
	        	$insert_help['status'] = "success";
	            $insert_help['message'] = "Successfully Inserted";
	            $insert_help['data'] = array('user_id'=>$user_id,'name'=>$name,'email'=>$email,'ad_title'=>$ad_title,'mobile'=>$mobile,'message'=>$message,'image'=>$image); 
	        }
	        else
	        {
	        	$insert_help['status'] = "failed";
	            $insert_help['message'] = "Data Not Inserted..!";
	            unset($insert_help['data']);
	        }
	            echoResponse(200,$insert_help);
        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }

});


$app->post('/deactivate_ads', function() use ($app)
{
  $headers = apache_request_headers();
    if(!empty($headers['secret_key']))
    {
        $check = token_auth($headers['secret_key']);
        if($check['status'] == 'true')
        {
        	$user_id = $check['data'][0]['User_id'];
        	$id = $app->request()->params('id');
        	global $db;
	        $deactivate = $db->update('Ads',array('ads_status' =>'1'),array('table_id' => $id ),array());
	        if($deactivate['status'] == 'success')
	        {
	        	$deactivate['status'] = "success";
	            $deactivate['message'] = "Successfully Deactivate";
	            //$deactivate['data'] = array('user_id'=>$user_id,'name'=>$name,'email'=>$email,'ad_title'=>$ad_title,'mobile'=>$mobile,'message'=>$message,'image'=>$image); 
	        }
	        else
	        {
	        	$deactivate['status'] = "failed";
	            $deactivate['message'] = "Not Deactivate..!";
	            unset($deactivate['data']);
	        }
	            echoResponse(200,$deactivate);
        }
        else
        {
            $check['status'] = "false";
            $check['message'] = "Invalid Token";
            unset($check['data']);
            echoResponse(200,$check);
        }
	}
	else
    {
        $check['status'] = "false";
        $check['message'] = "Unauthorised access";
        unset($check['data']);
        echoResponse(200,$check);
    }

});

//End Update_mobile_verification api

/*$app->post('/description', function() use ($app){

	$id =  $app->request()->params('id');
	global $db;

	$description = $db->customQuery("SELECT Ads.*,Ads_image.ads_image FROM `Ads` LEFT JOIN `Ads_image` ON `Ads_image`.`ad_id`=`Ads`.`table_id` where `table_id` = $id ");
	//$description = $db->customQuery("SELECT Ads.* FROM `Ads` where `table_id` = $id ");
	if($description['status'] == 'success')
	{
		  $description['status'] = "success";
	      $description['message'] = "success";
	      $description["data"] = $description["data"];
		//$image = $db->customQuery("SELECT  Ads_image.ads_image,Ads_image.image_table_id  FROM `Ads_image` where `ad_id` = $id");
		
	}
	else
	{
		$description['status'] = "failed";
	    $description['message'] = "failed";  
	    unset($description["data"]);
	    
	}
echoResponse(200,$description);

});
*/


/*$app->post('/ads_by_location', function() use ($app){

 	   $lat = $app->request()->params('lat');
 	   $lng = $app->request()->params('lng');
 	   global $db;


});*/

/*
$app->post('/ChangePassword', function() use ($app) { 
    
  $headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status']=="true")
		{
	        $user_id=$check['data'][0]['user_id'];
	        global $db;
		    $current_password = md5($app->request()->params('current_password'));
		    $new_password = $app->request()->params('new_password');
         	$condition = array('user_id'=>$user_id,'user_type'=>0);
          
          	$rows1 = $db->select("user","*",$condition);
            if($rows1['status']=="success")
            {
            	$password = $rows1['data'][0]['password']; 
				
				if($current_password == $password)
				{
					$newpassword = md5($new_password);
					$data = array(
					        'password'=>$newpassword,
					        'update_at'=>militime
					      );

					$rows = $db->update("user",$data,$condition,array());
					if($rows['status']='success')
					{
					    $rows["status"] = "success";
					    $rows["message"] = "Your password has been successfully changed.";
					    unset($rows['data']);
					}else
					{
					    $rows["status"] = "failed";
					    $rows["message"] = "failed";
					    unset($rows['data']);
					}
					echoResponse(200, $rows);
				}else
				{
					$rows1["status"] = "failed";
					$rows1["message"] = "The old password you entered was incorrect.";
					unset($rows1['data']);
					echoResponse(200, $rows1);
				}    
  			}else
  			{
  				$rows1["status"] = "failed";
        		$rows1["message"] = "This feature is not applicable for you";
        		unset($rows1['data']);
				echoResponse(200, $rows1);
  			}
  		}
		else
		{
			$msg['message'] = "Invalid Token";
			echoResponse(200,$msg);
		}
	}
	else
	{
		$msg['message'] = "Unauthorised access";
		echoResponse(200,$msg);
	}
});

$app->get('/GLC_category',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			global $db;
			$category = $db->select("GLC_category","category_id,category_name",array('del_status'=>1));
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					
					$arr[] = $key;
				}
				$query_login['status'] = "success";
				$query_login['message'] = "Successfull";
				$query_login['data'] = $arr;
				echoResponse(200,$query_login);
			}else
			{
				unset($query_login['data']);
				$query_login['status'] = "failed";
				$query_login['message'] = "Category Not Found";
				echoResponse(200,$query_login);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/GLC_Subcategory',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$categoryid = $app->request->params('categoryid');
			global $db;
			$category = $db->select("GLC_subcategory","subcate_id,sub_cate_name",array('category_id'=>$categoryid,'del_status'=>1));
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					$arr[] = $key;
				}
				$query_login['status'] = "success";
				$query_login['message'] = "Successfull";
				$query_login['data'] = $arr;
				echoResponse(200,$query_login);
			}else
			{
				unset($query_login['data']);
				$query_login['status'] = "failed";
				$query_login['message'] = "Sub Category Not Found";
				echoResponse(200,$query_login);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});
	
$app->get('/LatestGLC_List',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$sub_category_id = $app->request->params('sub_category_id');
			$create_at = $app->request->params('create_at');
			global $db;
			$orderby = "ORDER BY news_id DESC";
			$create = '';
			if($create_at != 0)
			{
				$create = "AND created_at < '$create_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$category = $db->customQuery("SELECT * FROM News_detail WHERE sub_cate_id = '$sub_category_id' AND del_status = 1 ".$create." ORDER BY news_id DESC LIMIT 10");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					$seconds = $key['created_at'] / 1000;
					$date = date("d-m-Y", $seconds);
					$key['news_date'] = $date;
					$image = base_url.'uploads/news_image/'.$key['image'];
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Latest@GLC List Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/LatestGLC_List_PulltoRefresh',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$sub_category_id = $app->request->params('sub_category_id');
			$create_at = $app->request->params('create_at');
			global $db;
			$orderby = "ORDER BY news_id DESC";
			$create = '';
			if($create_at != 0)
			{
				$create = "AND created_at > '$create_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$category = $db->customQuery("SELECT * FROM News_detail WHERE sub_cate_id = '$sub_category_id' AND del_status = 1 ".$create." ORDER BY news_id DESC LIMIT 10");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					$seconds = $key['created_at'] / 1000;
					$date = date("d-m-Y", $seconds);
					$key['news_date'] = $date;
					$image = base_url.'uploads/news_image/'.$key['image'];
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "No Latest@GLC List Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/Event_List',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$create_at = $app->request->params('create_at');
			global $db;
			$create = '';
			if($create_at != 0)
			{
				$create = "AND event_militime < '$create_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$category = $db->customQuery("SELECT * FROM GLC_event WHERE event_status = 1 ".$create." ORDER BY event_militime ASC LIMIT 2");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					
					$image = base_url.'uploads/event_image/'.$key['event_image'];
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "Event Not Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});

$app->get('/Event_List_PulltoRefresh',function() use ($app){
	$headers = apache_request_headers();
	if(!empty($headers['secret_key']))
	{
		$check = token_auth($headers['secret_key']);
		if($check['status'] == 'true')
		{	
			$create_at = $app->request->params('create_at');
			global $db;
			$create = '';
			if($create_at != 0)
			{
				$create = "AND event_militime > '$create_at'";
			}
			//$category = $db->select2("News_detail","*",array('sub_cate_id'=>$sub_category_id,'del_status'=>1),$orderby);
			$category = $db->customQuery("SELECT * FROM GLC_event WHERE event_status = 1 ".$create." ORDER BY event_militime ASC LIMIT 10");
			if($category['status']=="success")
			{
				foreach ($category['data'] as $key){
					
					$image = base_url.'uploads/event_image/'.$key['event_image'];
					$key['image'] = $image; 
					$arr[] = $key;
				}
				$category['status'] = "success";
				$category['message'] = "Successfull";
				$category['data'] = $arr;
				echoResponse(200,$category);
			}else
			{
				unset($category['data']);
				$category['status'] = "failed";
				$category['message'] = "Event Not Found";
				echoResponse(200,$category);
			}
		}else
		{
			$check['status'] = "false";
			$check['message'] = "Invalid Token";
			echoResponse(200,$check);
		}
	}else
	{
		$check['status'] = "false";
		$check['message'] = "Unauthorised access";
		echoResponse(200,$check);
	}
});*/


$app->post('/sendmsg', function() use ($app) {
global $db;
	$token = $db->select("select","Device_token",array('Email_id'=>"harsh.parashar@ebabu.co"));
	iOSPushNotification($token['data'][0]['Device_token'],"testing",'This is testing notification');

//    sendsms('9755116566','hii');
});

/*function sendsms($mobile_no,$msg)
{
  $mobilenumbers = $mobile_no; //enter Mobile numbers comma seperated
  $message = $msg;
  //$contrycode = $code;
  $senderid="bluetrade"; //Your senderid
  $authKey = "128888A4N4rOlh5808a257";  
  $route = "4";
  $url="https://control.msg91.com/api/sendhttp.php?";
    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS =>"authkey=$authKey&mobiles=$mobilenumbers&message=$message&sender=$senderid&route=$route"
        //,CURLOPT_FOLLOWLOCATION => true
    ));
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $output = curl_exec($ch);
    if(curl_errno($ch))
    {
        echo 'error:' . curl_error($ch);
    }
    curl_close($ch);
  // echo $output;exit;
}*/

$app->post('/darshancategory',function() use ($app){
	
	$json1 = file_get_contents('php://input');
	if(!empty($json1)){
		 $data = json_decode($json1);
global $db;
		 $parent_id= $data->parent_id;
		 	$condition2 = array('parent_id'=>$parent_id);
			$query_login = $db->select("category","*",$condition2);
			if($query_login["status"] == "success")
			{	$finalarr =array();
				foreach($query_login['data'] as $key_acc)
	            {
	            	$new = array(
	            			'category_id' => $key_acc['category_id'],
	            			'category_name' => $key_acc['category_name'],
	            			'parent_id' => $key_acc['parent_id'],

	            		);
	            	$finalarr[] = $new;
	            }	
	            $check_otp['status'] = "failed";
				$check_otp['message'] ="No data found";
				$check_otp['data'] = $finalarr;
				echoResponse(200,$check_otp);
			}else{
				$check_otp['status'] = "failed";
				$check_otp['message'] ="No data found";
				echoResponse(200,$check_otp);
			}
	}else{	
		$check_otp['message'] ="No Request parameter";
		echoResponse(200,$check_otp);
	}
});

function sendsms($mobileNumber,$message1)
    {
        $authKey = "128888A4N4rOlh5808a257";
        //$mobileNumber = "9999999";
        $senderId = "btrade";
        $message = urlencode($message1);
        //Define route 
        $route = "4";
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        $url="https://control.msg91.com/api/sendhttp.php?";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    
        //get response
        $output = curl_exec($ch);
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
       // echo $output;exit;
    }

function distance($lat1, $lon1, $lat2, $lon2, $unit)
{
	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);
	if ($unit == "K") {
	return ($miles * 1.609344);
	} else if ($unit == "N") {
	  return ($miles * 0.8684);
	} else {
	    return $miles;
	}
}
function echoResponse($status_code, $response) {
    global $app;
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response,JSON_NUMERIC_CHECK);
}


$app->run();
?>
