<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Woodbazaar | Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url()?>template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>theme/css/parsley.css" rel="stylesheet" type="text/css" media="all" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('designer/header'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
           <?php $this->load->view('designer/sidebar'); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Product
                            </h1>
                        </div>

                         <?php if(!empty($this->session->flashdata('error'))){echo "<span style='color:red;margin-left: 15%;'>".$this->session->flashdata('error')."</span>"; }?>
                        <!-- END PAGE TITLE -->

                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                      <!--   <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Form Stuff</span>
                        </li> -->
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">

                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Product </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form id="product_form" action="<?php echo base_url().'designer/product/edit/'.$product_data[0]->product_id; ?>" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate="">
                <div class="form-body">

                    <?php //print_r($product_data);exit;?>
                    <div class="form-group">
                        <label class="control-label col-md-3">Product Name</label>
                        <div class="col-md-9">
                            <input type="text" placeholder="Product Name" name="p_name" class="form-control" value="<?php if($product_data[0]->product_name)echo $product_data[0]->product_name; ?>" readonly/>
                            <input type="hidden"  name="product_id" value="<?php if($product_data[0]->product_id)echo $product_data[0]->product_id; ?>" />
                            <!-- <span class="help-block"> This is inline help </span> -->
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label class="control-label col-md-3">Category</label>
                        <div class="col-md-9">
                            <select  class="form-control" name="category" readonly>
                            <option value="">Select Category</option>
                            <?php if(!empty($category_data)){
                                 foreach($category_data as $category)
                                 {?>
                                  <option value="<?php echo $category->category_id;  ?>" <?php if($category->category_id==$product_data[0]->category_id) {echo "SELECTED";}?>><?php echo $category->category_name; ?></option>  

                                <?php  }  } ?>
                              
                           </select>
                        </div>
                    </div>  
                   <!--  <div class="form-group">
                        <label class="control-label col-md-3">Product Image</label>
                        <div class="col-md-9">
                            <input type="file" name="product_img" placeholder="Image" class="form-control" />
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Base Price</label>
                        <div class="col-md-9">
                            <input type="text" name="price_base" placeholder="Base Price" class="form-control"  value="<?php if($product_data[0]->price_base)echo $product_data[0]->price_base; ?>" readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Sale Price</label>
                        <div class="col-md-9">
                            <input type="text" name="price" placeholder="Price" class="form-control"  value="<?php if($product_data[0]->price)echo $product_data[0]->price; ?>" readonly/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">Select Discount Type</label>
                        <div class="col-md-9">
                            <select  class="form-control" name="discount_type" required>
                            <option value="">Select Discount type</option>
                            <option value="0" <?php if($product_data[0]->discount_type=='0')echo "SELECTED"; ?>>Percentage</option>
                            <option value="1" <?php if($product_data[0]->discount_type=='1')echo "SELECTED"; ?>>Direct In Amount</option>
                           </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">Discount(%)</label>
                        <div class="col-md-9">
                            <input type="text" name="discount" placeholder="discount" class="form-control"  value="<?php if($product_data[0]->discount)echo $product_data[0]->discount; ?>" required/>
                        </div>
                    </div>
                      <div class="form-group">
                        <label class="control-label col-md-3">Coupon Code</label>
                        <div class="col-md-9">
                            <input type="text" name="coupon_code" class="form-control"  value="<?php if($product_data[0]->coupon_code)echo $product_data[0]->coupon_code; ?>" readonly/>
                        </div>
                    </div>


                     <div class="form-group">
                        <label class="control-label col-md-3">Product Id</label>
                        <div class="col-md-9">
                            <input type="text" name="product_code" placeholder="Product ID" class="form-control"  value="<?php if($product_data[0]->product_code)echo $product_data[0]->product_code; ?>" readonly/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Material</label>
                        <div class="col-md-9">
                            <input type="text" name="material" placeholder="Material" class="form-control"  value="<?php if($product_data[0]->material)echo $product_data[0]->material; ?>" readonly/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Brand</label>
                        <div class="col-md-9">
                            <input type="text" name="brand" placeholder="brand" class="form-control"  value="<?php if($product_data[0]->brand)echo $product_data[0]->brand; ?>" readonly/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Color</label>
                        <div class="col-md-9">
                            <input type="text" name="color" placeholder="color" class="form-control"  value="<?php if($product_data[0]->color)echo $product_data[0]->color; ?>" readonly/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Style</label>
                        <div class="col-md-9">
                            <input type="text" name="style" placeholder="Style" class="form-control"  value="<?php if($product_data[0]->style)echo $product_data[0]->style; ?>" readonly/>
                        </div>
                    </div>
                
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" value="Submit">
                                
                            <a  href="<?php echo base_url('designer/product/show_product')?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
     <?php $this->load->view('designer/footer'); ?>
        <!-- END FOOTER -->
       
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/ckeditor/ckeditor.js"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
               <script src="<?php echo base_url(); ?>theme/js/parsley.min.js"> </script>

<script type="text/javascript">
  $('#product_form').parsley();
</script>
  
 <script type="text/javascript">
 function getsubcategory(category_id)
 { 
   var str = "category_id="+category_id;
  
   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>designer/product/get_category/",
        data:str,
        //beforeSend:function(){ alert(this.url);},
        success:function(data)
        {   $('#subcat_id').empty();
            $('#subcat_id').append(data);
        }
     });
     $('#subcat_id option').attr('selected', false);
 }             
   </script>
        
    </body>

</html>