 <div class="page-sidebar-wrapper">
                
                <div class="page-sidebar navbar-collapse collapse">
                   
                    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start active open">
                            <a href="<?php echo base_url().'designer/dashboard'?>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                                <!-- <span class="arrow open"></span> -->
                            </a>
                           
                        </li>
                     
                        <li class="nav-item  ">
                            <a href="<?php echo base_url('designer/product/show_product');?>" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">Product</span>
                                
                            </a>
                            
                        </li>

                          <li class="nav-item  ">
                            <a href="<?php echo base_url('designer/order');?>" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">Orders</span>
                                
                            </a>
                            
                        </li>
                        <li class="nav-item  ">
                            <a href="<?php echo base_url('designer/brand');?>" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">Brand Discount</span>
                                
                            </a>
                            
                        </li>
                        <li class="nav-item  ">
                            <a href="<?php echo base_url('designer/Redeem');?>" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">Redeem</span>
                            </a>
                        </li>
                      
                     
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>