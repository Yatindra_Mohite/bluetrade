<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset="utf-8" />
        <title>Add Quiz</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>template/assets/global/css/parsley.css" rel="stylesheet"><!-- Parsley -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
        <link rel="shortcut icon" href="favicon.ico" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>template/assets/global/css/bootstrap-clockpicker.min.css">

         </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/header'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
           <?php $this->load->view('admin/sidebar'); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Quiz
                            </h1>
                        </div>
                    </div>
                   
                    <ul class="page-breadcrumb breadcrumb">
                     
                    </ul>
                   
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                               
                <div class="">
                    <div class="tab-pane" id="tab_4">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                <i class=""></i><small>ADD QUIZ</small>
                                </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="#portlet-config" data-toggle="modal" class=""> </a>
                                        <a href="javascript:;" class=""> </a>
                                        <a href="javascript:;" class=""> </a>
                                    </div>
                                </div>
            
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <!-- action="<?php// echo base_url().'admin/product/add_product'?>" -->
           <?php
            if ($this->session->flashdata('success')) { 
                echo "<div class='alert alert-success'>", $this->session->flashdata('success') ,"</div>";
            }else if($this->session->flashdata('failed')){
                echo "<div class='alert alert-danger'>", $this->session->flashdata('failed') ,"</div>";
            }
           ?> 
            <form  class="form-horizontal form-row-seperated" action="<?php echo base_url().'admin/quiz/add_quiz'?>" method="post" enctype="multipart/form-data" id="form11" data-parsley-validate=''>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Title</label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Title" parsley-required="true" data-parsley-required-message="Title field is required" name="title" class="form-control" required/>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-7" >
                                <!-- <div name="summernote" id="summernote_1"></div> -->
                            <textarea rows="12"; cols="67" class="ckeditor" name="description" ></textarea>
                            </div>
                        </div>
                    </div>
                   <!--  <div class="form-group">
                        <label class="control-label col-md-3">Total Duration</label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            
                            <div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" name="total_duration" class="form-control" value="00:00" readonly >
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                                           
                        </div>
                    </div> -->
                     <div class="form-group">
                        <label class="control-label col-md-3">Start Date</label>
                        <div class="col-md-7">
                            <input type="text" name="start_date" id="datepicker" parsley-required="true" placeholder="Select Date" class="form-control" required/>
                           <!--  <?php
                            $message //= $this->session->flashdata('datefailed');
                             //if ($this->session->flashdata('datefailed')) { 
                         //echo "<div style='color:red;'>", $message['message'] ,"</div>";}?> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">End Date</label>
                        <div class="col-md-7">
                            <input type="text" id="datepicker1" name="end_date" class="form-control" placeholder="Select Date" required/>
                        </div>
                    </div>
                     
                    <div class="form-group" style="padding-left:25%">
                        <div class="col-md-2">
                            <input type="submit" name="submit" value="submit" class="form-control btn blue btn-block" />
                        </div>
                        <!-- <div class="col-md-2">
                            <a href="<?php //echo base_url().'admin/quiz/add_quiz'?>"><input type="button" name="cancel" value="Cancel" class="form-control btn blue btn-block" /></a>
                        </div> -->
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
           </div>
                </div>
            </div>
        </div>
<!-- END QUICK SIDEBAR -->
    </div>
    </div>    
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
     <?php $this->load->view('admin/footer'); ?>
        <!-- END FOOTER -->
       
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/js/parsley.min.js"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
       
        <script src="<?php echo base_url()?>template/assets/global/scripts/app.min.js" type="text/javascript"></script>
       
        <script src="<?php echo base_url()?>template/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/ckeditor/ckeditor.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
       
<script type="text/javascript">
 function getsubcategory(category_id)
 { 
   var str = "category_id="+category_id;
  
   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>admin/product/get_category/",
        data:str,
        //beforeSend:function(){ alert(this.url);},
        success:function(data)
        {   $('#subcat_id').empty();
            $('#subcat_id').append(data);
        }
     });
     $('#subcat_id option').attr('selected', false);
 }             
</script>
<script type="text/javascript">
  $('#form11').parsley();
</script>
<script>
$( function() {
  $( "#datepicker" ).datepicker({
  minDate: new Date(),
  dateFormat: 'dd-mm-yy'
   /* changeMonth: true,
    changeYear: true*/
  });
} );
</script>    
<script>
$( function() {
  $( "#datepicker1" ).datepicker({
  minDate: new Date(),
  dateFormat: 'dd-mm-yy'
   /* changeMonth: true,
    changeYear: true*/
  });
} );
</script>     
 <script type="text/javascript" src="<?php echo base_url(); ?>template/assets/global/js/bootstrap-clockpicker.min.js"></script>

       <script type="text/javascript">
        $('.clockpicker').clockpicker()
            .find('input').change(function(){
        });
      </script> 
    </body>

</html>