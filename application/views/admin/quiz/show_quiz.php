<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Show Quiz</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url()?>template/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url()?>template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
    <?php $this->load->view('admin/header'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
          <?php $this->load->view('admin/sidebar'); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Show Quiz
                               <!--  <small>rowreorder extension demos</small> -->
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url('admin/user');?>">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                    </ul>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"><small>Show Quiz</small></span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <?php if($this->session->flashdata('success'))
                                        {
                                            echo "<div class='alert alert-success'>", $this->session->flashdata('success'), "</div>";
                                        }else if($this->session->flashdata('failed'))
                                        {
                                            echo "<div class='alert alert-danger'>", $this->session->flashdata('failed'), "</div>";
                                        }
                                ?>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> S.No. </th>
                                                <th> Title </th>
                                                <th> Description </th>
                                                <th> start Date</th>
                                                <th> End date</th>
                                                <th> Total Duration </th>
                                                <th> Participant</th>
                                                <th> Publish Quiz</th>
                                                <th> Question/Answer</th>
                                                <th> Publish Result</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                          $i = 1;
                                           if(!empty($quiz_data))
                                            {
                                            foreach($quiz_data as $quizkey)   
                                            {
                                                if($quizkey->is_publish == 0){ $name = 'Unpublish'; $class = 'btn btn-danger btn-xs';}else{ $name = 'Publish'; $class = 'btn btn-primary btn-xs';}
                                                if($quizkey->publish_result == 0){ $name1 = 'Unpublish'; $class1 = 'btn btn-danger btn-xs';}else{ $name1 = 'Publish'; $class1 = 'btn btn-primary btn-xs';}
                                                $small = substr(strip_tags($quizkey->quiz_description), 0, 100);
                                                $curentdt = date('d-m-Y');
                                                $curmili = strtotime($curentdt)*1000;
                                                
                                                $query = $this->db->query("SELECT count(quiz_question_id) as qcount FROM `GLC_Quiz_Question` WHERE quiz_id = '".$quizkey->quiz_id."'");
                                                $data = $query->result();
                                                $countt = $data[0]->qcount;
                                                $qtime = $countt * 20;
                                                if($qtime < 60) { $quiztime = gmdate("s", $qtime); $m = 'Second';}
                                                elseif ($qtime >= 60 && $qtime < 3600) { $quiztime = gmdate("i:s", $qtime); $m = 'Min';}
                                                else { $quiztime = gmdate("H:i:s", $qtime); $m = 'Hours';}
                                          ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $quizkey->quiz_title;  ?> </td>
                                                <td><?php echo $small.'....'; ?> </td>
                                                <td><?php echo $quizkey->quiz_start_date; ?></td>
                                                <td><?php echo $quizkey->quiz_end_date; ?></td>
                                                <td><?php echo $quiztime.' '.$m; ?></td>
                                                <td><a href="<?php echo base_url('admin/quiz/quiz_user/'.$quizkey->quiz_id)?>" data-toggle="tooltip" title="View User"><button class="btn btn-primary btn-xs">Quiz User</button></a></td>
                                                <td><button onclick="publish(<?php echo $quizkey->quiz_id.','.$quizkey->is_publish; ?>,3)" data-toggle="tooltip" title="Publish/Unpublish" class="<?php echo $class;?>"><?php echo $name;?></button></td>
                                                <td style="width:5px"><a href="<?php echo base_url('admin/quiz/add_question/'.$quizkey->quiz_id)?>" data-toggle="tooltip" title="delete"><button class="btn btn-primary btn-xs">Add</button></a>
                                                <a href="<?php echo base_url('admin/quiz/Show_Questions/'.$quizkey->quiz_id)?>" data-toggle="tooltip" title="View"><button class="btn btn-primary btn-xs">View</button></a>    
                                                </td>
                                                <td><button onclick="publish(<?php echo $quizkey->quiz_id.','.$quizkey->publish_result; ?>,4)" data-toggle="tooltip" title="Publish/Unpublish" class="<?php echo $class1;?>" <?php if($quizkey->end_dt_milisecond > $curmili || $quizkey->is_publish == 0){ echo 'disabled'; }?>><?php echo $name1;?></button></td>
                                            </tr>
                                            <?php $i++; 
                                            } }
                                             else{?>
                                            <tr class="even pointer">
                                                <td class="" >Record not found</td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class=""></td>
                                                <td class=""></td>
                                            </tr>
                                             <?php } ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box red">
                              
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer');?>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url()?>template/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/pages/scripts/table-datatables-rowreorder.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
       
    <script type="text/javascript">
        function publish(id,status,uniqid)
        { 
            if(status == 0){
            var r = confirm('Are you really want to Publish?');
            var msg = "Quiz Successfully Published";
            var msg1 = "Quiz Result Successfully Published";
            }else{
            var r = confirm('Are you really want to Unpublish?');
            var msg = "Quiz Successfully Unpublished";
            var msg1 = "Quiz Result Successfully Unpublished";
            }
            if(r==true)
            { 
                $.ajax({
                   url:"<?php echo base_url('admin/quiz/publish_quiz/')?>",
                   type:"POST",
                   data:"status="+status+"&id="+id+"&uniqid="+uniqid,
                    //beforeSend:function(){ alert(this.data);},
                   success:function(data)
                   {   
                      
                       if(data==1000)
                       {
                            if(uniqid == 3)
                            {
                                alert(msg);
                            }else
                            {
                                alert(msg1);
                            }
                            location.reload();
                       }else if(data ==1001)
                       {
                            alert("Quiz Publish date should not be less than or grater than quiz start date.");
                       }else if(data == 1002)
                       {
                            alert("Result Publish date should not be less than quiz end date.");
                       }
                   }
                });
            }
        }
    </script>   

    </body>

</html>