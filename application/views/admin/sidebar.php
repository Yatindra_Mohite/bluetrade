<div class="page-sidebar-wrapper">
                
     <div class="page-sidebar navbar-collapse collapse">
                   
            <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item">
                            <a href="<?php echo base_url().'admin/user'?>" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">USER</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">CATEGORY</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <!-- <li class="nav-item">
                                    <a href="<?php //echo base_url('admin/category/add_category');?>" class="nav-link ">
                                        <span class="title">Add Category</span>
                                    </a>
                                </li> -->
                                <li class="nav-item  ">
                                    <a href="<?php echo base_url('admin/category/');?>" class="nav-link ">
                                        <span class="title">Show Category</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">What's GLC</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/whats_GLC/edit_vision/1');?>" class="nav-link ">
                                        <span class="title">Vision & Mission</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/whats_GLC/edit_heritage/2');?>" class="nav-link ">
                                        <span class="title">Heritage</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="#" class="nav-link ">
                                        <span class="title">Embedded Campus</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item">
                                            <a href="<?php echo base_url('admin/whats_GLC/add_embedded_campus');?>" class="nav-link ">
                                               <span class="title"> - Add Embedded Campus</span>
                                            </a>
                                        </li>
                                         <li class="nav-item">
                                            <a href="<?php echo base_url('admin/whats_GLC/show_embedded_campus');?>" class="nav-link ">
                                               <span class="title"> - Show Embedded Campus</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                 <li class="nav-item ">
                                    <a href="<?php echo base_url('admin/whats_GLC/show_study_with_us');?>" class="nav-link ">
                                        <span class="title">Study With Us</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="<?php echo base_url('admin/whats_GLC/show_student_life');?>" class="nav-link ">
                                        <span class="title">Student Life</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">LATEST@GLC</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/news/Add_News');?>" class="nav-link ">
                                        <span class="title">Add Latest@Glc</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="<?php echo base_url('admin/news/');?>" class="nav-link ">
                                        <span class="title">Show Latest@Glc</span>
                                    </a>
                                </li>
                             
                            </ul>
                        </li>  
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">EVENT</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/event/Add_Event');?>" class="nav-link ">
                                        <span class="title">Add Event</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="<?php echo base_url('admin/event/');?>" class="nav-link ">
                                        <span class="title">Show Event</span>
                                    </a>
                                </li>
                             
                            </ul>
                        </li> 
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">ILLUMINATI</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/illuminati/Add_Illuminati');?>" class="nav-link ">
                                        <span class="title">Add Illuminati</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="<?php echo base_url('admin/illuminati/');?>" class="nav-link ">
                                        <span class="title">Show Illuminati</span>
                                    </a>
                                </li>
                             
                            </ul>
                        </li> 
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">QUIZ</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/quiz/add_quiz');?>" class="nav-link ">
                                        <span class="title">Add Quiz</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="<?php echo base_url('admin/quiz');?>" class="nav-link ">
                                        <span class="title">Show Quiz</span>
                                    </a>
                                </li>
                                <!--  <li class="nav-item ">
                                    <a href="<?php //echo base_url('admin/quiz/quiz_user');?>" class="nav-link ">
                                        <span class="title">Quiz User</span>
                                    </a>
                                </li> -->
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('admin/admission/');?>" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">ADMISSION</span>
                                <span class="arrow"></span>
                            </a>
                        </li>                           
                    </ul>
                    
                    <!-- END SIDEBAR MENU -->
    </div>
                <!-- END SIDEBAR -->
</div>