<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Event</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url()?>template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/header'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
           <?php $this->load->view('admin/sidebar'); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Event
                            </h1>
                        </div>
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                      <!--   <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Form Stuff</span>
                        </li> -->
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                               
                <div class="">
                    <div class="tab-pane" id="tab_4">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                <i class=""></i><small>Edit Event</small> 
                                </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                        <!-- <a href="javascript:;" class="remove"> </a> -->
                                    </div>
                                </div>
            
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <!-- action="<?php// echo base_url().'admin/product/add_product'?>" -->
           <?php if ($this->session->flashdata('error_pic')) { 
            echo "<div class='alert alert-danger'>", $this->session->flashdata('error_pic') ,"</div>";
          }else if($this->session->flashdata('error_video')){
            echo "<div class='alert alert-danger'>", $this->session->flashdata('error_video') ,"</div>";
          }
          if ($this->session->flashdata('success')) { 
            echo "<div class='alert alert-success'>", $this->session->flashdata('success') ,"</div>";
          }else if($this->session->flashdata('failed')){
            echo "<div class='alert alert-danger'>", $this->session->flashdata('failed') ,"</div>";
          } 
            $eventid = $this->uri->segment(4);
            $newss = $this->common_model->getData('GLC_event',array('event_id'=>$eventid));
          ?>
            <form  class="form-horizontal form-row-seperated" action="<?php echo base_url().'admin/event/edit_event/'.$eventid;?>" method="post" enctype="multipart/form-data" >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Event Title</label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Title" name="title" value="<?php echo $newss[0]->event_title; ?>" class="form-control" />
                            <!-- <span class="help-block"> This is inline help </span> -->
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Event Description</label>
                            <div class="col-md-7">
                                <!-- <div name="summernote" id="summernote_1"></div> -->
                            <textarea rows="12"; cols="67" class="ckeditor" value="<?php echo $newss[0]->event_desc; ?>" name="description"><?php echo $newss[0]->event_desc; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Event Image</label>
                        <div class="col-md-7">
                            <input style="float:left;" type="file" name="uploadedimages" placeholder="Image" class="form-control" multiple/>
                            <img src="<?php if($newss[0]->event_image){ echo base_url().'uploads/event_image/'.$newss[0]->event_image; }else{ echo base_url().'uploads/user_image/no_image1.jpeg'; } ?>" height="25px" width="25px">
                        </div>
                        <div class="col-md-2 pull-right">
                        <a class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete Image" onclick="delete_image(<?= $eventid;?>)"><i class='fa fa-trash'></i></a>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="control-label col-md-3">Event Video</label>
                        <div class="col-md-7">
                            <input type="file" name="event_video" placeholder="Video" class="form-control" />
                            <img src="<?php //echo base_url().'uploads/event_video/'.$newss[0]->event_video;?>" height="25px" width="25px">
                        </div>
                    </div> -->
                     <div class="form-group">
                        <label class="control-label col-md-3">Event Location</label>
                        <div class="col-md-7">
                            <input type="text" name="location" placeholder="Enter Location" value="<?php echo $newss[0]->event_location; ?>" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Event Date</label>
                        <div class="col-md-7">
                            <input type="text" name="date" id="datepicker" parsley-required="true" value="<?php echo $newss[0]->event_date; ?>" placeholder="Select Date" class="form-control" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Event Video</label>
                        <div class="col-md-7">
                            <input type="url" name="url" placeholder="Enter URL" value="<?php echo $newss[0]->event_url; ?>" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group" style="padding-left:25%">
                        <div class="col-md-2">
                            <input type="submit" name="submit" value="submit" class="form-control btn blue btn-block" />
                        </div>
                        <div class="col-md-2">
                            <a href="<?php echo base_url().'admin/event/'?>"><input type="button" name="cancel" value="Cancel" class="form-control btn blue btn-block" /> </a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- END QUICK SIDEBAR -->
    </div>
    </div>    
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
     <?php $this->load->view('admin/footer'); ?>
        <!-- END FOOTER -->
       
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/ckeditor/ckeditor.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
       
 <script type="text/javascript">
 function getsubcategory(category_id)
 { 
   var str = "category_id="+category_id;
  
   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>admin/product/get_category/",
        data:str,
        //beforeSend:function(){ alert(this.url);},
        success:function(data)
        {   $('#subcat_id').empty();
            $('#subcat_id').append(data);
        }
     });
     $('#subcat_id option').attr('selected', false);
 } 

 function delete_image(id)
 {
    var eid = id;
    var a = confirm('Are you really want to delete image');
    if(a == 1)
    {
        $.ajax({ 
            type:"POST",
            url:"<?php echo base_url();?>admin/event/delete_image/",
            data:"eventid="+eid,
            success:function(data)
            {  
                if(data == '1000')
                {
                    alert("Image Successfully Deleted");
                    location.reload();
                }
            }

        })
    }
 }            
 </script>
 <script>
    $( function() {
      $( "#datepicker" ).datepicker({
      minDate: new Date()
       /* changeMonth: true,
        changeYear: true*/
      });
    } );
</script>         
    </body>

</html>