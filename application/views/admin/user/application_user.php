<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Application Status | Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.22/themes/redmond/jquery-ui.css" /><link href="<?php echo base_url()?>template/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url()?>template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>template/assets/global/css/parsley.css" rel="stylesheet"><!-- Parsley -->
        <link href="<?php echo base_url();?>template/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet"><!-- Parsley -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>template/assets/global/css/jquery.ptTimeSelect.css" />
        <!-- END THEME LAYOUT STYLES -->

        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/header'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
           <?php $this->load->view('admin/sidebar'); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Application Status
                               
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                      <!--   <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Form Stuff</span>
                        </li> -->
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <?php 
                        $id = $this->uri->segment(4);
                        $data = $this->common_model->getData("user",array('user_id'=>$id));
                        if($this->session->flashdata('success'))
                        {
                            echo "<div class='alert alert-success'>", $this->session->flashdata('success'), "</div>";
                        }else if($this->session->flashdata('failed'))
                        {
                            echo "<div class='alert alert-danger'>", $this->session->flashdata('failed'), "</div>";
                        }
                    ?>           
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=""></i>Application Status </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <!-- <a href="javascript:;" class="remove"> </a> -->
                                                </div>
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="<?php echo base_url().'admin/user/application_user/'.$id?>" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data"  id="form11" data-parsley-validate=''>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Application Id</label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Application Id" name="appid" class="form-control" parsley-required="true" value="<?php echo $data[0]->application_id; ?>" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Applicant Name</label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Applicant Name" name="name" parsley-required="true" class="form-control" value="<?php echo $data[0]->applicant_name; ?>" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Contact No.</label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Contact No." data-parsley-type="integer" maxlength="10" minlength="10" data-error-message="Contact No. should be an integer and length should be 10 digits" name="contact" class="form-control" value="<?php echo $data[0]->contact; ?>"/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Email Id.</label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Email" name="email" parsley-required="true" class="form-control" value="<?php echo $data[0]->email; ?>" disabled required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Application Status</label>
                        <div class="col-md-7">
                            <select name="appstatus" class="form-control" parsley-required="true"  required>
                                <option value=""  <?php if($data[0]->application_status == 0){ echo 'selected'; }?>>Not Applied</option>
                                <option value="1" <?php if($data[0]->application_status == 1){ echo 'selected'; }?>>Applied</option>
                                <option value="2" <?php if($data[0]->application_status == 2){ echo 'selected'; }?>>Shortlisted</option>
                                <option value="3" <?php if($data[0]->application_status == 3){ echo 'selected'; }?>>Called for Interview</option>
                                <option value="4" <?php if($data[0]->application_status == 4){ echo 'selected'; }?>>Accepted</option>
                                <option value="5" <?php if($data[0]->application_status == 5){ echo 'selected'; }?>>Admitted</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="datetime">
                <div class="form-group">
                        <label class="control-label col-md-3">Date</label>
                        <div class="col-md-7">
                            <input type="text" name="date" id="datepicker" value="<?php echo $data[0]->appoint_date; ?>" placeholder="Select Date" class="form-control" readonly/>
                        </div>
                </div>
                <div class="form-group">
                        <label class="control-label col-md-3">Time</label>
                        <div class="col-md-7">
                            <input type="text" name="time" placeholder="Select Time" value="<?php echo $data[0]->appoint_time; ?>" class="form-control" value="" readonly>
                        </div>
                </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" value="Submit">
                            &nbsp&nbsp&nbsp&nbsp&nbsp    
                            <a href="<?php echo base_url('admin/user');?>" type="button" class="btn default">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
            </div>
                </div>
                    </div>
                      </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
     <?php $this->load->view('admin/footer'); ?>
        <!-- END FOOTER -->
       
        <!-- BEGIN CORE PLUGINS -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/js/parsley.min.js"></script>
       
        <script src="<?php echo base_url()?>template/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
         <script type="text/javascript" src="<?php echo base_url()?>template/assets/global/js/jquery.ptTimeSelect.js"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/ckeditor/ckeditor.js"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
       
<script type="text/javascript">
    $('#form11').parsley(); //parsley function

     $(document).ready(function(){
            $('input[name="time"]').ptTimeSelect();
        }); //time function

     $( function() {
      $( "#datepicker" ).datepicker({
      minDate: new Date(),
      dateFormat: 'dd-mm-yy'
       /* changeMonth: true,
        changeYear: true*/
      });
    } ); //date function

    $(document).ready(function(){
        $("select").change(function(){
            $(this).find("option:selected").each(function(){
                if($(this).attr("value") =="3"){
                //    $(".box").not(".red").hide();
                    $("#datetime").show();
                }
                else{
                    $("#datetime").hide();
                }
            });
        }).change();
    });
</script>
</body>

</html>