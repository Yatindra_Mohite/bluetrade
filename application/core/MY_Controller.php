<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller {
	
	public $data = array();

	public function __construct()
	{
		parent::__construct();

		
		$data['category_data'] = $this->common_model->getData('category', array('parent_id'=>'0'));
		foreach($data['category_data'] as $category_data)
		{  
			$sub_cat_id = 	$category_data->category_id;
			$sub_cat_data = $this->common_model->getData('category', array('parent_id'=>$sub_cat_id));
			$arr1 = array();
			foreach($sub_cat_data as $sub_cat )
		    {
		    	$arr1[] = array('sub_category_id'=>$sub_cat->category_id,'category_name'=>$sub_cat->category_name);
		    }		
            $data['arr'][] = array('category_id' => $category_data->category_id,
            	'category_name'=>$category_data->category_name,
            	'sub_category'=>$arr1
            	);

		}
       
		$this->data['category_result'] = $data['arr'];





		//for cart and header


		if($this->session->userdata('user_id')){
			$user_id = $this->session->userdata('user_id');
			$cart = $this->common_model->getData('add_to_cart', array('user_id'=>$user_id));
		}else
		{
			$cart =$this->cart->contents();
		}
		$final_discount=0;
		if($cart!=array())
		{
			if($this->session->userdata('user_id'))
			{
				foreach ($cart as $key) 
				{
					if($key->referral_code)
					{
						$product_id= $key->product_id;
						$referral_code= $key->referral_code;
						$qty= $key->qty;

					//	$code_data = $this->common_model->common_getRow('product_to_interior_designer',array('coupon_code'=>$referral_code,'product_id'=>$product_id));
				        $dode_dd=$this->db->query("SELECT product_to_interior_designer.* FROM product_to_interior_designer  LEFT JOIN product_tb ON product_tb.brand=product_to_interior_designer.brand_id WHERE product_to_interior_designer.coupon_code='$referral_code' AND product_tb.product_id='$product_id'");
				      	$code_data=$dode_dd->row();
				      	if($code_data)
				      	{
				      		$pro_data = $this->common_model->common_getRow('product_tb',array('product_id'=>$product_id));
					   		$discount=$code_data->discount;
					   		$price=$pro_data->price;
					   		$price_base=$pro_data->price_base;

					   		if($code_data->discount_type==0)
					   		{
					   			$discount_amount = (($price*$discount)/100);
					   			$price_after_discounted = $price-$discount_amount;
					   			if($price_after_discounted <= $price_base)
					   			{
					   				$final_discount += $price_base*$qty;
					   			}else
					   			{
					   				$final_discount += $discount_amount*$qty;
					   			}
					   		}else
					   		{
					   			$final_discount += $discount*$qty;
					   		}
				      	}
					}
				}

			}else
			{
				foreach ($cart as $key) 
				{
					if($key['referral_code'])
					{
						$product_id= $key['id'];
						$referral_code= $key['referral_code'];
						$qty= $key['qty'];
						//$code_data = $this->common_model->common_getRow('product_to_interior_designer',array('coupon_code'=>$referral_code,'product_id'=>$product_id));
				      	$dode_dd=$this->db->query("SELECT product_to_interior_designer.* FROM product_to_interior_designer  LEFT JOIN product_tb ON product_tb.brand=product_to_interior_designer.brand_id WHERE product_to_interior_designer.coupon_code='$referral_code' AND product_tb.product_id='$product_id'");
				      	$code_data=$dode_dd->row();
				      	if($code_data)
				      	{
				      		$pro_data = $this->common_model->common_getRow('product_tb',array('product_id'=>$product_id));
					   		$discount=$code_data->discount;
					   		$price=$pro_data->price;
					   		$price_base=$pro_data->price_base;

					   		if($code_data->discount_type==0)
					   		{
					   			$discount_amount = (($price*$discount)/100);
					   			$price_after_discounted = $price-$discount_amount;
					   			if($price_after_discounted <= $price_base)
					   			{
					   				$final_discount += $price_base*$qty;
					   			}else
					   			{
					   				$final_discount += $discount_amount*$qty;
					   			}
					   		}else
					   		{
					   			$final_discount += $discount*$qty;
					   		}
				      	}
					}
				}
			}
		}
		$this->data['discount']=$final_discount;

		$this->data['user_data']="";
		$this->data['cart_data']="";
		
		if($user_id = $this->session->userdata('user_id'))
		{	
			$user_id = $this->session->userdata('user_id');
			$this->data['cart_data'] = $this->common_model->getData('add_to_cart', array('user_id'=>$user_id));
			$this->data['user_data'] = $this->common_model->common_getRow('customers', array('serial'=>$user_id));

		}

		//end cart
		
	}

	

	public function randno($tot=false)
	{
		if($tot=='')
		{
			$tot=6;	
		}
		return $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $tot);	
	}
	function calculate_discount($referral_code,$product_id,$qty)
	{
  		$dode_dd=$this->db->query("SELECT product_to_interior_designer.* FROM product_to_interior_designer  LEFT JOIN product_tb ON product_tb.brand=product_to_interior_designer.brand_id WHERE product_to_interior_designer.coupon_code='$referral_code' AND product_tb.product_id='$product_id'");
		$code_data=$dode_dd->row();
      	if($code_data)
      	{
      		$pro_data = $this->common_model->common_getRow('product_tb',array('product_id'=>$product_id));
	   		$discount=$code_data->discount;
	   		$price=$pro_data->price;
	   		$price_base=$pro_data->price_base;

	   		if($code_data->discount_type==0)
	   		{
	   			$discount_amount = (($price*$discount)/100);
	   			$price_after_discounted = $price-$discount_amount;
	   			if($price_after_discounted <= $price_base)
	   			{
	   				return $final_discount = $price_base*$qty;
	   			}else
	   			{
	   				return $final_discount = $discount_amount*$qty;
	   			}
	   		}else
	   		{
	   			return $final_discount = $discount*$qty;
	   		}
      	}
	}	
}
