<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('designer_id')){
			redirect(base_url('designer/login'));
		}
		date_default_timezone_set('Asia/Kolkata');

		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
		//to get all details of current page like query details, exicution time and all
		//$this->output->enable_profiler(TRUE);
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$user_id = $this->session->userdata('designer_id');

		$query = $this->db->query("SELECT brand.* FROM brand ORDER BY brand.brand_id DESC ");
		$data['brand_list']=$query->result();
		$query1 = $this->db->query("SELECT product_to_interior_designer.*,brand.brand_name FROM product_to_interior_designer LEFT JOIN brand ON product_to_interior_designer.brand_id=brand.brand_id WHERE product_to_interior_designer.user_id='$user_id' AND product_to_interior_designer.del_status='0'  ORDER BY product_to_interior_designer.coupon_id DESC ");
		$data['discount_list']=$query1->result();
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$brand_id=$this->input->post('brand_id');
			$discount=$this->input->post('discount');
			$coupon_code =substr($this->common_model->randomuniqueCode(),0,6);
   			$check_code = $this->common_model->common_getRow('product_to_interior_designer',array('coupon_code'=>$coupon_code));
   			//print_r($check_code);exit;
   			if($check_code)
   			{
   				$coupon_code11 =substr($this->common_model->randomuniqueCode(),0,7);
   				$check_code11 = $this->common_model->common_getRow('product_to_interior_designer',array('coupon_code'=>$coupon_code11));
	   			if($check_code11)
	   			{
	   				$coupon_code111 =substr($this->common_model->randomuniqueCode(),0,8);
	       			$insert_code = $this->common_model->common_insert('product_to_interior_designer',array('coupon_code'=>$coupon_code111,'brand_id'=>$brand_id,'user_id'=>$user_id,'create_at'=>datetime,'discount'=>$discount,'discount_type'=>1));
	       			$this->session->set_flashdata('success','Discount Added Successfully');
	       			redirect('designer/brand');
	   			}
	       		else
	       		{
	       			$insert_code = $this->common_model->common_insert('product_to_interior_designer',array('coupon_code'=>$coupon_code11,'brand_id'=>$brand_id,'user_id'=>$user_id,'create_at'=>datetime,'discount'=>$discount,'discount_type'=>1));
	       			$this->session->set_flashdata('success','Discount Added Successfully');
	       			redirect('designer/brand');
	       		}
   			}
       		else
       		{
       			$insert_code = $this->common_model->common_insert('product_to_interior_designer',array('coupon_code'=>$coupon_code,'brand_id'=>$brand_id,'user_id'=>$user_id,'create_at'=>datetime,'discount'=>$discount,'discount_type'=>1));
       			$this->session->set_flashdata('success','Discount Added Successfully');
       			redirect('designer/brand');
       				
       		}
		}
	    $this->load->view('designer/brand_discount/show',$data);
	}


	/*public function edit($product_id=false)
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
	    {
	    	$user_id = $this->session->userdata('designer_id');
	    	$product_id=$this->input->post('product_id');
	    	$discount=$this->input->post('discount');
	    	$discount_type=$this->input->post('discount_type');

	    	$query = $this->db->query("SELECT product_tb.*,product_to_interior_designer.coupon_code,product_to_interior_designer.discount FROM product_tb LEFT JOIN product_to_interior_designer ON product_tb.product_id=product_to_interior_designer.product_id WHERE product_to_interior_designer.user_id='$user_id' AND product_tb.product_id='$product_id'");
			$product_data=$query->result();
			$price_base=$product_data[0]->price_base;
			$price=$product_data[0]->price;
			if($discount_type==0)
			{
				$discounted_amount = (($price*$discount)/100);
				$discounted_price = $price-$discounted_amount;
				if($discounted_price <= $price_base)
				{
					$this->session->set_flashdata('error', 'Discount offer can not be greater then base price!!');
		        	redirect('designer/product/edit/'.$product_id);
				}
			}else
			{
				$discounted_price = $price-$discount;
				if($discounted_price <= $price_base)
				{
					$this->session->set_flashdata('error', 'Discount offer can not be greater then base price!!');
		        	redirect('designer/product/edit/'.$product_id);
				}
			}

	    	$dis_update = $this->common_model->updateData('product_to_interior_designer',array('discount'=>$discount,'discount_type'=>$discount_type),array('product_id'=>$product_id,'user_id'=>$user_id));
	    	redirect('designer/product/show_product');
	    	//die('king');
	    }

		$user_id = $this->session->userdata('designer_id');
		$query = $this->db->query("SELECT product_tb.*,product_to_interior_designer.coupon_code,product_to_interior_designer.discount,product_to_interior_designer.discount_type FROM product_tb LEFT JOIN product_to_interior_designer ON product_tb.product_id=product_to_interior_designer.product_id WHERE product_to_interior_designer.user_id='$user_id' AND product_tb.product_id='$product_id'");
		$data['product_data']=$query->result();
		$data['category_data'] = $this->common_model->getData('category');
	    $this->load->view('designer/product/edit_product',$data);

	}*/
	public function  delete_coupon($coupon_id=false)
	{  
		$product_delete = $this->common_model->updateData('product_to_interior_designer',array('del_status'=>1),array('coupon_id'=>$coupon_id));
		if($product_delete)
		{
			echo "1000"; exit;
		}
	}

}
