<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if($userid = $this->session->userdata('designer_id')){
			redirect(base_url('designer/dashboard'));
		}
		date_default_timezone_set('Asia/Kolkata');
		//to get all details of current page like query details, exicution time and all
		//$this->output->enable_profiler(TRUE);
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{ 
		if(isset($_POST['submit']))
		{
			      $username = $this->input->post ( 'email' );
				  $password = md5($this->input->post('password')) ;
				
				$AdminData = $this->common_model->common_getRow('interior_designer',array('email'=>$username,'password'=>$password));
				if (!empty ( $AdminData ) ) 
				{
						$this->session->set_userdata ( array (
								'designer_id'   => $AdminData->user_id,
								'email'	     => $AdminData->email
														));
						   
						redirect(base_url().'designer/dashboard');
				}
				else 
				{
					$this->session->set_flashdata('msg' ,'The Email or Password you entered is incorrect');	
					redirect( base_url('designer/login') );
				}
		}
		$this->load->view('designer/index');
		
	}
}
