<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('designer_id')){
			redirect(base_url('designer/login'));
		}
		date_default_timezone_set('Asia/Kolkata');

		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
		//to get all details of current page like query details, exicution time and all
		//$this->output->enable_profiler(TRUE);
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{ 
	 	$user_id = $this->session->userdata('designer_id');
		$statics_data = $this->db->query("SELECT order_product.*,orders.create_at  FROM `order_product` LEFT JOIN `orders` ON `order_product`.`order_id`=`orders`.`serial` LEFT JOIN product_to_interior_designer ON order_product.coupon_code =product_to_interior_designer.coupon_code  WHERE product_to_interior_designer.coupon_code!='' AND product_to_interior_designer.user_id='$user_id'");	
		$this->data['order_data']=$statics_data->result();
		//print_r($this->db->last_query());exit;
		$this->load->view('designer/order/show',$this->data);

	}

}
