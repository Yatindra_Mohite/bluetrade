<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('designer_id')){
			redirect(base_url('designer/login'));
		}
		//to get all details of current page like query details, exicution time and all
		//$this->output->enable_profiler(TRUE);
	}
	/*if(!$this->session->userdata('admin_id'))
	{
			redirect( base_url() );	
	}*/
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
    {	
    	$user_id = $this->session->userdata('designer_id');
    	$total_sales = $this->db->query("SELECT count(*) AS pro_count,sum(order_product.total) AS total FROM `order_product` LEFT JOIN product_to_interior_designer ON order_product.coupon_code =product_to_interior_designer.coupon_code  WHERE product_to_interior_designer.coupon_code!='' AND product_to_interior_designer.user_id='$user_id'");	
		$this->data['total_sales']=$total_sales->result();

		$statics_data = $this->db->query("SELECT (sum(order_product.total)-sum(product_tb.price_base*order_product.quantity)) AS diff  FROM `order_product` LEFT JOIN product_to_interior_designer ON order_product.coupon_code =product_to_interior_designer.coupon_code LEFT JOIN product_tb ON order_product.product_id=product_tb.product_id WHERE product_to_interior_designer.coupon_code!='' AND product_to_interior_designer.user_id='$user_id'");	
		$this->data['statics_data']=$statics_data->result();
		$this->load->view('designer/dashboard',$this->data);
	}
}
