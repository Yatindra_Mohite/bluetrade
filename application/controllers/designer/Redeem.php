<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Redeem extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('designer_id')){
			redirect(base_url('designer/login'));
		}
		date_default_timezone_set('Asia/Kolkata');

		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
		//to get all details of current page like query details, exicution time and all
		//$this->output->enable_profiler(TRUE);
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{ 
	 
    //  $data['category_data'] = $this->common_model->getData('category');
	  $this->load->view('designer/radeem/show');

	}
	
public function show_product()
{
	$user_id = $this->session->userdata('designer_id');

    //$data['product_data'] = $this->common_model->getData('product_tb',array(),'product_id','DESC');
	$query = $this->db->query("SELECT product_tb.*,product_to_interior_designer.coupon_code,product_to_interior_designer.discount,product_to_interior_designer.discount_type FROM product_tb LEFT JOIN product_to_interior_designer ON product_tb.product_id=product_to_interior_designer.product_id WHERE product_to_interior_designer.user_id='$user_id'");
	$data['product_data']=$query->result();
	//print_r($data);
    $this->load->view('designer/product/show_product',$data);

}


public function  delete_product($product_id=false)
{  
	$product_delete = $this->common_model->updateData('product_tb',array('del_status'=>1),array('product_id'=>$product_id));
	
	if($product_delete)
	{
		echo "1000"; exit;
	}
}

}
