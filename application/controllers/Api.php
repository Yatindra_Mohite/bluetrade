<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends MY_Controller {
function __construct() {
		parent::__construct();
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
	}

	public function signup() 
	{
		$headers = apache_request_headers();
		$json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    if(!empty($json_array))
	    {
	     	$final_output = array();
	     	$check_phone = $this->common_model->common_getRow('customers',array('phone'=>$json_array->phone));
	     	if($check_phone)
	     	{  
				/*$data_array   = array(
					                  'phone'=>$json_array->phone,
					                  'country_code'=>$json_array->country_code,
					                  'create_at'=>militime,
					                  'phone_otp'=>'1234',
					                  'status'=>0
					                );
				
				$insertId = $this->common_model->common_insert('customers', $data_array);
				if($insertId)
				{  
					$final_output['status'] = 'success';
					$final_output['user_id'] = $insertId;		
					$final_output['message'] = 'successfully';	
				}
				else
				{ 
					$final_output['status'] = 'failed';	
				}*/
				$update_phone = $this->common_model->updateData('customers',array('phone_otp'=>'1234','status'=>0),array('phone'=>$json_array->phone));
	     		if($update_phone)
				{  
					$final_output['status'] = 'success';
					$final_output['user_id'] = $check_phone->serial;		
					$final_output['message'] = 'successfully';	
				}
	     	}
	     	else
	     	{
	     		$final_output['status'] = 'failed';	
				$final_output['message'] = 'please register first!!';	
	     		/*$update_phone = $this->common_model->updateData('customers',array('phone_otp'=>'1234','status'=>0),array('phone'=>$json_array->phone));
	     		if($update_phone)
				{  
					$final_output['status'] = 'success';
					$final_output['user_id'] = $check_phone->serial;		
					$final_output['message'] = 'successfully';	
				}
				else
				{ 
					$final_output['status'] = 'failed';	
				}*/
	     	}
	     	
	    }
	    else{
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);

	}

	public function register() 
	{
		$headers = apache_request_headers();
		$json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    if(!empty($json_array))
	    {
	     	$final_output = array();
	     	$check_row = $this->db->query("SELECT * FROM customers WHERE phone='$json_array->phone' OR email='$json_array->email'");
	     	$check_row1=$check_row->row();
	     	if($check_row1)
	     	{  
	     		if($check_row1->email==$json_array->email)
	     		{

	     			$final_output['status'] = 'failed';	
					$final_output['message'] = 'email elready exist!!';	
	     		}else if($check_row1->phone==$json_array->phone)
	     		{
	     			$final_output['status'] = 'failed';	
					$final_output['message'] = 'phone elready exist!!';	
	     		}
	     	}
	     	else
	     	{
	     		$data_array   = array(
					                  'name'=>$json_array->name,
					                  'phone'=>$json_array->phone,
					                  'email'=>$json_array->email,
					                  'country_code'=>$json_array->country_code,
					                  'create_at'=>militime,
					                  'phone_otp'=>'1234',
					                  'status'=>0
					                );
				
				$insertId = $this->common_model->common_insert('customers', $data_array);
				if($insertId)
				{  
					$final_output['status'] = 'success';
					$final_output['user_id'] = $insertId;		
					$final_output['message'] = 'successfully';	
				}
				else
				{ 
					$final_output['status'] = 'failed';	
				}
	     	}
	     	
	    }
	    else{
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);

	}

	public function verify()
	{		
		$headers = apache_request_headers();

		$json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    
	    if(!empty($json_array))
	    {

	     	$final_output = array();
	     	$check_phone = $this->common_model->common_getRow('customers',array('serial'=>$json_array->user_id,'phone_otp'=>$json_array->phone_otp));
	     	if($check_phone)
	     	{  
     			$auth_key1 = bin2hex(openssl_random_pseudo_bytes(16));
				$auth_key = $auth_key1.militime;


				
				$update = $this->common_model->updateData('customers',array('status'=>1,'token'=>$auth_key),array('serial'=>$json_array->user_id));
				if($update)
				{  
					$final_output['status'] = 'success';
					$final_output['message'] = 'successfully';	
					$final_output['secret_key'] = $auth_key;	

				}
				else
				{ 
					$final_output['status'] = 'failed';	
				}
	     	}
	     	else
	     	{
				$final_output['status'] = 'failed';	
				$final_output['message'] = 'Otp not matched';	
				
	     	}
	     	
	     }
	     else{
	     	$final_output['status'] = 'unauthorized access';
	     }
	     header("content-type: application/json");
	     echo json_encode($final_output);
	}

   /*public function login() 
	{
		$json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    
	    if(!empty($json_array))
	    {   
	    	$userData = '';
			$username = '';
	    	$final_output= array();
	    	
	    	if(!empty($json_array->username)){ $username = $json_array->username; }
			
			$password = $this->common_model->pass_encrypt ( $json_array->password );
			$EmailData = $this->common_model->getDataRow('users',array('email'=>$username,'password'=>$password));
			
			if(!empty($EmailData))
			{
				$userData = $EmailData; 
			}
			else{
				$userData = $this->common_model->getDataRow('users',array('username'=>$username,'password'=>$password));
			}

			if (!empty ( $userData ) || !empty ( $userData ) ) 
			{  
				if($userData->activate==1)
				{

					$auth_key1 = bin2hex(openssl_random_pseudo_bytes(16));
					$auth_key = $auth_key1.militime;


					$data = array( 'token'=> md5($userData->user_id.time()),'auth_key'=>$auth_key );
					$where_array = array('user_id'=>$userData->user_id);
					$this->common_model->updateData('users',$data,$where_array);

					$final_output['status'] = 'success';
	     			$final_output['data'] = array('user_id'=>$userData->user_id,'token'=>md5($userData->user_id.time()),'auth_key'=>$auth_key);
				}
				else
				{
					$final_output['status'] = 'failed';
	     			$final_output['message'] = 'Your account is Inactive, please contact with site administrator';
				}
			
			}
			else 
			{
				$final_output['status'] = 'failed';
     			$final_output['message'] = 'The Email or Password you entered is incorrect';
			}
	    }
	    else{
	     	$final_output['status'] = 'unauthorized access';
	     }
	    echo json_encode($final_output);
	}*/
    
    public function get_version_code()
    {
	    $json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    
	    if(!empty($json_array))
	    {   //$final_output= array();
	     	if(!empty($json_array->version_code))
	     	{
		$version_update = $this->common_model->getDataRow('version_code',array('version_code'=>$json_array->version_code));
			    if(!empty($version_update))
			    {
			     		$final_output['status'] = 'success';
			     		$final_output['status_maintain'] = $version_update->status_maintain;
			     		$final_output['data'] = $version_update;
			    }
			    else{
			     		$final_output['status'] = 'failed';
			     		$final_output['data'] = 'version update not found';
			    }
	     	}
	     	else{
	     		$final_output['status'] = 'failed';
	     		$final_output['message'] = 'please enter version code';
	     	}
	    }
	    else{
	     	$final_output['status'] = 'unauthorized access';
	     }

	     echo json_encode($final_output);
  	}

    public function getCategory()
    {
	    $json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    
	    if(!empty($json_array)  && ($json_array->category_id !=''))
	    {   
			$result = $this->common_model->getData('category',array('parent_id'=>$json_array->category_id));
		    if(!empty($result))
		    {
		    	//print_r($result);exit;
		        $data_arr = array(); 
		    	$final_output['status'] = 'success';
		    	foreach ($result as $data) 
		    	{
		    		if($data->category_image)
		    		{
		    			$image= base_url()."uploads/category/".$data->category_image;
		    		}
					$subresult = $this->common_model->getData('category',array('parent_id'=>$data->category_id));
		    		$data_arr[] = array('category_id'=>$data->category_id,'category_name'=>$data->category_name,'category_image'=>$image,'details'=>$subresult);

		    	}
		  		$final_output['data'] = $data_arr;
		    }
		    else
		    {
	     		$final_output['status'] = 'failed';
	     		$final_output['data'] = 'categories not found';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function getProductByCategory()
    {
	    $json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    //print_r($json_array->category_id);exit;
	   /* if(!empty($json_array)  && ($json_array->category_id !='') && $json_array->create_at !='')
	    {*/   
	    	if($json_array->create_at=='0')
	    	{
	    		$create_at1 ="";
	    	}else{
	    		$create_at1 ="AND product_tb.create_at < '".$json_array->create_at."'";
	    	}
			$result1 = $this->db->query("SELECT product_tb.* FROM product_tb WHERE product_tb.category_id='".$json_array->category_id."' AND product_tb.del_status='0' ".$create_at1." ORDER BY product_tb.product_id DESC LIMIT 10");
			$result= $result1->result();
		    if(!empty($result))
		    {
		    	foreach ($result as $key) 
		    	{
		    		$aa[]=array(
		    				"product_id"=>$key->product_id,
		    				"product_name"=>$key->product_name,
		    				"p_image"=>base_url()."uploads/product_image/".$key->p_image,
		    				"price"=>$key->price,
		    				"product_code"=>$key->product_code,
		    				"material"=>$key->material,
		    				"color"=>$key->color,
		    				"brand"=>$key->brand,
		    				"style"=>$key->style,
		    				"description"=>$key->description,
		    				"long_description"=>$key->long_description,
		    				"create_at"=>$key->create_at
		    			);
		    	}
		    	$final_output['status'] = 'success';
		  		$final_output['data'] = $aa;

		    }
		    else
		    {
	     		$final_output['status'] = 'failed';
	     		$final_output['data'] = 'product not found';
		    }
	  /*  }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }*/
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function getProductByCategoryPullTorefresh()
    {
	    $json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    
	    /*if(!empty($json_array) && ($json_array->category_id!='') && ($json_array->create_at!=''))
	    {*/   
	    	if($json_array->create_at==0)
	    	{
	    		$create_at1 ="";
	    	}else{
	    		$create_at1 ="AND product_tb.create_at > '".$json_array->create_at."'";
	    	}
			$result1 = $this->db->query("SELECT product_tb.* FROM product_tb WHERE product_tb.category_id='".$json_array->category_id."' AND product_tb.del_status='0' ".$create_at1." ORDER BY product_tb.product_id DESC LIMIT 10");
			$result= $result1->result();
		    if(!empty($result))
		    {
		    	
		    	foreach ($result as $key) 
		    	{
		    		$aa[]=array(
		    				"product_id"=>$key->product_id,
		    				"product_name"=>$key->product_name,
		    				"p_image"=>base_url()."uploads/product_image/".$key->p_image,
		    				"price"=>$key->price,
		    				"product_code"=>$key->product_code,
		    				"material"=>$key->material,
		    				"color"=>$key->color,
		    				"brand"=>$key->brand,
		    				"style"=>$key->style,
		    				"description"=>$key->description,
		    				"long_description"=>$key->long_description,
		    				"create_at"=>$key->create_at
		    			);
		    	}

		    	$final_output['status'] = 'success';
		  		$final_output['data'] = $aa;
		    }
		    else
		    {
	     		$final_output['status'] = 'failed';
	     		$final_output['data'] = 'product not found';
		    }
	    /*}
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }*/
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }
    public function getProductByAjax()
    {
	    $json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);

			$result1 = $this->db->query("SELECT product_tb.* FROM product_tb WHERE product_tb.product_name LIKE '%".$json_array->search_name."%' AND product_tb.del_status='0' ");
			$result= $result1->result();
			//print_r($this->db->last_query());exit;
		    if(!empty($result))
		    {
		    	foreach ($result as $key) 
		    	{
		    		$aa[]=array(
		    				"product_id"=>$key->product_id,
		    				"product_name"=>$key->product_name,
		    				"p_image"=>base_url()."uploads/product_image/".$key->p_image,
		    				"price"=>$key->price,
		    				"product_code"=>$key->product_code,
		    				"material"=>$key->material,
		    				"color"=>$key->color,
		    				"brand"=>$key->brand,
		    				"style"=>$key->style,
		    				"description"=>$key->description,
		    				"long_description"=>$key->long_description,
		    				"create_at"=>$key->create_at
		    			);
		    	}
		    	$final_output['status'] = 'success';
		  		$final_output['data'] = $aa;

		    }
		    else
		    {
	     		$final_output['status'] = 'failed';
	     		$final_output['data'] = 'product not found';
		    }

	    header("content-type: application/json");
	    echo json_encode($final_output);
    }
    public function getProductDetailById()
    {
    	$json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    if(!empty($json_array)  && ($json_array->product_id !=''))
	    { 
	    	$result = array(); 
	    	$result = $this->common_model->common_getRow('product_tb',array('product_id'=>$json_array->product_id));
		    if(!empty($result))
		    {
		    	$final_output['status'] = 'success';
		  		$final_output['data'] = $result;
		    }else
		    {
		    	$final_output['status'] = 'product not found';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }

	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function addToCart()
    {
    	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$serial = $check['data']->serial; 
				$json1 = file_get_contents('php://input');
	    		$json_array = json_decode($json1);
				$result = array(); 
				$check_cart = $this->common_model->common_getRow('add_to_cart',array('product_id'=>$json_array->product_id,'user_id'=>$serial));
				if($check_cart)
				{
					$final_output['status'] = 'failed';
			    	$final_output['message'] = 'already in cart';	
				}else
				{
					$result = $this->common_model->common_insert('add_to_cart',array('product_id'=>$json_array->product_id,'qty'=>$json_array->quantity,'user_id'=>$serial,'price'=>$json_array->subtotal,'create_at'=>militime));
				    if($result)
				    {
				    	$final_output['status'] = 'success';
				    	$final_output['message'] = 'successfully';	
				    }else
				    {
				    	$final_output['status'] = 'failed';
				    }
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function OrderCheckout()
    {
    	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$user_id = $check['data']->serial; 
				$json1 = file_get_contents('php://input');
	    		$json_array = json_decode($json1);

				/*$upd_data=array(
									'name'=>$json_array->name,
									'email'=>$json_array->email,
									'phone'=>$json_array->phone
								);*/

				$ins_data=array(
									'date'=>date('Y-m-d'),
									'customerid'=>$user_id,
									'address_id'=>$json_array->address_id,
									'payment_method'=>$json_array->payment_method,
									'comment'=>$json_array->comment,
									'create_at'=>militime,
									'date_added'=>datetime
								);

				
				$check_pro = $this->common_model->common_getRow('product_tb',array('product_id'=>$json_array->product_id,'del_status'=>0));
				if($check_pro)
				{
				//	$this->common_model->updateData('customers',$upd_data,array('serial'=>$user_id));
					$order_id = $this->common_model->common_insert('orders',$ins_data);
					if($order_id)
					{
						$pro_data=array(
									'order_id'=>$order_id,
									'product_id'=>$json_array->product_id,
									'name'=>$check_pro->product_name,
									'quantity'=>$json_array->quantity,
									'price'=>$check_pro->price,
								//	'total'=>total,
									'coupon_code'=>$json_array->coupon_code
									//'discount'=>$discount
								);
						$order_pro_id = $this->common_model->common_insert('order_product',$pro_data);
						if($order_pro_id)
						{
							$final_output['status'] = 'success';
			    			$final_output['message'] = 'Order placed successfully';	
						}
					}else
					{
						$final_output['status'] = 'failed';
			    		$final_output['message'] = 'Somthind Wrong!!';	
					}

				}else
				{
					$final_output['status'] = 'failed';
			    	$final_output['message'] = 'Product Not Found';	
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function OrderCartCheckout()
    {
    	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$user_id = $check['data']->serial; 
				$json1 = file_get_contents('php://input');
	    		$json_array = json_decode($json1);

			/*	$upd_data=array(
									'name'=>$json_array->name,
									'email'=>$json_array->email,
									'phone'=>$json_array->phone
								);*/

				$ins_data=array(
									'date'=>date('Y-m-d'),
									'customerid'=>$user_id,
									'address_id'=>$json_array->address_id,
									'payment_method'=>$json_array->payment_method,
									'comment'=>$json_array->comment,
									'create_at'=>militime,
									'date_added'=>datetime
								);

				
				$check_cart = $this->common_model->getData('add_to_cart',array('user_id'=>$user_id));
				if($check_cart)
				{
				//	$this->common_model->updateData('customers',$upd_data,array('serial'=>$user_id));
					$order_id = $this->common_model->common_insert('orders',$ins_data);
					if($order_id)
					{
						foreach ($check_cart as $key) 
						{
							$pro_data=array(
									'order_id'=>$order_id,
									'product_id'=>$key->product_id,
									'name'=>$key->name,
									'quantity'=>$key->qty,
									'price'=>$key->price,
								//	'total'=>total,
									'coupon_code'=>$key->referral_code
									//'discount'=>$discount
								);
							$order_pro_id = $this->common_model->common_insert('order_product',$pro_data);
						}
						$this->common_model->deleteData('add_to_cart',array('user_id'=>$user_id));
						$final_output['status'] = 'success';
		    			$final_output['message'] = 'Order placed successfully';	
						
					}else
					{
						$final_output['status'] = 'failed';
			    		$final_output['message'] = 'Somthind Wrong!!';	
					}

				}else
				{
					$final_output['status'] = 'failed';
			    	$final_output['message'] = 'Product Not Found in cart!!';	
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function getCart()
    {
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$serial = $check['data']->serial; 
				$result1 = $this->db->query("SELECT add_to_cart.cart_id,add_to_cart.product_id,add_to_cart.qty,product_tb.* FROM add_to_cart LEFT JOIN product_tb ON add_to_cart.product_id=product_tb.product_id WHERE add_to_cart.user_id='$serial'");
				$result=$result1->result();
				if(!empty($result))
				{
					$final_output['status'] = 'success';	
					$final_output['message'] = 'successfully';	

					foreach ($result as $key) 
					{
						$upd_data[] = array(
								"cart_id"=>$key->cart_id,
								"product_id"=>$key->product_id,
								"qty"=>$key->qty,
								"product_name"=>$key->product_name,
			    				"p_image"=>base_url()."uploads/product_image/".$key->p_image,
			    				"price"=>$key->price,
			    				"product_code"=>$key->product_code,
			    				"material"=>$key->material,
			    				"color"=>$key->color,
			    				"brand"=>$key->brand,
			    				"style"=>$key->style,
			    				"description"=>$key->description,
			    				"long_description"=>$key->long_description,
								"create_at"=>$key->create_at,
						);	
		  				$final_output['data'] = $upd_data;
					}
				}else
				{	
					$final_output['status'] = 'failed';	
					unset($final_output['data']);
					$final_output['message'] = 'Your cart is empty';	
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function getOrderList()
    {
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$user_id = $check['data']->serial; 
				$result1 = $this->db->query("SELECT `order_product`.*,product_tb.* FROM `order_product` LEFT JOIN `orders` ON `order_product`.`order_id`=`orders`.`serial` LEFT JOIN `product_tb` ON `order_product`.`product_id`=`product_tb`.`product_id` WHERE `orders`.`customerid`='$user_id'");
				$result=$result1->result();
				if(!empty($result))
				{
					$final_output['status'] = 'success';	
					$final_output['message'] = 'successfully';	

					foreach ($result as $key) 
					{
						$upd_data[] = array(
								"order_id"=>$key->order_id,
								"product_id"=>$key->product_id,
								"qty"=>$key->quantity,
								"product_name"=>$key->name,
			    				"p_image"=>base_url()."uploads/product_image/".$key->p_image,
			    				"price"=>$key->price,
			    				"coupon_code"=>$key->coupon_code,
			    				"discount"=>$key->discount,
			    				"total"=>$key->total,
			    				"material"=>$key->material,
			    				"color"=>$key->color,
			    				"brand"=>$key->brand,
			    				"style"=>$key->style,
			    				"description"=>$key->description,
			    				"long_description"=>$key->long_description,
								"create_at"=>$key->create_at,
						);	
		  				$final_output['data'] = $upd_data;
					}
				}else
				{	
					$final_output['status'] = 'failed';	
					unset($final_output['data']);
					$final_output['message'] = 'Your cart is empty';	
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function addToTouchFeelCart()
    {
    	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$serial = $check['data']->serial; 
				$json1 = file_get_contents('php://input');
	    		$json_array = json_decode($json1);
				$result = array(); 
				$product_ids= $json_array->product_id;
				$product_id1=explode(',', $product_ids);
				//print_r($product_id1);exit;

				foreach ($product_id1 as $product_id) {
					$check_cart = $this->common_model->common_getRow('add_to_touch_feel',array('product_id'=>$product_id,'user_id'=>$serial,'status'=>0));
					if($check_cart)
					{
						$final_output['status'] = 'failed';
				    	$final_output['message'] = 'already in cart';	
					}else
					{
						$result = $this->common_model->common_insert('add_to_touch_feel',array('product_id'=>$product_id,'user_id'=>$serial,'create_at'=>militime));
					    if($result)
					    {
					    	$final_output['status'] = 'success';
					    	$final_output['message'] = 'successfully';	
					    }
					}
				}
				
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function OrderTouchFeelCart()
    {
    	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$serial = $check['data']->serial; 
				$json1 = file_get_contents('php://input');
	    		$json_array = json_decode($json1);
				$result = array(); 
				$check_cart = $this->common_model->common_getRow('add_to_touch_feel',array('user_id'=>$serial,'status'=>0));
				if($check_cart)
				{
					$order_id = $this->common_model->common_insert('order_touch_feel',array('customer_id'=>$serial,'create_at'=>militime));
				    if($order_id)
				    {
				    	$this->common_model->updateData('add_to_touch_feel',array('order_touch_feel_id'=>$order_id),array('user_id'=>$serial,'status'=>0));
				    	$final_output['status'] = 'success';
				    	$final_output['message'] = 'successfully';	
				    }else
				    {
				    	$final_output['status'] = 'failed';
				    }
					
				}else
				{
				    $final_output['status'] = 'failed';
			    	$final_output['message'] = 'cart is empty';	
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function getTouchAndFeelCart()
    {
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$serial = $check['data']->serial; 
				$result1 = $this->db->query("SELECT add_to_touch_feel.cart_id,add_to_touch_feel.product_id,product_tb.* FROM add_to_touch_feel LEFT JOIN product_tb ON add_to_touch_feel.product_id=product_tb.product_id WHERE add_to_touch_feel.user_id='$serial'");
				$result=$result1->result();
				if(!empty($result))
				{
					$final_output['status'] = 'success';	
					$final_output['message'] = 'successfully';	

					foreach ($result as $key) 
					{
						$upd_data[] = array(
								"cart_id"=>$key->cart_id,
								"product_id"=>$key->product_id,
								"product_name"=>$key->product_name,
			    				"p_image"=>base_url()."uploads/product_image/".$key->p_image,
			    				"price"=>$key->price,
			    				"product_code"=>$key->product_code,
			    				"material"=>$key->material,
			    				"color"=>$key->color,
			    				"brand"=>$key->brand,
			    				"style"=>$key->style,
			    				"description"=>$key->description,
			    				"long_description"=>$key->long_description
							//	"create_at"=>$key->create_at,
						);	
		  				$final_output['data'] = $upd_data;
					}
				}else
				{	
					$final_output['status'] = 'failed';	
					unset($final_output['data']);
					$final_output['message'] = 'Your cart is empty';	
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function profileUpdate()
    {
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$serial = $check['data']->serial; 

			    if(isset($_FILES['profile_pic']['name']) && $_FILES['profile_pic']['name'] != '')
				{  
					$date = date("ymdhis");
					$config['upload_path'] = 'uploads/user_image/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					
					$subFileName = explode('.',$_FILES['profile_pic']['name']);
					$ExtFileName = end($subFileName);
					$config['file_name'] = md5($date.$_FILES['profile_pic']['name']).'.'.$ExtFileName;
		            
					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					if($this->upload->do_upload('profile_pic'))
					{ 
		                $upload_data = $this->upload->data();
						$image =  $upload_data['file_name'];
					}
				}
				else
				{  
					$image = $check['data']->image; 
				}
			//print_r($_GET);exit;
				$upd_data = array(
								"name"=>$this->input->get_post('name'),
								"email"=>$this->input->get_post('email'),
								"dob"=>$this->input->get_post('dob'),
								"image"=>$image,

					);	
			//	print_r($upd_data);exit;
				$result = $this->common_model->updateData('customers',$upd_data,array('serial'=>$serial));
			    if($result)
			    {
			    	$user_data = array(
								"name"=>$this->input->get_post('name'),
								"email"=>$this->input->get_post('email'),
								"dob"=>$this->input->get_post('dob'),
								"image"=>base_url()."uploads/user_image/".$image,

					);	

			  		$final_output['data'] = $user_data;
			  		$final_output['status'] = 'success';	
					$final_output['message'] = 'successfully';	
			    }
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function getProfile()
    {
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$serial = $check['data']->serial; 

				if($check['data']->image)
				{
					$image=base_url()."uploads/user_image/".$check['data']->image;
				}else
				{
					$image="";
				}
				$upd_data = array(
								"name"=>$check['data']->name,
								"email"=>$check['data']->email,
								"dob"=>$check['data']->dob,
								"image"=>$image,

					);	
		  		$final_output['data'] = $upd_data;
		  		$final_output['status'] = 'success';	
				$final_output['message'] = 'successfully';	
			    
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }
    //need to fix some field
    public function addAddress()
    {
    	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$serial = $check['data']->serial; 
				$is_add = $this->common_model->common_getRow('customer_address',array('user_id'=>$serial));
				if($is_add)
				{
					$final_output['status'] = 'failed';
				    $final_output['message'] = 'you can add only one address';	
				}else
				{
					
					$json1 = file_get_contents('php://input');
		    		$json_array = json_decode($json1);
					$result = array(); 
			    	$result = $this->common_model->common_insert('customer_address',array('user_id'=>$serial,'zip_code'=>$json_array->zip_code,'address1'=>$json_array->address1,'address2'=>$json_array->address2,'city_id'=>$json_array->city_id,'state_id'=>$json_array->state_id,'country_id'=>'113','create_at'=>militime));
				    if($result)
				    {
				    	$final_output['status'] = 'success';
				    	$final_output['message'] = 'successfully';	
				    }else
				    {
				    	$final_output['status'] = 'failed';
				    }
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function getAddress()
    {
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$serial = $check['data']->serial; 
				$result1 = $this->db->query("SELECT customer_address.*,region.Region,city.City FROM customer_address LEFT JOIN region ON customer_address.state_id=region.RegionId LEFT JOIN city ON customer_address.city_id=city.CityId WHERE customer_address.user_id='$serial'");
				$result=$result1->result();
				if(!empty($result))
				{
					$final_output['status'] = 'success';	
					$final_output['message'] = 'successfully';	

					foreach ($result as $key) 
					{
						$upd_data = array(
								"address_id"=>$key->address_id,
								"name"=>$check['data']->name,
								"email"=>$check['data']->email,
								"phone"=>$check['data']->phone,
								"zip_code"=>$key->zip_code,
								"address1"=>$key->address1,
								"address2"=>$key->address2,
								"city_id"=>$key->city_id,
								"state_id"=>$key->state_id,
								"state"=>$key->Region,
								"city"=>$key->City

						);	
		  				$final_output['data'] = $upd_data;
					}
				}else
				{	
					$final_output['status'] = 'failed';	
					unset($final_output['data']);
					$final_output['message'] = 'please add address';	
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function updateAddress()
    {
    	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$serial = $check['data']->serial; 
				$json1 = file_get_contents('php://input');
	    		$json_array = json_decode($json1);
				$result = array(); 
				$upd_data=array(
									'name'=>$json_array->name,
									'email'=>$json_array->email,
									'phone'=>$json_array->phone
								);
				$is_add = $this->common_model->common_getRow('customer_address',array('user_id'=>$serial));
				if($is_add)
				{
			    	$result = $this->common_model->updateData('customer_address',array('zip_code'=>$json_array->zip_code,'address1'=>$json_array->address1,'address2'=>$json_array->address2,'city_id'=>$json_array->city_id,'state_id'=>$json_array->state_id,'update_at'=>militime),array('user_id'=>$serial));
				    if($result)
				    {
				    	$this->common_model->updateData('customers',$upd_data,array('serial'=>$serial));
				    	$final_output['status'] = 'success';
				    	$final_output['message'] = 'successfully';	
				    }else
				    {
				    	$final_output['status'] = 'failed';
				    }
					
				}else
				{
					$result = $this->common_model->common_insert('customer_address',array('user_id'=>$serial,'zip_code'=>$json_array->zip_code,'address1'=>$json_array->address1,'address2'=>$json_array->address2,'city_id'=>$json_array->city_id,'state_id'=>$json_array->state_id,'country_id'=>'113','create_at'=>militime));
				    if($result)
				    {
				    	$this->common_model->updateData('customers',$upd_data,array('serial'=>$serial));
				    	$final_output['status'] = 'success';
				    	$final_output['message'] = 'successfully';	
				    }else
				    {
				    	$final_output['status'] = 'failed';
				    }
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }
    public function getState()
    {

		$result = $this->common_model->getData('region');
		if(!empty($result))
		{
			$final_output['status'] = 'success';	
			$final_output['message'] = 'successfully';	

			foreach ($result as $key) 
			{
				$upd_data[] = array(
						"state_id"=>$key->RegionId,
						"name"=>$key->Region
				);	
  				$final_output['data'] = $upd_data;
			}
		}else
		{	
			$final_output['status'] = 'failed';	
			unset($final_output['data']);
			$final_output['message'] = 'No state';	
		}


	    header("content-type: application/json");
	    echo json_encode($final_output);
    }
    public function getCity()
    {
    	$json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    
	    if(!empty($json_array))
	    { 
	    	$state_id=$json_array->state_id;
			$result = $this->common_model->getData('city',array('RegionID'=>$state_id));
			if(!empty($result))
			{
				$final_output['status'] = 'success';	
				$final_output['message'] = 'successfully';	

				foreach ($result as $key) 
				{
					$upd_data[] = array(
							"city_id"=>$key->CityId,
							"name"=>$key->City
					);	
	  				$final_output['data'] = $upd_data;
				}
			}else
			{	
				$final_output['status'] = 'failed';	
				unset($final_output['data']);
				$final_output['message'] = 'No city';	
			}
		}
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }


	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function getBrand()
    {
		$result = $this->common_model->getData('brand');
		if(!empty($result))
		{
			$final_output['status'] = 'success';	
			$final_output['message'] = 'successfully';	

			foreach ($result as $key) 
			{
				$upd_data[] = array(
						"brand_id"=>$key->brand_id,
						"brand_name"=>$key->brand_name
				);	
  				$final_output['data'] = $upd_data;
			}
		}else
		{	
			$final_output['status'] = 'failed';	
			unset($final_output['data']);
			$final_output['message'] = 'No brand';	
		}

	    header("content-type: application/json");
	    echo json_encode($final_output);
    }

    public function CartProductUpdate()
    {
    	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$json1 = file_get_contents('php://input');
	    		$json_array = json_decode($json1);
	    		$product_id=$json_array->product_id;
	    		$quantity=$json_array->quantity;
	    		if($quantity!="" && $product_id!="")
	    		{
					$serial = $check['data']->serial; 
					$is_add = $this->common_model->common_getRow('add_to_cart',array('user_id'=>$serial,'product_id'=>$product_id));
					if($is_add)
					{
						$json1 = file_get_contents('php://input');
			    		$json_array = json_decode($json1);
						$result = array(); 
				    	$result = $this->common_model->updateData('add_to_cart',array('qty'=>$quantity),array('user_id'=>$serial,'product_id'=>$product_id));
					    if($result)
					    {
					    	$final_output['status'] = 'success';
					    	$final_output['message'] = 'successfully';	
					    }else
					    {
					    	$final_output['status'] = 'failed';
					    }
						
					}else
					{
						$final_output['status'] = 'failed';
					    $final_output['message'] = 'Item Not Exist!!';	
					}
				}else
				{
					$final_output['status'] = 'empty request parameter!!';
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }
    public function RemoveCartItem()
    {
    	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$json1 = file_get_contents('php://input');
	    		$json_array = json_decode($json1);
	    		$product_id=$json_array->product_id;
	    		if($product_id!="")
	    		{
					$serial = $check['data']->serial; 
					$is_add = $this->common_model->common_getRow('add_to_cart',array('user_id'=>$serial,'product_id'=>$product_id));
					if($is_add)
					{
						$json1 = file_get_contents('php://input');
			    		$json_array = json_decode($json1);
						$result = array(); 
				    	$result = $this->common_model->deleteData('add_to_cart',array('user_id'=>$serial,'product_id'=>$product_id));
					    if($result)
					    {
					    	$final_output['status'] = 'success';
					    	$final_output['message'] = 'successfully';	
					    }else
					    {
					    	$final_output['status'] = 'failed';
					    }
						
					}else
					{
						$final_output['status'] = 'failed';
					    $final_output['message'] = 'Item Not exist!!';	
					}
				}else
				{
					$final_output['status'] = 'empty request parameter!!';
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }
    public function RemoveTouchFeelCartItem()
    {
    	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$json1 = file_get_contents('php://input');
	    		$json_array = json_decode($json1);
	    		$product_id=$json_array->product_id;
	    		if($product_id!="")
	    		{
					$serial = $check['data']->serial; 
					$is_add = $this->common_model->common_getRow('add_to_touch_feel',array('user_id'=>$serial,'product_id'=>$product_id));
					if($is_add)
					{
						$json1 = file_get_contents('php://input');
			    		$json_array = json_decode($json1);
						$result = array(); 
				    	$result = $this->common_model->deleteData('add_to_touch_feel',array('user_id'=>$serial,'product_id'=>$product_id));
					    if($result)
					    {
					    	$final_output['status'] = 'success';
					    	$final_output['message'] = 'successfully';	
					    }else
					    {
					    	$final_output['status'] = 'failed';
					    }
						
					}else
					{
						$final_output['status'] = 'failed';
					    $final_output['message'] = 'Item Not exist!!';	
					}
				}else
				{
					$final_output['status'] = 'empty request parameter!!';
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
    }
    public function check_referral_code()
	{
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$json1 = file_get_contents('php://input');
	    		$json_array = json_decode($json1);

				$referral_code=$json_array->referral_code;
				$product_id=$json_array->product_id;
				$quantity_id=$json_array->quantity;

				if($quantity_id=="")
				{
					$quantity_id=1;
				}

		        //$code_data = $this->common_model->common_getRow('product_to_interior_designer',array('coupon_code'=>$referral_code,'product_id'=>$product_id));
		        $dode_dd=$this->db->query("SELECT product_to_interior_designer.* FROM product_to_interior_designer  LEFT JOIN product_tb ON product_tb.brand=product_to_interior_designer.brand_id WHERE product_to_interior_designer.coupon_code='$referral_code' AND product_tb.product_id='$product_id'");
		      //	print_r($this->db->last_query());exit;
		      	$code_data=$dode_dd->row();
		      	if($code_data)
		      	{
		      		$pro_data = $this->common_model->common_getRow('product_tb',array('product_id'=>$product_id));
			   		$discount=$code_data->discount;
			   		$price=$pro_data->price;
			   		$price_base=$pro_data->price_base;

			   		if($code_data->discount_type==0)
			   		{
			   			$discounted_amount = (($price*$discount)/100);
			   			$discounted_price = $price-$discounted_amount;
			   			if($discounted_amount >= $price_base)
			   			{
			   				$discounted_price = $price_base;
			   			}
			   			$final_output['coupon_id'] = $code_data->coupon_id;
			   			$final_output['discount'] = $discount;
			   			$final_output['status'] = 'success';

			   			$final_output['message'] = $discount."% offer is successfully applied,you have save Rs ".$discounted_amount*$quantity_id." and payble amount is  Rs ".$discounted_price*$quantity_id;
			   			//echo "<div class='offer_design'><p><STRONG>".$discount."% offer is successfully applied,you have save Rs ".$discounted_amount*$quantity_id." and payble amount is  Rs ".$discounted_price*$quantity_id.".</STRONG></p></div>";exit;
			   		}else
			   		{
			   			$discounted_price = $price-$discount;

			   			$final_output['coupon_id'] = $code_data->coupon_id;
			   			$final_output['discount'] = $discount;

			   			$final_output['status'] = 'success';
			   			$final_output['message'] = "offer is successfully applied,you have save Rs ".$discount*$quantity_id." and payble amount is  Rs ".$discounted_price*$quantity_id;
			   			//echo "<div class='offer_design'><p><STRONG>offer is successfully applied,you have save Rs ".$discount*$quantity_id." and payble amount is  Rs ".$discounted_price*$quantity_id.".</STRONG></p></div>";exit;
			   		}

		      	}else
		      	{
		      		//echo 2000;exit;
		      		$final_output['status'] = 'Invalid Code';
		      	}
		    }
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	public function check_zip_code()
	{
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{ 
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$json1 = file_get_contents('php://input');
	    		$json_array = json_decode($json1);

				$zip_code=$json_array->zip_code;

				$check_code_data = $this->common_model->common_getRow('zip_code',array('zip_code'=>$zip_code));
				if($check_code_data)
				{
					if($check_code_data->delivery_status=="Delivery")
					{
						$final_output['status'] = 'success';
						$final_output['delivery_status'] = $check_code_data->delivery_status;

						$final_output['message'] = "Delivery to pincode ".$zip_code." is available for this item";
					}
					else
					{
						$final_output['status'] = 'success';
						$final_output['delivery_status'] = $check_code_data->delivery_status;

						$final_output['message'] = "Delivery to pincode ".$zip_code." is currently not available for this item";
					}
				}else
				{
					$final_output['status'] = 'Please enter a valid 6 digit pincode';
				}
			}
		    else
		    {
		     	$final_output['status'] = 'invalid token';
		    }
	    }
	    else
	    {
	     	$final_output['status'] = 'unauthorized access';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

    public function ChechAuth($token)
	{
		$auth = $this->common_model->common_getRow('customers',array('token'=>$token));
		if(!empty($auth))
		{
			$abc['status'] = "true";
			$abc['data'] =$auth;
			return $abc;
		}else
		{
			$abc['status'] = "false";
			return $abc;
		}
	}
}
